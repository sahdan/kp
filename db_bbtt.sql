-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2019 at 03:21 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bbtt`
--
CREATE DATABASE IF NOT EXISTS `db_bbtt` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_bbtt`;

-- --------------------------------------------------------

--
-- Table structure for table `t_ap`
--

CREATE TABLE `t_ap` (
  `ap_id` int(11) NOT NULL,
  `ps_id` int(2) DEFAULT NULL,
  `ap_bill_id` int(11) NOT NULL,
  `ap_bill_type` varchar(7) NOT NULL COMMENT 'Biaya vs Invoice',
  `ap_date` date NOT NULL,
  `ap_amount` int(9) NOT NULL,
  `ap_payment_type` varchar(5) NOT NULL,
  `ap_notes` text,
  `ap_created_by` int(4) NOT NULL,
  `ap_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ap`
--

INSERT INTO `t_ap` (`ap_id`, `ps_id`, `ap_bill_id`, `ap_bill_type`, `ap_date`, `ap_amount`, `ap_payment_type`, `ap_notes`, `ap_created_by`, `ap_status`) VALUES
(1, NULL, 1, 'Biaya', '2019-03-19', 500000, 'BCA', 'Biaya Internet', 1, 1),
(2, NULL, 2, 'Biaya', '2019-03-20', 640000, 'BCA', 'Biaya Listrik', 1, 1),
(3, NULL, 3, 'Biaya', '2019-03-21', 50000, 'Tunai', 'Biaya Air', 1, 1),
(4, NULL, 4, 'Biaya', '2019-03-21', 23000, 'BCA', 'Biaya Telpon', 1, 1),
(5, NULL, 5, 'Biaya', '2019-03-21', 15000, 'Tunai', 'Biaya ATK', 1, 1),
(6, 1, 1, 'Invoice', '2019-03-21', 400000, 'BCA', 'SUB - SIN', 1, 1),
(7, 2, 1, 'Invoice', '2019-03-21', 1000000, 'BCA', 'SUB-CGK PP', 1, 1),
(10, 2, 2, 'Invoice', '2019-04-24', 600000, 'BCA', 'MR WENDA MANDIRI', 1, 0),
(11, 3, 2, 'Invoice', '2019-04-24', 2000000, 'BCA', 'ROUND TRIP BALI\r\nMR SMITH\r\nMRS SMITH', 1, 0),
(12, 2, 3, 'Invoice', '2019-04-26', 800000, 'BCA', 'SUB-CGK Lion Air\r\nMR WENDA MANDIRI', 1, 1),
(13, 6, 3, 'Invoice', '2019-04-26', 1200000, 'BCA', 'Mobil Avanza 3 hari + Bensin + Sopir', 1, 1),
(14, 4, 4, 'Invoice', '2019-05-01', 1600000, 'BCA', 'Jakarta Surabaya Pulang Pergi\r\nBerangkat 2 Mei 2019 8:00 WIB\r\nPulang 5 Mei 2010 8:00 WIB\r\nKelas Eksekutif\r\nMR ANDHI AKBARA', 1, 1),
(15, 6, 4, 'Invoice', '2019-05-01', 800000, 'BCA', 'Mobil Avanza + Bensin + Sopir\r\n2 hari', 1, 1),
(16, 4, 5, 'Invoice', '2019-05-07', 1200000, 'BCA', 'JAKK - SGU PP\r\nBerangkat 7 Mei 2019\r\nPulang 10 Mei 2019\r\nMR ANDHI AKBARA', 1, 1),
(17, 2, 6, 'Invoice', '2019-05-07', 1800000, 'BCA', 'SUB - CGK Pulang Pergi\r\nMR. SYABITH UMAR AHDAN\r\nMR. WENDA MANDIRI', 1, 1),
(18, NULL, 6, 'Biaya', '2019-05-07', 500000, 'Tunai', 'Biaya Internet', 1, 1),
(19, NULL, 7, 'Biaya', '2019-05-07', 25000, 'BCA', 'Biaya Air', 1, 1),
(20, NULL, 8, 'Biaya', '2019-05-07', 20000, 'BCA', 'Biaya Admin BCA', 1, 1),
(21, NULL, 9, 'Biaya', '2019-05-09', 50000, 'Tunai', 'Beli Bolpoin, Map, dan Rak kecil untuk meja', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_ar`
--

CREATE TABLE `t_ar` (
  `ar_id` int(11) NOT NULL,
  `ar_income_id` int(11) NOT NULL,
  `ar_income_type` varchar(12) NOT NULL DEFAULT 'Invoice' COMMENT 'Invoice vs Other Income',
  `ar_date` date NOT NULL,
  `ar_amount` int(9) NOT NULL,
  `ar_payment_type` varchar(5) NOT NULL,
  `ar_notes` text,
  `ar_created_by` int(4) NOT NULL,
  `ar_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ar`
--

INSERT INTO `t_ar` (`ar_id`, `ar_income_id`, `ar_income_type`, `ar_date`, `ar_amount`, `ar_payment_type`, `ar_notes`, `ar_created_by`, `ar_status`) VALUES
(1, 2, 'Invoice', '2019-04-10', 1900000, 'BCA', '', 1, 0),
(2, 2, 'Invoice', '2019-04-24', 1000000, 'BCA', '', 1, 0),
(3, 1, 'Invoice', '2019-05-01', 1600000, 'Tunai', 'Pembayaran Lunas', 1, 1),
(4, 6, 'Other Income', '2019-05-07', 10000, 'BCA', 'Bunga Bank BCA Bulan April', 1, 1),
(5, 7, 'Other Income', '2019-05-07', 1700000, 'BCA', 'Dijual ke Umar', 1, 1),
(6, 6, 'Invoice', '2019-05-07', 2200000, 'Tunai', 'Lunas', 1, 1),
(7, 8, 'Other Income', '2019-05-09', 300000, 'Tunai', 'Di jual ke Rudi', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_cost_journals`
--

CREATE TABLE `t_cost_journals` (
  `cj_id` int(11) NOT NULL,
  `cj_date` date NOT NULL,
  `cj_name` varchar(100) NOT NULL,
  `cj_category` varchar(50) NOT NULL,
  `cj_amount` int(9) NOT NULL,
  `cj_created_by` int(4) NOT NULL,
  `cj_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_cost_journals`
--

INSERT INTO `t_cost_journals` (`cj_id`, `cj_date`, `cj_name`, `cj_category`, `cj_amount`, `cj_created_by`, `cj_status`) VALUES
(1, '2019-03-19', 'Biaya Internet', 'Biaya Usaha', 500000, 1, 1),
(2, '2019-03-20', 'Biaya Listrik', 'Biaya Usaha', 640000, 1, 1),
(3, '2019-03-21', 'Biaya Air', 'Biaya Usaha', 50000, 1, 1),
(4, '2019-03-21', 'Biaya Telepon', 'Biaya Usaha', 23000, 1, 1),
(5, '2019-03-21', 'Biaya ATK', 'Biaya Usaha', 15000, 1, 1),
(6, '2019-05-07', 'Biaya Internet', 'Biaya Usaha', 500000, 1, 1),
(7, '2019-05-07', 'Biaya Air', 'Biaya Usaha', 25000, 1, 1),
(8, '2019-05-07', 'Biaya Admin BCA', 'Biaya Admin dan Provisi Bank', 20000, 1, 1),
(9, '2019-05-09', 'Biaya ATK', 'Biaya Umum dan Administrasi Lainnya', 50000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_customers`
--

CREATE TABLE `t_customers` (
  `cust_id` int(9) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `cust_title` tinytext NOT NULL,
  `cust_id_number` varchar(100) NOT NULL,
  `cust_id_type` varchar(10) NOT NULL,
  `cust_email` varchar(50) DEFAULT NULL,
  `cust_phone` varchar(20) NOT NULL,
  `cust_address` varchar(255) DEFAULT NULL,
  `cust_birthdate` date NOT NULL,
  `cust_emer_name` varchar(50) NOT NULL,
  `cust_emer_phone` varchar(20) NOT NULL,
  `cust_date_created` date NOT NULL,
  `cust_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_customers`
--

INSERT INTO `t_customers` (`cust_id`, `cust_name`, `cust_title`, `cust_id_number`, `cust_id_type`, `cust_email`, `cust_phone`, `cust_address`, `cust_birthdate`, `cust_emer_name`, `cust_emer_phone`, `cust_date_created`, `cust_status`) VALUES
(1, 'Syabith Umar Ahdan', 'Mr', 'A 9601796', 'Passport', 'syabith33@gmail.com', '+6287856339039', 'Jl. Rungkut Mapan Tengah VI', '2019-03-12', 'Rully Virayanti', 'bc9ffb83569c7e269c04', '2019-04-26', 1),
(2, 'Wenda Mandiri', 'Mr', '1234567891234', 'KTP', 'wenda1945@gmail.com', '087855443322', 'Kosan Dekat STTS', '1997-04-24', 'Mr X', '087744443322', '2019-04-26', 1),
(3, 'Andhi Akbara', 'Mr', '215842310119900001', 'KTP', 'syabith33@gmail.com', '087878945612', 'Jl Pondok Chandra Sidoarjo', '1990-01-31', 'Fatika Akbara', '087878945612', '2019-05-01', 1),
(4, 'Dewi Sumarni', 'Ms', '123456789456123154', 'SIM', 'syabith33@gmail.com', '087812345678', 'Jl Fiksi no 3', '2001-05-07', 'Dewi Sumarni', '087812345678', '2019-05-07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_employees`
--

CREATE TABLE `t_employees` (
  `emp_id` int(4) NOT NULL,
  `emp_username` varchar(20) NOT NULL,
  `emp_password` varchar(32) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `emp_id_card_number` varchar(20) NOT NULL,
  `emp_email` varchar(50) NOT NULL,
  `emp_phone` varchar(20) NOT NULL,
  `emp_address` varchar(255) NOT NULL,
  `emp_birthdate` date NOT NULL,
  `emp_access_level` tinyint(1) NOT NULL,
  `emp_reset_token` varchar(32) DEFAULT NULL COMMENT 'Token for Password reset',
  `emp_token_expire` timestamp NULL DEFAULT NULL,
  `emp_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_employees`
--

INSERT INTO `t_employees` (`emp_id`, `emp_username`, `emp_password`, `emp_name`, `emp_id_card_number`, `emp_email`, `emp_phone`, `emp_address`, `emp_birthdate`, `emp_access_level`, `emp_reset_token`, `emp_token_expire`, `emp_status`) VALUES
(1, 'dion', '827ccb0eea8a706c4c34a16891f84e7b', 'Aristyo Wardiono', '3571020101910001', 'bbtraveltour.surabaya@gmail.com', '03158200740', 'Apartemen Puri Mas G - 07, Jl. I Gusti Ngurah Rai, Surabaya', '1987-04-12', 1, NULL, NULL, 1),
(2, 'novan', '827ccb0eea8a706c4c34a16891f84e7b', 'Novan', '3571020101910002', 'novan@bbtourtravel.com', '03158200740', 'Jl. Puri', '1987-04-12', 0, NULL, NULL, 1),
(3, 'umar', 'e1475179e1e797ca347528ba5a12781f', 'Syabith Umar Ahdan', '3571020101910003', 'syabithumar33@yahoo.com', '+6287856339039', 'Jl. Rungkut Mapan Tengah VI', '2019-03-13', 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice_d`
--

CREATE TABLE `t_invoice_d` (
  `invh_id` int(11) NOT NULL,
  `ps_id` int(11) NOT NULL,
  `invd_description` text NOT NULL,
  `invd_quantity` int(3) NOT NULL,
  `invd_cost` int(9) NOT NULL,
  `invd_price_display` int(9) NOT NULL,
  `invd_price_actual` int(9) NOT NULL,
  `invd_commission` int(9) NOT NULL DEFAULT '0',
  `invd_notes` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_invoice_d`
--

INSERT INTO `t_invoice_d` (`invh_id`, `ps_id`, `invd_description`, `invd_quantity`, `invd_cost`, `invd_price_display`, `invd_price_actual`, `invd_commission`, `invd_notes`) VALUES
(1, 1, 'SUB - SIN', 1, 400000, 600000, 500000, 10000, NULL),
(1, 2, 'SUB-CGK PP', 1, 1000000, 1200000, 1100000, 10000, NULL),
(2, 2, 'MR WENDA MANDIRI', 2, 300000, 400000, 350000, 10000, NULL),
(2, 3, 'ROUND TRIP BALI\r\nMR SMITH\r\nMRS SMITH', 2, 1000000, 1200000, 1100000, 50000, NULL),
(3, 2, 'SUB-CGK Lion Air\r\nMR WENDA MANDIRI', 2, 400000, 600000, 500000, 10000, NULL),
(3, 6, 'Mobil Avanza 3 hari + Bensin + Sopir', 1, 1200000, 1500000, 1400000, 0, NULL),
(4, 4, 'Jakarta Surabaya Pulang Pergi\r\nBerangkat 2 Mei 2019 8:00 WIB\r\nPulang 5 Mei 2010 8:00 WIB\r\nKelas Eksekutif\r\nMR ANDHI AKBARA', 1, 1600000, 2000000, 1800000, 18000, NULL),
(4, 6, 'Mobil Avanza + Bensin + Sopir\r\n2 hari', 1, 800000, 1000000, 900000, 9000, NULL),
(5, 4, 'JAKK - SGU PP\r\nBerangkat 7 Mei 2019\r\nPulang 10 Mei 2019\r\nMR ANDHI AKBARA', 1, 1200000, 1500000, 1350000, 13500, NULL),
(6, 2, 'SUB - CGK Pulang Pergi\r\nMR. SYABITH UMAR AHDAN\r\nMR. WENDA MANDIRI', 2, 900000, 1300000, 1100000, 22000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_invoice_h`
--

CREATE TABLE `t_invoice_h` (
  `invh_id` int(11) NOT NULL,
  `invh_date_created` date NOT NULL,
  `invh_date_due` date NOT NULL,
  `cust_id` int(9) NOT NULL,
  `invh_created_by` int(4) NOT NULL,
  `invh_status` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_invoice_h`
--

INSERT INTO `t_invoice_h` (`invh_id`, `invh_date_created`, `invh_date_due`, `cust_id`, `invh_created_by`, `invh_status`) VALUES
(1, '2019-03-21', '2019-03-28', 1, 3, 'Paid'),
(2, '2019-04-24', '2019-04-30', 2, 1, 'Voided'),
(3, '2019-04-26', '2019-05-17', 2, 2, 'Sent'),
(4, '2019-05-01', '2019-05-31', 3, 2, 'Sent'),
(5, '2019-05-07', '2019-05-29', 3, 3, 'Sent'),
(6, '2019-05-07', '2019-05-31', 2, 2, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `t_other_incomes`
--

CREATE TABLE `t_other_incomes` (
  `oi_id` int(11) NOT NULL,
  `oi_date` date NOT NULL,
  `oi_name` varchar(100) NOT NULL,
  `oi_category` varchar(50) NOT NULL,
  `oi_amount` int(9) NOT NULL,
  `oi_created_by` int(4) NOT NULL,
  `oi_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_other_incomes`
--

INSERT INTO `t_other_incomes` (`oi_id`, `oi_date`, `oi_name`, `oi_category`, `oi_amount`, `oi_created_by`, `oi_status`) VALUES
(6, '2019-05-07', 'Bunga Bank BCA', 'Pendapatan Lain-Lain (Jasa Giro,Bunga,dll)', 10000, 1, 1),
(7, '2019-05-07', 'Jual PC Lama Dell', 'Pendapatan Usaha Lainnya', 1700000, 1, 1),
(8, '2019-05-09', 'Jual Monitor HP Bekas', 'Pendapatan Usaha Lainnya', 300000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_products_services`
--

CREATE TABLE `t_products_services` (
  `ps_id` int(2) NOT NULL,
  `ps_name` varchar(100) NOT NULL,
  `ps_description` text NOT NULL,
  `ps_category` varchar(20) NOT NULL,
  `ps_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_products_services`
--

INSERT INTO `t_products_services` (`ps_id`, `ps_name`, `ps_description`, `ps_category`, `ps_status`) VALUES
(1, 'SUB-CGK', 'Penerbangan Surabaya Jakarta', 'Flight Ticket', 1),
(2, 'SUB-CGK Round Trip', 'Penerbangan Surabaya - Jakarta pulang pergi', 'Flight Ticket', 1),
(3, 'Paket Honeymoon Bali 3D2N', 'Paket Honeymoon ke Bali untuk 2 orang selama 3 hari 2 malam', 'Tour Package', 1),
(4, 'JAKK - SGU Round Trip', 'Tiket Kereta Jakarta Kota - Surabaya Gubeng Pulang Pergi', 'Train Ticket', 1),
(5, 'Voucher Hotal di Bali', 'Voucher Hotel di Bali', 'Hotel Voucher', 1),
(6, 'Sewa Mobil', 'Sewa Mobil', 'Car Rental', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_reminders`
--

CREATE TABLE `t_reminders` (
  `rem_id` int(11) NOT NULL,
  `rem_date_created` date NOT NULL,
  `rem_date_remind` date NOT NULL,
  `invh_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_reminders`
--

INSERT INTO `t_reminders` (`rem_id`, `rem_date_created`, `rem_date_remind`, `invh_id`) VALUES
(2, '2019-05-14', '2019-05-15', 5);

-- --------------------------------------------------------

--
-- Table structure for table `t_vendors`
--

CREATE TABLE `t_vendors` (
  `vend_id` int(4) NOT NULL,
  `vend_name` varchar(100) NOT NULL,
  `vend_email` varchar(50) NOT NULL,
  `vend_phone` varchar(20) NOT NULL,
  `vend_address` varchar(255) NOT NULL,
  `vend_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_vendors`
--

INSERT INTO `t_vendors` (`vend_id`, `vend_name`, `vend_email`, `vend_phone`, `vend_address`, `vend_status`) VALUES
(1, 'Lion Air', 'contact@lionair.com', '+6287856339039', 'Jl. Rungkut Mapan Tengah VI', 1),
(2, 'Air Asia', 'contact@airasia.com', '+6287856339039', 'Jl. Rungkut Mapan Tengah VI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_vend_cps`
--

CREATE TABLE `t_vend_cps` (
  `vendcp_id` int(4) NOT NULL,
  `vend_id` int(4) NOT NULL,
  `vendcp_name` varchar(100) NOT NULL,
  `vendcp_phone` varchar(20) NOT NULL,
  `vendcp_position` varchar(30) NOT NULL,
  `vendcp_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_vend_cps`
--

INSERT INTO `t_vend_cps` (`vendcp_id`, `vend_id`, `vendcp_name`, `vendcp_phone`, `vendcp_position`, `vendcp_status`) VALUES
(1, 1, 'Umar', '12345', 'CEO', 1),
(2, 2, 'Ahdan', '+6287856339039', 'CTO', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_active_invoice_d`
-- (See below for the actual view)
--
CREATE TABLE `v_active_invoice_d` (
`invh_date_created` date
,`invh_created_by` int(4)
,`invh_id` int(11)
,`ps_id` int(11)
,`ps_name` varchar(100)
,`invd_description` text
,`invd_quantity` int(3)
,`invd_cost` int(9)
,`invd_price_display` int(9)
,`invd_price_actual` int(9)
,`invd_commission` int(9)
,`invd_total` bigint(21)
,`invd_discount` bigint(22)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_active_invoice_d_last_3_months`
-- (See below for the actual view)
--
CREATE TABLE `v_active_invoice_d_last_3_months` (
`invh_id` int(11)
,`ps_id` int(11)
,`ps_name` varchar(100)
,`invd_description` text
,`invd_quantity` int(3)
,`invd_cost` int(9)
,`invd_price_display` int(9)
,`invd_price_actual` int(9)
,`invd_commission` int(9)
,`invd_total` bigint(21)
,`invd_discount` bigint(22)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_ar_per_invoice`
-- (See below for the actual view)
--
CREATE TABLE `v_ar_per_invoice` (
`invh_id` int(11)
,`ar_total` decimal(32,0)
,`ar_status` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_d`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_d` (
`invh_id` int(11)
,`ps_id` int(11)
,`ps_name` varchar(100)
,`invd_description` text
,`invd_quantity` int(3)
,`invd_cost` int(9)
,`invd_price_display` int(9)
,`invd_price_actual` int(9)
,`invd_commission` int(9)
,`invd_total` bigint(21)
,`invd_discount` bigint(22)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_invoice_h`
-- (See below for the actual view)
--
CREATE TABLE `v_invoice_h` (
`invh_status` varchar(7)
,`invh_date_created` date
,`invh_date_due` date
,`invh_id` int(11)
,`invh_created_by` int(4)
,`cust_id` int(9)
,`cust_name` varchar(100)
,`invh_total` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_inv_this_month`
-- (See below for the actual view)
--
CREATE TABLE `v_inv_this_month` (
`invh_id` int(11)
,`invh_date_created` date
,`invh_date_due` date
,`cust_id` int(9)
,`invh_created_by` int(4)
,`invh_status` varchar(7)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_new_cust_this_month`
-- (See below for the actual view)
--
CREATE TABLE `v_new_cust_this_month` (
`cust_id` int(9)
,`cust_name` varchar(100)
,`cust_title` tinytext
,`cust_id_number` varchar(100)
,`cust_id_type` varchar(10)
,`cust_email` varchar(50)
,`cust_phone` varchar(20)
,`cust_address` varchar(255)
,`cust_birthdate` date
,`cust_emer_name` varchar(50)
,`cust_emer_phone` varchar(20)
,`cust_date_created` date
,`cust_status` tinyint(1)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_this_month`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_this_month` (
`sales` decimal(64,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_sales_total_last_3_months`
-- (See below for the actual view)
--
CREATE TABLE `v_sales_total_last_3_months` (
`Month` varchar(69)
,`Total` decimal(65,4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_total_per_cat_last_3_months`
-- (See below for the actual view)
--
CREATE TABLE `v_total_per_cat_last_3_months` (
`ps_category` varchar(20)
,`total` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_total_per_ps_last_3_month`
-- (See below for the actual view)
--
CREATE TABLE `v_total_per_ps_last_3_month` (
`ps_name` varchar(100)
,`count` decimal(32,0)
,`total` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_transactions`
-- (See below for the actual view)
--
CREATE TABLE `v_transactions` (
`tran_date` date
,`tran_amount` int(11)
,`tran_created_by` int(11)
,`tran_notes` text
,`tran_type` varchar(2)
,`tran_status` tinyint(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_unpaid_invoice_h`
-- (See below for the actual view)
--
CREATE TABLE `v_unpaid_invoice_h` (
`invh_status` varchar(7)
,`invh_date_created` date
,`invh_date_due` date
,`invh_id` int(11)
,`cust_name` varchar(100)
,`amount_due` decimal(43,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_vend_cps`
-- (See below for the actual view)
--
CREATE TABLE `v_vend_cps` (
`vendcp_id` int(4)
,`vendcp_name` varchar(100)
,`vendcp_phone` varchar(20)
,`vendcp_position` varchar(30)
,`vendcp_status` tinyint(1)
,`vend_name` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure for view `v_active_invoice_d`
--
DROP TABLE IF EXISTS `v_active_invoice_d`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_active_invoice_d`  AS  select `v_invoice_h`.`invh_date_created` AS `invh_date_created`,`v_invoice_h`.`invh_created_by` AS `invh_created_by`,`v_invoice_d`.`invh_id` AS `invh_id`,`v_invoice_d`.`ps_id` AS `ps_id`,`v_invoice_d`.`ps_name` AS `ps_name`,`v_invoice_d`.`invd_description` AS `invd_description`,`v_invoice_d`.`invd_quantity` AS `invd_quantity`,`v_invoice_d`.`invd_cost` AS `invd_cost`,`v_invoice_d`.`invd_price_display` AS `invd_price_display`,`v_invoice_d`.`invd_price_actual` AS `invd_price_actual`,`v_invoice_d`.`invd_commission` AS `invd_commission`,`v_invoice_d`.`invd_total` AS `invd_total`,`v_invoice_d`.`invd_discount` AS `invd_discount` from (`v_invoice_d` join `v_invoice_h` on((`v_invoice_d`.`invh_id` = `v_invoice_h`.`invh_id`))) where ((`v_invoice_h`.`invh_status` <> 'Voided') and (`v_invoice_h`.`invh_status` <> 'Draft')) ;

-- --------------------------------------------------------

--
-- Structure for view `v_active_invoice_d_last_3_months`
--
DROP TABLE IF EXISTS `v_active_invoice_d_last_3_months`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_active_invoice_d_last_3_months`  AS  select `v_invoice_d`.`invh_id` AS `invh_id`,`v_invoice_d`.`ps_id` AS `ps_id`,`v_invoice_d`.`ps_name` AS `ps_name`,`v_invoice_d`.`invd_description` AS `invd_description`,`v_invoice_d`.`invd_quantity` AS `invd_quantity`,`v_invoice_d`.`invd_cost` AS `invd_cost`,`v_invoice_d`.`invd_price_display` AS `invd_price_display`,`v_invoice_d`.`invd_price_actual` AS `invd_price_actual`,`v_invoice_d`.`invd_commission` AS `invd_commission`,`v_invoice_d`.`invd_total` AS `invd_total`,`v_invoice_d`.`invd_discount` AS `invd_discount` from (`v_invoice_d` join `v_invoice_h` on((`v_invoice_d`.`invh_id` = `v_invoice_h`.`invh_id`))) where ((`v_invoice_h`.`invh_status` <> 'Voided') and (`v_invoice_h`.`invh_status` <> 'Draft') and (`v_invoice_h`.`invh_date_created` > (curdate() - interval 3 month))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_ar_per_invoice`
--
DROP TABLE IF EXISTS `v_ar_per_invoice`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_ar_per_invoice`  AS  select `t_ar`.`ar_income_id` AS `invh_id`,sum(`t_ar`.`ar_amount`) AS `ar_total`,`t_ar`.`ar_status` AS `ar_status` from `t_ar` where ((`t_ar`.`ar_status` = 1) and (`t_ar`.`ar_income_type` like 'Invoice')) group by `t_ar`.`ar_income_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_d`
--
DROP TABLE IF EXISTS `v_invoice_d`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_d`  AS  select `invd`.`invh_id` AS `invh_id`,`invd`.`ps_id` AS `ps_id`,`ps`.`ps_name` AS `ps_name`,`invd`.`invd_description` AS `invd_description`,`invd`.`invd_quantity` AS `invd_quantity`,`invd`.`invd_cost` AS `invd_cost`,`invd`.`invd_price_display` AS `invd_price_display`,`invd`.`invd_price_actual` AS `invd_price_actual`,`invd`.`invd_commission` AS `invd_commission`,(`invd`.`invd_quantity` * `invd`.`invd_price_display`) AS `invd_total`,((`invd`.`invd_quantity` * `invd`.`invd_price_display`) - (`invd`.`invd_quantity` * `invd`.`invd_price_actual`)) AS `invd_discount` from (`t_invoice_d` `invd` join `t_products_services` `ps` on((`invd`.`ps_id` = `ps`.`ps_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_invoice_h`
--
DROP TABLE IF EXISTS `v_invoice_h`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_h`  AS  select `invh`.`invh_status` AS `invh_status`,`invh`.`invh_date_created` AS `invh_date_created`,`invh`.`invh_date_due` AS `invh_date_due`,`invh`.`invh_id` AS `invh_id`,`invh`.`invh_created_by` AS `invh_created_by`,`cust`.`cust_id` AS `cust_id`,`cust`.`cust_name` AS `cust_name`,sum((`invd`.`invd_price_actual` * `invd`.`invd_quantity`)) AS `invh_total` from ((`t_invoice_h` `invh` join `t_customers` `cust` on((`invh`.`cust_id` = `cust`.`cust_id`))) join `t_invoice_d` `invd` on((`invh`.`invh_id` = `invd`.`invh_id`))) group by `invh`.`invh_id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_inv_this_month`
--
DROP TABLE IF EXISTS `v_inv_this_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inv_this_month`  AS  select `t_invoice_h`.`invh_id` AS `invh_id`,`t_invoice_h`.`invh_date_created` AS `invh_date_created`,`t_invoice_h`.`invh_date_due` AS `invh_date_due`,`t_invoice_h`.`cust_id` AS `cust_id`,`t_invoice_h`.`invh_created_by` AS `invh_created_by`,`t_invoice_h`.`invh_status` AS `invh_status` from `t_invoice_h` where ((`t_invoice_h`.`invh_status` not in ('Draft','Voided')) and (month(`t_invoice_h`.`invh_date_created`) = month(curdate())) and (year(`t_invoice_h`.`invh_date_created`) = year(curdate()))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_new_cust_this_month`
--
DROP TABLE IF EXISTS `v_new_cust_this_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_new_cust_this_month`  AS  select `t_customers`.`cust_id` AS `cust_id`,`t_customers`.`cust_name` AS `cust_name`,`t_customers`.`cust_title` AS `cust_title`,`t_customers`.`cust_id_number` AS `cust_id_number`,`t_customers`.`cust_id_type` AS `cust_id_type`,`t_customers`.`cust_email` AS `cust_email`,`t_customers`.`cust_phone` AS `cust_phone`,`t_customers`.`cust_address` AS `cust_address`,`t_customers`.`cust_birthdate` AS `cust_birthdate`,`t_customers`.`cust_emer_name` AS `cust_emer_name`,`t_customers`.`cust_emer_phone` AS `cust_emer_phone`,`t_customers`.`cust_date_created` AS `cust_date_created`,`t_customers`.`cust_status` AS `cust_status` from `t_customers` where ((month(`t_customers`.`cust_date_created`) = month(curdate())) and (year(`t_customers`.`cust_date_created`) = year(curdate()))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_this_month`
--
DROP TABLE IF EXISTS `v_sales_this_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_this_month`  AS  select ifnull(sum(`v_invoice_h`.`invh_total`),0) AS `sales` from `v_invoice_h` where ((`v_invoice_h`.`invh_status` not in ('Draft','Voided')) and (month(`v_invoice_h`.`invh_date_created`) = month(curdate())) and (year(`v_invoice_h`.`invh_date_created`) = year(curdate()))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_sales_total_last_3_months`
--
DROP TABLE IF EXISTS `v_sales_total_last_3_months`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sales_total_last_3_months`  AS  select date_format(`v_invoice_h`.`invh_date_created`,'%M %Y') AS `Month`,(sum(`v_invoice_h`.`invh_total`) / 1000000) AS `Total` from `v_invoice_h` where ((`v_invoice_h`.`invh_status` <> 'Voided') and (`v_invoice_h`.`invh_status` <> 'Draft') and (`v_invoice_h`.`invh_date_created` >= ((last_day(now()) + interval 1 day) - interval 3 month))) group by date_format(`v_invoice_h`.`invh_date_created`,'%m-%Y') ;

-- --------------------------------------------------------

--
-- Structure for view `v_total_per_cat_last_3_months`
--
DROP TABLE IF EXISTS `v_total_per_cat_last_3_months`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_total_per_cat_last_3_months`  AS  select `ps`.`ps_category` AS `ps_category`,ifnull(sum((`aid`.`invd_quantity` * `aid`.`invd_price_actual`)),0) AS `total` from (`t_products_services` `ps` left join `v_active_invoice_d_last_3_months` `aid` on((`ps`.`ps_id` = `aid`.`ps_id`))) group by `ps`.`ps_category` ;

-- --------------------------------------------------------

--
-- Structure for view `v_total_per_ps_last_3_month`
--
DROP TABLE IF EXISTS `v_total_per_ps_last_3_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_total_per_ps_last_3_month`  AS  select `ps`.`ps_name` AS `ps_name`,ifnull(sum(`aid`.`invd_quantity`),0) AS `count`,ifnull(sum((`aid`.`invd_quantity` * `aid`.`invd_price_actual`)),0) AS `total` from (`t_products_services` `ps` left join `v_active_invoice_d_last_3_months` `aid` on((`ps`.`ps_id` = `aid`.`ps_id`))) group by `ps`.`ps_id` order by ifnull(sum((`aid`.`invd_quantity` * `aid`.`invd_price_actual`)),0) desc limit 10 ;

-- --------------------------------------------------------

--
-- Structure for view `v_transactions`
--
DROP TABLE IF EXISTS `v_transactions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_transactions`  AS  select `t_ap`.`ap_date` AS `tran_date`,`t_ap`.`ap_amount` AS `tran_amount`,`t_ap`.`ap_created_by` AS `tran_created_by`,`t_ap`.`ap_notes` AS `tran_notes`,'ap' AS `tran_type`,`t_ap`.`ap_status` AS `tran_status` from `t_ap` union all select `t_ar`.`ar_date` AS `tran_date`,`t_ar`.`ar_amount` AS `tran_amount`,`t_ar`.`ar_created_by` AS `tran_created_by`,`t_ar`.`ar_notes` AS `tran_notes`,'ar' AS `tran_type`,`t_ar`.`ar_status` AS `tran_status` from `t_ar` order by `tran_date` desc ;

-- --------------------------------------------------------

--
-- Structure for view `v_unpaid_invoice_h`
--
DROP TABLE IF EXISTS `v_unpaid_invoice_h`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_unpaid_invoice_h`  AS  select `vih`.`invh_status` AS `invh_status`,`vih`.`invh_date_created` AS `invh_date_created`,`vih`.`invh_date_due` AS `invh_date_due`,`vih`.`invh_id` AS `invh_id`,`vih`.`cust_name` AS `cust_name`,(`vih`.`invh_total` - ifnull(`ar`.`ar_total`,0)) AS `amount_due` from (`v_invoice_h` `vih` left join `v_ar_per_invoice` `ar` on((`ar`.`invh_id` = `vih`.`invh_id`))) where ((`vih`.`invh_total` - ifnull(`ar`.`ar_total`,0)) > 0) ;

-- --------------------------------------------------------

--
-- Structure for view `v_vend_cps`
--
DROP TABLE IF EXISTS `v_vend_cps`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_vend_cps`  AS  select `vcp`.`vendcp_id` AS `vendcp_id`,`vcp`.`vendcp_name` AS `vendcp_name`,`vcp`.`vendcp_phone` AS `vendcp_phone`,`vcp`.`vendcp_position` AS `vendcp_position`,`vcp`.`vendcp_status` AS `vendcp_status`,`v`.`vend_name` AS `vend_name` from (`t_vend_cps` `vcp` join `t_vendors` `v` on((`vcp`.`vend_id` = `v`.`vend_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_ap`
--
ALTER TABLE `t_ap`
  ADD PRIMARY KEY (`ap_id`),
  ADD KEY `FK_T_PS_ID_T_PS` (`ps_id`);

--
-- Indexes for table `t_ar`
--
ALTER TABLE `t_ar`
  ADD PRIMARY KEY (`ar_id`),
  ADD KEY `FK_T_CREAT_BY_T_EMPLOYEES2` (`ar_created_by`);

--
-- Indexes for table `t_cost_journals`
--
ALTER TABLE `t_cost_journals`
  ADD PRIMARY KEY (`cj_id`);

--
-- Indexes for table `t_customers`
--
ALTER TABLE `t_customers`
  ADD PRIMARY KEY (`cust_id`);

--
-- Indexes for table `t_employees`
--
ALTER TABLE `t_employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `t_invoice_d`
--
ALTER TABLE `t_invoice_d`
  ADD PRIMARY KEY (`invh_id`,`ps_id`),
  ADD KEY `FK_T_PS_ID_T_PRODUCTS_SERVICES` (`ps_id`);

--
-- Indexes for table `t_invoice_h`
--
ALTER TABLE `t_invoice_h`
  ADD PRIMARY KEY (`invh_id`),
  ADD KEY `FK_T_CUST_ID_T_CUSTOMERS` (`cust_id`),
  ADD KEY `FK_T_CREAT_BY_T_EMPLOYEES` (`invh_created_by`);

--
-- Indexes for table `t_other_incomes`
--
ALTER TABLE `t_other_incomes`
  ADD PRIMARY KEY (`oi_id`);

--
-- Indexes for table `t_products_services`
--
ALTER TABLE `t_products_services`
  ADD PRIMARY KEY (`ps_id`);

--
-- Indexes for table `t_reminders`
--
ALTER TABLE `t_reminders`
  ADD PRIMARY KEY (`rem_id`),
  ADD KEY `FK_T_INVH_ID_T_INVOICE_H2` (`invh_id`);

--
-- Indexes for table `t_vendors`
--
ALTER TABLE `t_vendors`
  ADD PRIMARY KEY (`vend_id`);

--
-- Indexes for table `t_vend_cps`
--
ALTER TABLE `t_vend_cps`
  ADD PRIMARY KEY (`vendcp_id`),
  ADD KEY `FK_T_VEND_ID_T_VENDORS` (`vend_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_ap`
--
ALTER TABLE `t_ap`
  MODIFY `ap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `t_ar`
--
ALTER TABLE `t_ar`
  MODIFY `ar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_cost_journals`
--
ALTER TABLE `t_cost_journals`
  MODIFY `cj_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_customers`
--
ALTER TABLE `t_customers`
  MODIFY `cust_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_employees`
--
ALTER TABLE `t_employees`
  MODIFY `emp_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_invoice_h`
--
ALTER TABLE `t_invoice_h`
  MODIFY `invh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13501;

--
-- AUTO_INCREMENT for table `t_other_incomes`
--
ALTER TABLE `t_other_incomes`
  MODIFY `oi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_products_services`
--
ALTER TABLE `t_products_services`
  MODIFY `ps_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_reminders`
--
ALTER TABLE `t_reminders`
  MODIFY `rem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_vendors`
--
ALTER TABLE `t_vendors`
  MODIFY `vend_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_vend_cps`
--
ALTER TABLE `t_vend_cps`
  MODIFY `vendcp_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_ap`
--
ALTER TABLE `t_ap`
  ADD CONSTRAINT `FK_T_PS_ID_T_PS` FOREIGN KEY (`ps_id`) REFERENCES `t_products_services` (`ps_id`);

--
-- Constraints for table `t_ar`
--
ALTER TABLE `t_ar`
  ADD CONSTRAINT `FK_T_CREAT_BY_T_EMPLOYEES2` FOREIGN KEY (`ar_created_by`) REFERENCES `t_employees` (`emp_id`);

--
-- Constraints for table `t_invoice_d`
--
ALTER TABLE `t_invoice_d`
  ADD CONSTRAINT `FK_T_INVH_ID_T_INVOICE_H` FOREIGN KEY (`invh_id`) REFERENCES `t_invoice_h` (`invh_id`),
  ADD CONSTRAINT `FK_T_PS_ID_T_PRODUCTS_SERVICES` FOREIGN KEY (`ps_id`) REFERENCES `t_products_services` (`ps_id`);

--
-- Constraints for table `t_invoice_h`
--
ALTER TABLE `t_invoice_h`
  ADD CONSTRAINT `FK_T_CREAT_BY_T_EMPLOYEES` FOREIGN KEY (`invh_created_by`) REFERENCES `t_employees` (`emp_id`),
  ADD CONSTRAINT `FK_T_CUST_ID_T_CUSTOMERS` FOREIGN KEY (`cust_id`) REFERENCES `t_customers` (`cust_id`);

--
-- Constraints for table `t_reminders`
--
ALTER TABLE `t_reminders`
  ADD CONSTRAINT `FK_T_INVH_ID_T_INVOICE_H2` FOREIGN KEY (`invh_id`) REFERENCES `t_invoice_h` (`invh_id`);

--
-- Constraints for table `t_vend_cps`
--
ALTER TABLE `t_vend_cps`
  ADD CONSTRAINT `FK_T_VEND_ID_T_VENDORS` FOREIGN KEY (`vend_id`) REFERENCES `t_vendors` (`vend_id`);

DELIMITER $$
--
-- Events
--
DROP EVENT IF EXISTS `e_invh_status`$$
CREATE DEFINER=`root`@`localhost` EVENT `e_invh_status` ON SCHEDULE EVERY 5 MINUTE STARTS '2019-04-26 02:52:46' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE t_invoice_h SET invh_status = 'Overdue' WHERE (invh_status = 'Sent' OR invh_status = 'Partial') AND invh_date_due < CURDATE()$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
