<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Cost Journals</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CCostJournals/index'); ?>">Cost Journals</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Cost Journals List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addCostJournalModal">
                                    <i class="la la-plus"></i>
                                    Add Cost Journal
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add CostJournal -->
                            <div class="modal fade" id="addCostJournalModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span>
                                                <span class="fw-light">
                                                    Cost Journal
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addCostJournalForm" autocomplete="off" method="post" action="<?php echo base_url('CCostJournals/addCostJournal');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new cost journal using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <input type="hidden" id="addEmployeeID" name="edEmpID" value="<?php echo $_SESSION["emp_id"];?>">
                                                            <label>DATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="addCostJournalDate" name="edDate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addCostJournalName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Category <span class="required-label">*</span></label>
                                                            <select id="addCostJournalCategory" name="edCategory" type="text" class="form-control" required>
                                                                <option>Biaya Usaha</option>
                                                                <option>Biaya Umum dan Administrasi Lainnya</option>
                                                                <option>Biaya Lain-lain</option>
                                                                <option>Biaya Admin dan Provisi Bank</option>
                                                                <option>Biaya Bunga Bank</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Amount <span class="required-label">*</span></label>
                                                            <input id="addCostJournalAmount" name="edAmount" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Payment Type <span class="required-label">*</span></label>
                                                            <select id="addCostJournalPaymentType" name="edPaymentType" type="text" class="form-control" required>
                                                                <option>Tunai</option>
                                                                <option>BCA</option>
                                                                <option>BNI</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Notes</label>
                                                            <textarea id="addCostJournalNotes" name="edNotes" class="form-control" rows="5" placeholder="fill notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CCostJournals/addCostJournal');?>" type="submit" name="action" id="AddCostJournalButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add CostJournal -->

                            <!-- Modal Edit CostJournal -->
                            <div class="modal fade" id="editCostJournalModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span>
                                                <span class="fw-light">
                                                    Cost Journal
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editCostJournalForm" autocomplete="off" method="post" action="<?php echo base_url('CCostJournals/editCostJournal');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing cost journal using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <input type="hidden" id="editCostJournalID" name="edID" value="">
                                                            <input type="hidden" id="editEmployeeID" name="edEmpID" value="<?php echo $_SESSION["emp_id"];?>">
                                                            <label>DATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="editCostJournalDate" name="edDate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editCostJournalName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Category <span class="required-label">*</span></label>
                                                            <select id="editCostJournalCategory" name="edCategory" type="text" class="form-control" required>
                                                                <option>Biaya Usaha</option>
                                                                <option>Biaya Umum dan Administrasi Lainnya</option>
                                                                <option>Biaya Lain-lain</option>
                                                                <option>Biaya Admin dan Provisi Bank</option>
                                                                <option>Biaya Bunga Bank</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Amount <span class="required-label">*</span></label>
                                                            <input id="editCostJournalAmount" name="edAmount" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Payment Type <span class="required-label">*</span></label>
                                                            <select id="editCostJournalPaymentType" name="edPaymentType" type="text" class="form-control" required>
                                                                <option>Tunai</option>
                                                                <option>BCA</option>
                                                                <option>BNI</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Notes</label>
                                                            <textarea id="editCostJournalNotes" name="edNotes" class="form-control" rows="5" placeholder="fill notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CCostJournals/editCostJournal');?>" type="submit" name="action" id="editCostJournalButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit CostJournal -->

                            <!-- Modal Delete CostJournal -->
                            <div class="modal fade" id="deleteCostJournalModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span>
                                                <span class="fw-light">
                                                    Cost Journal
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteCostJournalForm" autocomplete="off" method="post" action="<?php echo base_url('CCostJournals/deleteCostJournal');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteCostJournalID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete cost journal <span id="delVendName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CCostJournals/deleteCostJournal');?>" type="submit" name="action" id="deleteCostJournalButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete CostJournal -->

                            <!-- Modal Notifikasi CostJournal-->
                            <div class="modal fade" id="costJournalNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi costJournal -->
                            <div class="table-responsive">
                                <table id="costJournalsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Date</th>
                                            <th>Name</th>
                                            <th style="width: 15%">Amount</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        //Tampilkan Semua CostJournals
                                        // $costJournals dapat dari CCostJournals $data['costJournals']
                                        foreach ($costJournals as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->cj_date ?></td>
                                            <td><?php echo $item->cj_name ?></td>
                                            <td class="amount-out" contenteditable="true" spellcheck="false"><?php echo $item->cj_amount ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editCostJournalModal">
                                                        <button type="button" onclick="editCostJournal(<?php echo $item->cj_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task" <?php if(date("m-Y",strtotime($item->cj_date)) != date('m-Y')) { ?> style="visibility: hidden;" <?php } ?>>
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteCostJournalModal">
                                                        <button type="button" onclick="deleteCostJournal(<?php echo $item->cj_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable" <?php if(date("m-Y",strtotime($item->cj_date)) != date('m-Y')) { ?> style="visibility: hidden;" <?php } ?>>
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    // For Datepicker to actually work
    $('#addCostJournalDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editCostJournalDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the costJournals table as data table to enable search and pagination feature
    $('#costJournalsTable').DataTable( { "order": [[ 0, "desc" ]] } );

    // Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    vMin: '0',
    modifyValueOnWheel: false
    };

    new AutoNumeric.multiple('.amount-out', rupiahFormat);
    new AutoNumeric('#addCostJournalAmount', rupiahFormat);
    editAmount = new AutoNumeric('#editCostJournalAmount', rupiahFormat);
    // Add Modal Form Validation
    $("#addCostJournalForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editCostJournalForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    //$('#costJournalNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CCostJournals/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#costJournalNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CCostJournals/index'); ?>");
    });

    // Filling selected costJournal data to edit costJournal modal
    function editCostJournal(cjID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CCostJournals/getCostJournalByID") ?>',
            data: {
                cjID : cjID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editCostJournalID').val(cjID);
            $('#editCostJournalDate').val(result.cj_date);
            $('#editCostJournalName').val(result.cj_name);
            $('#editCostJournalCategory').val(result.cj_category);
            editAmount.set(result.cj_amount,);
        });

        $.ajax({
            method: "post",
            url: '<?= base_url("CCostJournals/getAccountPayableByCJID") ?>',
            data: {
                cjID : cjID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editCostJournalPaymentType').val(result.ap_payment_type);
            $('#editCostJournalNotes').val(result.ap_notes);
        });
    }
    // Filling selected costJournal name to delete costJournal modal
    function deleteCostJournal(cjID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CCostJournals/getCostJournalNameByID") ?>',
            data: {
                cjID : cjID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteCostJournalID').val(cjID);
            $('#delVendName').html(result.cj_name);
        });
    }
</script>
</body>
</html>
