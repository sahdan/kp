<?php
foreach($reminders as $item){
    $date = new DateTime($item->rem_date_remind." 00:00:00");
    ?>
    <div class="row">
        <div class="col-sm-12 reminder-item">
            <input class="reminder-id" name="edReminderId" type="hidden" value="<?= $item->rem_id ?>"/>
            <?= $date->format("l, jS \of F Y") ?>
            <button style="padding: 5px 7px;" type="button" class="remove-reminder btn btn-link btn-danger" data-toggle="tooltip" title="Remove reminder" class="btn btn-link btn-danger" data-original-title="Remove">
                <i class="la la-times"></i>
            </button>
        </div>
    </div>
    <?php	
}
?>