<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<title>BB Tour & Travel</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico') ?>" type="image/x-icon">

</head>
<body>
    <div class="col-md-12">
        <div class="card card-invoice">
            <form id="addInvoiceForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/editInvoice');?>" enctype="multipart/form-data">
                <div style="width: 600px;">
                    <div class="invoice-header">
                        <input type="hidden" id="editInvoiceHeaderID" name="edInvID" value="<?= $invoiceHeader->invh_id ?>">
                        <h3 style="margin:auto;text-align:center;">
                            Invoice BB-<?= $invoiceHeader->invh_id ?>
                        </h3>
                    </div>
                    <div class="invoice-desc" style="text-align: left; display: inline-block">
                    <strong> BB Tour and Travel Surabaya </strong><br>
                    Apartemen Puri Mas G-07<br>
                    Jl I Gusti Ngurah Rai No 44, Gn. Anyar<br>
                    Surabaya, Jawa Timur 60294<br>
                    Indonesia<br>
                    </div>
                    <div class="invoice-desc" style="text-align: right; float: right; clear: right; display: inline-block">
                    <strong> Contact Information </strong><br>
                    Phone: (031) 582 007 40<br>
                    Fax: (031) 582 007 40<br>
                    Mobile: 0822 9923 8008<br>
                    </div>
                    <hr>
                </div>
                <div class="card-body">
                    <div class="seperator-solid"></div>
                    <div class="row">
                        <p>Invoice Date: <?php echo date_format(date_create($invoiceHeader->invh_date_created),'l jS F Y');?> <br>
                        Payment Due Date: <?php echo date_format(date_create($invoiceHeader->invh_date_due),'l jS F Y');?><br>
                        Invoice To: <?= $invoiceHeader->cust_name ?></p>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="invoice-detail">
                                <div class="invoice-top">
                                    <h3 class="title"><strong>Order summary</strong></h3>
                                </div>
                                <div class="invoice-item">
                                    <div class="table-responsive">
                                        <table style="border-collapse: collapse; border: 1px solid black; padding: 5px; width: 600px;">
                                            <thead>
                                                <tr style="border: 1px solid black; padding: 5px;">
                                                    <td style="border: 1px solid black; padding: 5px; text-align: center;"><strong>Product/Services</strong></td>
                                                    <td style="border: 1px solid black; padding: 5px; text-align: center;"><strong>Price</strong></td>
                                                    <td style="border: 1px solid black; padding: 5px; width: 5%;"><strong>Qty</strong></td>
                                                    <td style="border: 1px solid black; padding: 5px; text-align: center;"><strong>Totals</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row = 1; ?>
                                            <?php $subTotal = 0; ?>
                                            <?php $discount = 0; ?>
                                            <?php foreach ($invoiceDetails as $item) { ?>
                                                <tr id="row_<?= $row ?>">
                                                    <td style="border: 1px solid black; padding: 5px;"><strong><?= $item->ps_name ?></strong><br>
                                                        <?= nl2br($item->invd_description) ?></td>
                                                    <td style="border: 1px solid black; padding: 5px; text-align: right;">Rp. <?= $item->invd_price_display ?></td>
                                                    <td style="border: 1px solid black; padding: 5px; text-align: center;"><?= $item->invd_quantity ?></td>
                                                    <td style="border: 1px solid black; padding: 5px; text-align: right;">Rp. <?= $item->invd_total ?></td>
                                                </tr>
                                            <?php $row++; ?>
                                            <?php $subTotal = $subTotal + $item->invd_total; ?>
                                            <?php $discount = $discount + $item->invd_discount; ?>
                                            <?php } ?>
                                            <?php $total = $subTotal - $discount; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="seperator-solid  mb-3"></div>
                        </div>	
                    </div>
                </div>
                <div style="width: 600px;">
                    <div class="row">
                        <div style="text-align: left; display: inline-block; margin-top: 30px;">
                            <strong>Bank Transfer</strong><br>
                            BCA 7880501164 - Aristyo Wardiono<br>
                            BNI 0795629012 - Aristyo Wardiono<br>
                            BRI 058701000651566 - Aristyo Wardiono
                        </div>
                        <div style="text-align: right; float: right; clear: right; display: inline-block; margin-top: 30px;">
                            <div>
                                <div class="sub" style="text-align: left; float: left; display: inline-block; margin-right: 30px;">Sub Total</div>
                                <div id="subTotal" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;">Rp. <?= $subTotal ?></div>
                            </div>		
                            <div>
                            <div class="sub" style="text-align: left; float: left; display: inline-block; margin-right: 30px;">Discount</div>
                                <div id="discount" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;">Rp. <?= $discount ?></div>
                            </div>	
                            <div>
                                <div class="price" style="text-align: left; float: left; display: inline-block; margin-right: 30px; font-weight: bold;">Total</div>
                                <div id="total" class="price text-right" style=" float: right; clear: right; display: inline-block; font-weight: bold;">Rp. <?= $total ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <footer class="footer" style="clear: right;">
        <div style="width: 600px; margin-top: 30px;">
            <hr>
            <p style="margin:auto;text-align:center;">© 2019 BB Tour & Travel.com | All rights reserved.</p>
        </div>
    </footer>
</body>
</html>