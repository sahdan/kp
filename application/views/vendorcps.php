<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Vendor Contact Persons</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CVendorCPs/index'); ?>">Vendor Contact Persons</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Vendor Contact Persons List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addVendorCPModal">
                                    <i class="la la-plus"></i>
                                    Add Vendor Contact Person
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add VendorCP -->
                            <div class="modal fade" id="addVendorCPModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span> 
                                                <span class="fw-light">
                                                    Vendor Contact Person
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addVendorCPForm" autocomplete="off" method="post" action="<?php echo base_url('CVendorCPs/addVendorCP');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new vendor contact person using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addVendorCPName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="addVendorCPPhone" name="edPhone" type="text" class="form-control" placeholder="fill phone" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Vendor <span class="required-label">*</span></label>
                                                            <select id="addVendorCPVendor" name="edVendor" type="text" class="form-control" required>
                                                                <?php 
                                                                // Tampilkan Semua Vendors
                                                                // $vendors dapat dari CVendorCPs $data['vendors']
                                                                foreach ($vendors as $item) {
                                                                ?>
                                                                <option value="<?php echo $item->vend_id ?>"><?php echo $item->vend_name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Position <span class="required-label">*</span></label>
                                                            <input id="addVendorCPPosition" name="edPosition" type="text" class="form-control" placeholder="fill position" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CVendorCPs/addVendorCP');?>" type="submit" name="action" id="AddVendorCPButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add VendorCP -->
                            
                            <!-- Modal Edit VendorCP -->
                            <div class="modal fade" id="editVendorCPModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span> 
                                                <span class="fw-light">
                                                    VendorCP
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editVendorCPForm" autocomplete="off" method="post" action="<?php echo base_url('CVendorCPs/editVendorCP');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing vendor using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <input type="hidden" id="editVendorCPID" name="edID" value="">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editVendorCPName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="editVendorCPPhone" name="edPhone" type="text" class="form-control" placeholder="fill phone" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Vendor <span class="required-label">*</span></label>
                                                            <select id="editVendorCPVendor" name="edVendor" type="text" class="form-control" required>
                                                                <?php 
                                                                // Tampilkan Semua Vendors
                                                                // $vendors dapat dari CVendorCPs $data['vendors']
                                                                foreach ($vendors as $item) {
                                                                ?>
                                                                <option value="<?php echo $item->vend_id ?>"><?php echo $item->vend_name ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Position <span class="required-label">*</span></label>
                                                            <input id="editVendorCPPosition" name="edPosition" type="text" class="form-control" placeholder="fill position" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CVendorCPs/editVendorCP');?>" type="submit" name="action" id="editVendorCPButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit VendorCP -->

                            <!-- Modal Delete VendorCP -->
                            <div class="modal fade" id="deleteVendorCPModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span> 
                                                <span class="fw-light">
                                                    Vendor Contact Person
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteVendorCPForm" autocomplete="off" method="post" action="<?php echo base_url('CVendorCPs/deleteVendorCP');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteVendorCPID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete vendor contact person <span id="delVendName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CVendorCPs/deleteVendorCP');?>" type="submit" name="action" id="deleteVendorCPButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete VendorCP -->

                            <!-- Modal Notifikasi VendorCP-->
                            <div class="modal fade" id="vendorCPNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi vendorCP -->
                            <div class="table-responsive">
                                <table id="vendorCPsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Vendor</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Vendor</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                        //Tampilkan Semua VendorCPs
                                        // $vendorCPs dapat dari CVendorCPs $data['vendorCPs']
                                        foreach ($vendorCPs as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->vendcp_name ?></td>
                                            <td><?php echo $item->vendcp_phone ?></td>
                                            <td><?php echo $item->vend_name ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editVendorCPModal">
                                                        <button type="button" onclick="editVendorCP(<?php echo $item->vendcp_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteVendorCPModal">
                                                        <button type="button" onclick="deleteVendorCP(<?php echo $item->vendcp_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable">
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    // Set the vendorCPs table as data table to enable search and pagination feature
    $('#vendorCPsTable').DataTable();

    $(document).ready(function(){
        <?php if(isset($selected_vendor)){ ?>
            $('input[aria-controls="vendorCPsTable"]').val('<?php echo $selected_vendor ?>').trigger(jQuery.Event('keyup', { keycode: 13 }));
        <?php } ?>
    });

    // Add Modal Form Validation
    $("#addVendorCPForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editVendorCPForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    // $('#vendorCPNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {        			
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CVendorCPs/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#vendorCPNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CVendorCPs/index'); ?>");
    });
    
    // Filling selected vendorCP data to edit vendorCP modal
    function editVendorCP(vendCPID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CVendorCPs/getVendorCPByID") ?>',
            data: {
                vendCPID : vendCPID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editVendorCPID').val(vendCPID);
            $('#editVendorCPName').val(result.vendcp_name);            
            $('#editVendorCPPhone').val(result.vendcp_phone);
            $('#editVendorCPVendor').val(result.vend_id);
            $('#editVendorCPPosition').val(result.vendcp_position);
        });
    }
    // Filling selected vendorCP name to delete vendorCP modal
    function deleteVendorCP(vendCPID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CVendorCPs/getVendorCPNameByID") ?>',
            data: {
                vendCPID : vendCPID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteVendorCPID').val(vendCPID);
            $('#delVendName').html(result.vendcp_name);
        });
    }
</script>
</body>
</html>