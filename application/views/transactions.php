<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Transactions</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CTransactions/index'); ?>">Transactions</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Transactions List</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="transactionsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Date</th>
                                            <th>Notes</th>
                                            <th style="width: 15%">Amount</th>
                                            <!-- <th style="width: 10%"> Action</th> -->
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <!-- <th>Action</th>  -->
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                        //Tampilkan Semua Transactions
                                        // $transactions dapat dari CTransactions $data['transactions']
                                        foreach ($transactions as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->tran_date ?></td>
                                            <td><?php echo $item->tran_notes ?></td>
                                            <td class="<?php if ($item->tran_type == "ap") { echo 'amount-out'; } else { echo 'amount-in'; } ?>" contenteditable="true" spellcheck="false"><?php echo $item->tran_amount ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>

    // Set the transactions table as data table to enable search and pagination feature
   let table = $('#transactionsTable').DataTable( { "order": [[ 0, "desc" ]] } );

    // Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    //vMin: '0',
    modifyValueOnWheel: false
    };

    new AutoNumeric.multiple('.amount-out', rupiahFormat);
    new AutoNumeric.multiple('.amount-in', rupiahFormat);

    table.on( 'draw', function () {
        new AutoNumeric.multiple('.amount-out', rupiahFormat);
        new AutoNumeric.multiple('.amount-in', rupiahFormat);
    });

    

</script>
</body>
</html>