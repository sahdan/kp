<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Reminders</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CReminders/index'); ?>">Reminders</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Reminders List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addReminderModal">
                                    <i class="la la-plus"></i>
                                    Add Reminder
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add Reminder -->
                            <div class="modal fade" id="addReminderModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span> 
                                                <span class="fw-light">
                                                    Reminder
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addReminderForm" autocomplete="off" method="post" action="<?php echo base_url('CReminders/addReminder');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new reminder using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Invoice ID <span class="required-label">*</span></label>
                                                            <input id="addInvoiceID" name="edInvoiceID" type="text" class="form-control" placeholder="fill invoice id" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <label>DATE TO REMIND <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="addDateToRemind" name="edDateToRemind" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CReminders/addReminder');?>" type="submit" name="action" id="AddReminderButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add Reminder -->
                            
                            <!-- Modal Edit Reminder -->
                            <div class="modal fade" id="editReminderModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span> 
                                                <span class="fw-light">
                                                    Reminder
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editReminderForm" autocomplete="off" method="post" action="<?php echo base_url('CReminders/editReminder');?>" enctype="multipart/form-data">
                                            <input id="editReminderID" type="hidden" name="edID" value="">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing reminder using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Invoice ID <span class="required-label">*</span></label>
                                                            <input id="editInvoiceID" name="edInvoiceID" type="text" class="form-control" placeholder="fill invoice id" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <label>DATE TO REMIND <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="editDateToRemind" name="edDateToRemind" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CReminders/editReminder');?>" type="submit" name="action" id="editReminderButton" class="btn btn-primary">Edit</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit Reminder -->

                            <!-- Modal Delete Reminder -->
                            <div class="modal fade" id="deleteReminderModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span> 
                                                <span class="fw-light">
                                                    Reminder
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteReminderForm" autocomplete="off" method="post" action="<?php echo base_url('CReminders/deleteReminder');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteReminderID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete this reminder?</p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CReminders/deleteReminder');?>" type="submit" name="action" id="deleteReminderButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete Reminder -->

                            <!-- Modal Notifikasi Reminder-->
                            <div class="modal fade" id="reminderNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi Reminder -->
                            <div class="table-responsive">
                                <table id="remindersTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Invoice ID</th>
                                            <th>Date Created</th>
                                            <th>Date To Remind</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Invoice ID</th>
                                            <th>Date Created</th>
                                            <th>Date To Remind</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                        //Tampilkan Semua Reminders
                                        // $reminders dapat dari CReminders $data['reminders']
                                        foreach ($reminders as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->invh_id ?></td>
                                            <td><?php echo $item->rem_date_created ?></td>
                                            <td><?php echo $item->rem_date_remind ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editReminderModal">
                                                        <button type="button" onclick="editReminder(<?php echo $item->rem_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteReminderModal">
                                                        <button type="button" onclick="deleteReminder(<?php echo $item->rem_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable">
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    $('#addDateToRemind').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editDateToRemind').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the reminder table as data table for search and pagination feature
    $('#remindersTable').DataTable();

    // Add Modal Form Validation
    $("#addReminderForm").validate({
        validClass: "success"
	});

    // edit Modal Form Validation
    $("#editReminderForm").validate({
        validClass: "success"
	});

    <?php if(isset($notif_message)) { ?>
    // $('#reminderNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {        			
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CReminders/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#reminderNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CReminders/index'); ?>");
    });

    // Filling selected reminder data to edit reminder modal
    function editReminder(remID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CReminders/getReminderByID") ?>',
            data: {
                remID : remID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editReminderID').val(remID);
            $('#editInvoiceID').val(result.invh_id);
            $('#editDateToRemind').val(result.rem_date_remind);
        });
    }

    // Filling selected reminder name to delete reminder modal
    function deleteReminder(remID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CReminders/getReminderByID") ?>',
            data: {
                remID : remID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteReminderID').val(remID);
        });
    }
</script>
</body>
</html>