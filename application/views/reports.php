<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Reports</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CProducts/index'); ?>">Reports</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Reports</h4>
                                <form id="ReportPeriodeForm" autocomplete="off" method="post" action="<?php echo base_url('CReports/');?>" enctype="multipart/form-data" class="ml-auto">
                                    <select id="reportMonth" name="edMonth" type="text" required>
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select id="reportYear" name="edYear" type="text" required>
                                        <option>2019</option>
                                        <option>2020</option>
                                        <option>2021</option>
                                        <option>2022</option>
                                        <option>2023</option>
                                        <option>2024</option>
                                        <option>2025</option>
                                        <option>2026</option>
                                        <option>2027</option>
                                        <option>2028</option>
                                    </select>
                                    <input type="hidden" name="edAction" value="view">
                                    <button type="submit" name="action" id="generateReportButton" class="btn btn-primary btn-round ml-auto" ><i class="la la-newspaper-o"></i>Generate Reports</button>
                                </form>
                            </div>
                        </div>
                        <?php if ($load) { ?>
                            <div class="card-body">
                            <div style="text-align: center;">
                                <h4>BB Tour Travel</h4>
                                <h5>Perhitungan Laba (Rugi) Bulanan </h5>
                                <h5>Periode <?php echo $bulan ?> <?php echo $year ?></h5>
                            </div>
                            <div class="table-responsive">
                                <table id="reportsTable" class="display table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">No</th>
                                            <th>Keterangan</th>
                                            <th style="width: 15%">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Keterangan</th>
                                            <th>Jumlah</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr style="font-weight: bold;">
                                            <td>1</td>
                                            <td>PENDAPATAN</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>1.1</td>
                                            <td><strong>Pendapatan Operasional</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>1.1.1</td>
                                            <td>Penjualan Tiket Pesawat</td>
                                            <td class="amount-in"><?php echo $flightTicketSales ?></td>
                                        </tr>
                                        <tr>
                                            <td>1.1.2</td>
                                            <td>Penjualan Tiket Kereta</td>
                                            <td class="amount-in"><?php echo $trainTicketSales ?></td>
                                        </tr>
                                        <tr>
                                            <td>1.1.3</td>
                                            <td>Penjualan Voucher Hotel</td>
                                            <td class="amount-in"><?php echo $hotelVoucherSales ?></td>
                                        </tr>
                                        <tr>
                                            <td>1.1.4</td>
                                            <td>Penjualan Paket Tour</td>
                                            <td class="amount-in"><?php echo $tourPackageSales ?></td>
                                        </tr>
                                        <tr>
                                            <td>1.1.5</td>
                                            <td>Penjualan Rent Car</td>
                                            <td class="amount-in"><?php echo $carRentalSales ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>1.1.6</td>
                                            <td>Jumlah Pendapatan Operasional (1.1.1+1.1.2+1.1.3+1.1.4+1.1.5)</td>
                                            <td class="amount-in"><?php echo $operasionalIncomeTotal ?></td>
                                        </tr>
                                        <tr>
                                            <td>1.2</td>
                                            <td>Pendapatan Usaha Lainnya</td>
                                            <td></td>
                                        </tr>
                                        <?php
                                        $ctr =1;
                                        foreach ($otherIncomes as $item) {
                                        ?>
                                        <tr>
                                            <td>1.2.<?php echo $ctr ?></td>
                                            <td><?php echo $item->oi_name ?></td>
                                            <td class="amount-in"><?php echo $item->oi_amount ?></td>
                                        </tr>
                                        <?php
                                        $ctr++;
                                        } ?>
                                        <tr style="font-weight: bold;">
                                            <td>1.2.<?php echo $ctr ?></td>
                                            <td>Jumlah Pendapatan Usaha Lainnya</td>
                                            <td class="amount-in"><?php echo $otherIncomesTotal ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>1.3</td>
                                            <td>Jumlah Pendapatan (1.1.6+1.2.<?php echo $ctr ?>)</td>
                                            <td class="amount-in"><?php echo $incomeTotal ?></td>
                                        </tr>
                                        <tr style="background-color: gray;">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>2</td>
                                            <td><strong>PENGELUARAN</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2.1</td>
                                            <td><strong>Pembelian</strong></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>2.1.1</td>
                                            <td>Pembelian Tiket Pesawat</td>
                                            <td class="amount-out"><?php echo $flightTicketSpending ?></td>
                                        </tr>
                                        <tr>
                                            <td>2.1.1</td>
                                            <td>Pembelian Tiket Kereta</td>
                                            <td class="amount-out"><?php echo $trainTicketSpending ?></td>
                                        </tr>
                                        <tr>
                                            <td>2.1.1</td>
                                            <td>Pembelian Voucher Hotel</td>
                                            <td class="amount-out"><?php echo $hotelVoucherSpending ?></td>
                                        </tr>
                                        <tr>
                                            <td>2.1.1</td>
                                            <td>Pembelian Paket Tour</td>
                                            <td class="amount-out"><?php echo $tourPackageSpending ?></td>
                                        </tr>
                                        <tr>
                                            <td>2.1.1</td>
                                            <td>Pembelian Rent Car</td>
                                            <td class="amount-out"><?php echo $carRentalSpending ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>2.1.5</td>
                                            <td>Jumlah Pengeluaran (2.1.1+2.1.2+2.1.3+2.1.4+2.1.5)</td>
                                            <td class="amount-out"><?php echo $operasionalSpendingTotal ?></td>
                                        </tr>
                                        <tr style="background-color: gray;">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>LABA KOTOR PROJECT (1.3-2.1.5)</td>
                                            <td class="amount-out"><?php echo $grossProfit ?></td>
                                        </tr>
                                        <tr style="background-color: gray;">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>4</td>
                                            <td>BIAYA</td>
                                            <td></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>4.1</td>
                                            <td>Biaya Usaha</td>
                                            <td></td>
                                        </tr>
                                        <?php
                                        $ctr =1;
                                        foreach ($businessCosts as $item) {
                                        ?>
                                        <tr>
                                            <td>4.1.<?php echo $ctr ?></td>
                                            <td><?php echo $item->cj_name ?></td>
                                            <td class="amount-out"><?php echo $item->cj_amount ?></td>
                                        </tr>
                                        <?php
                                        $ctr++;
                                        } ?>
                                        <tr style="font-weight: bold;">
                                            <td>4.1.<?php echo $ctr ?></td>
                                            <td>Jumlah Biaya Usaha</td>
                                            <td class="amount-out"><?php echo $businessCostsTotal ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>4.2</td>
                                            <td>Biaya Umum Dan Administrasi Lainnya</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>4.2.1</td>
                                            <td>Biaya Komisi</td>
                                            <td class="amount-out"><?php echo $commissionsTotal ?></td>
                                        </tr>
                                        <?php
                                        $ctr =2;
                                        foreach ($GeneralAndAdministrativeCosts as $item) {
                                        ?>
                                        <tr>
                                            <td>4.2.<?php echo $ctr ?></td>
                                            <td><?php echo $item->cj_name ?></td>
                                            <td class="amount-out"><?php echo $item->cj_amount ?></td>
                                        </tr>
                                        <?php
                                        $ctr++;
                                        } ?>
                                        <tr style="font-weight: bold;">
                                            <td>4.2.<?php echo $ctr ?></td>
                                            <td>Jumlah Biaya Umum Dan Administrasi Lainnya</td>
                                            <td class="amount-out"><?php echo $GeneralAndAdministrativeCostsTotal ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>4.3</td>
                                            <td>Jumlah Biaya</td>
                                            <td class="amount-out"><?php echo $costsTotal ?></td>
                                        </tr>
                                        <tr style="background-color: gray;">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>5</td>
                                            <td>LABA RUGI USAHA (3-4.3)</td>
                                            <td class="<?php if ($netProfitLoss < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $netProfitLoss ?></td>
                                        </tr>
                                        <tr style="background-color: gray;">
                                        <td colspan="3"></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>6</td>
                                            <td>PENDAPATAN/BIAYA LAIN LAIN BANK</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>6.1</td>
                                            <td>Pendapatan Lain-Lain (Jasa Giro,Bunga,dll)</td>
                                            <td class="amount-in"><?php echo $otherBankIncome ?></td>
                                        </tr>
                                        <tr>
                                            <td>6.2</td>
                                            <td>Biaya Lain-lain</td>
                                            <td class="amount-out"><?php echo $OtherCost ?></td>
                                        </tr>
                                        <tr>
                                            <td>6.3</td>
                                            <td>Biaya Admin dan Provisi bank</td>
                                            <td class="amount-out"><?php echo $bankAdminCost ?></td>
                                        </tr>
                                        <tr>
                                            <td>6.4</td>
                                            <td>Biaya Bunga Bank</td>
                                            <td class="amount-out"><?php echo $bankInterestCost ?></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>6.5</td>
                                            <td>Jumlah Pendapatan dan Biaya Lainnya (6.1-6.2-6.3-6.4)</td>
                                            <td class="<?php if ($otherBankIncomeAndCostTotal < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $otherBankIncomeAndCostTotal ?></td>
                                        </tr>
                                        <tr style="background-color: gray">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Earning Before Tax (5-6.5)</td>
                                            <td class="<?php if ($earningBeforeTax < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $earningBeforeTax ?></td>
                                        </tr>
                                        <tr style="background-color: gray">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>TAX</td>
                                            <td class="<?php if ($tax < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $tax ?></td>
                                        </tr>
                                        <tr style="background-color: gray">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Earning After Tax</td>
                                            <td class="<?php if ($earningAfterTax < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $earningAfterTax ?></td>
                                        </tr>
                                        <tr style="background-color: gray">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Retained Earning</td>
                                            <td class="<?php if ($retainedEarning < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $retainedEarning ?></td>
                                        </tr>
                                        <tr style="background-color: gray">
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>11</td>
                                            <td>NET INCOME</td>
                                            <td class="<?php if ($netIncome < 0) { echo 'amount-out'; } else { echo 'amount-in'; } ?>"><?php echo $netIncome ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <div style="text-align: center;">
                                <h5>Employees Total Sales and Commission</h5>
                                <h5>Periode <?php echo $bulan ?> <?php echo $year ?></h5>
                            </div>
                            <div class="table-responsive">
                                <table id="employeeSalesTable" class="display table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th style="width: 15%">Sales</th>
                                            <th style="width: 15%">Commission</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($employees as $item) { ?>
                                        <tr>
                                            <td><?php echo $item->emp_name ?></td>
                                            <td class="amount-in"><?php echo $item->sales ?></td>
                                            <td class="amount-in"><?php echo $item->commission ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php } else { ?>
                        <p style="text-align: center;">Choose month and year</p>
                        <?php } ?>
                        <?php if ($load) { ?>
                        <div class="card-footer">
                            <div class="row">
                                <button onclick="window.open('<?php echo base_url('CReports/index?edMonth=' . $month . '&edYear=' . $year . '&edAction=print')?>','_blank')" type="button" class="btn btn-primary btn-round" id="printReportButton" style="float: right; margin-right: 15px; margin-bottom: 15px; margin-left: auto;">
                                    <i class="la la-print"></i>
                                    Print
                                </button>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>

    // Set the currency formatting
    const rupiahFormat = {
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    //vMin: '0',
    modifyValueOnWheel: false
    };

    new AutoNumeric.multiple('.amount-out', rupiahFormat);
    new AutoNumeric.multiple('.amount-in', rupiahFormat);

    <?php if ($load) { ?>
    $("#reportMonth").val("<?php echo $month ?>");
    $("#reportYear").val("<?php echo $year ?>");
    <?php } ?>
</script>
</body>
</html>
