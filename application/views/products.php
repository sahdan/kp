<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Products & Services</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CProducts/index'); ?>">Products & Services</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Products & Services List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addProductModal">
                                    <i class="la la-plus"></i>
                                    Add Product & Services
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add Product -->
                            <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span> 
                                                <span class="fw-light">
                                                    Product & Service
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addProductForm" autocomplete="off" method="post" action="<?php echo base_url('CProducts/addProduct');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new product & service using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addProductName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Description <span class="required-label">*</span></label>
                                                            <textarea id="addProductDescription" name="edDescription" type="text" class="form-control" placeholder="fill description" rows="2" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Category <span class="required-label">*</span></label>
                                                            <select id="addProductCategory" name="edCategory" type="text" class="form-control" required>
                                                                <option>Flight Ticket</option>
                                                                <option>Hotel Voucher</option>
                                                                <option>Tour Package</option>
                                                                <option>Car Rental</option>
                                                                <option>Train Ticket</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CProducts/addProduct');?>" type="submit" name="action" id="AddProductButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add Product -->
                            
                            <!-- Modal Edit Product -->
                            <div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span> 
                                                <span class="fw-light">
                                                    Product & Services
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editProductForm" autocomplete="off" method="post" action="<?php echo base_url('CProducts/editProduct');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing product & service using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <input type="hidden" id="editProductID" name="edID" value="">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editProductName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Description <span class="required-label">*</span></label>
                                                            <textarea id="editProductDescription" name="edDescription" type="text" class="form-control" placeholder="fill description" rows="2" required></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Category <span class="required-label">*</span></label>
                                                            <select id="editProductCategory" name="edCategory" type="text" class="form-control" required>
                                                                <option>Flight Ticket</option>
                                                                <option>Hotel Voucher</option>
                                                                <option>Tour Package</option>
                                                                <option>Car Rental</option>
                                                                <option>Train Ticket</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CProducts/editProduct');?>" type="submit" name="action" id="editProductButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit Product -->

                            <!-- Modal Delete Product -->
                            <div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span> 
                                                <span class="fw-light">
                                                    Product & Services
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteProductForm" autocomplete="off" method="post" action="<?php echo base_url('CProducts/deleteProduct');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteProductID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete product & service <span id="delPSName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CProducts/deleteProduct');?>" type="submit" name="action" id="deleteProductButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete Product -->

                            <!-- Modal Notifikasi Product-->
                            <div class="modal fade" id="productNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi product -->
                            <div class="table-responsive">
                                <table id="productsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Category</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Category</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                        //Tampilkan Semua Products
                                        // $products dapat dari CProducts $data['products']
                                        foreach ($products as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->ps_name ?></td>
                                            <td><?php echo $item->ps_description ?></td>
                                            <td><?php echo $item->ps_category ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editProductModal">
                                                        <button type="button" onclick="editProduct(<?php echo $item->ps_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteProductModal">
                                                        <button type="button" onclick="deleteProduct(<?php echo $item->ps_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable">
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    // Set the products table as data table to enable search and pagination feature
    $('#productsTable').DataTable();

    // Add Modal Form Validation
    $("#addProductForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editProductForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    // $('#productNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {        			
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CEmployees/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#productNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CProducts/index'); ?>");
    });

    // Filling selected product data to edit product modal
    function editProduct(psID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CProducts/getProductByID") ?>',
            data: {
                psID : psID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editProductID').val(psID);
            $('#editProductName').val(result.ps_name);
            $('#editProductDescription').val(result.ps_description);
            $('#editProductCategory').val(result.ps_category);
        });
    }
    // Filling selected product name to delete product modal
    function deleteProduct(psID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CProducts/getProductNameByID") ?>',
            data: {
                psID : psID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteProductID').val(psID);
            $('#delPSName').html(result.ps_name);
        });
    }
</script>
</body>
</html>