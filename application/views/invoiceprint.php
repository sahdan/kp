<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('header.php');
?>
<body>
    <div class="col-md-12">
        <div class="card card-invoice">
            <form id="addInvoiceForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/editInvoice');?>" enctype="multipart/form-data">
                <div class="card-header">
                    <div class="invoice-header">
                        <input type="hidden" id="editInvoiceHeaderID" name="edInvID" value="<?= $invoiceHeader->invh_id ?>">
                        <h3 class="invoice-title">
                            Invoice BB-<?= $invoiceHeader->invh_id ?>
                        </h3>
                        <div class="invoice-logo">
                            <img src="<?php echo base_url('/assets/img/logoinvoice.png'); ?>" alt="company logo">
                        </div>
                    </div>
                    <div class="invoice-desc" style="text-align: left; display: inline-block">
                    <strong> BB Tour and Travel Surabaya </strong><br>
                    Apartemen Puri Mas G-07<br>
                    Jl I Gusti Ngurah Rai No 44, Gn. Anyar<br>
                    Surabaya, Jawa Timur 60294<br>
                    Indonesia<br>
                    </div>
                    <div class="invoice-desc" style="text-align: right; float: right; clear: right; display: inline-block">
                    <strong> Contact Information </strong><br>
                    Phone: (031) 582 007 40<br>
                    Fax: (031) 582 007 40<br>
                    Mobile: 0822 9923 8008<br>
                    </div>
                </div>
                <div class="card-body">
                    <div class="seperator-solid"></div>
                    <div class="row">
                        <div class="col-md-4 info-invoice">
                            <div class="form-group form-show-validation">
                                <label>Invoice Date </label>
                                <p><?php echo date_format(date_create($invoiceHeader->invh_date_created),'l jS F Y');?></p>
                            </div>
                        </div>
                        <div class="col-md-4 info-invoice">
                            <div class="form-group form-show-validation">
                                <label>Payment Due Date </label>
                                <p><?php echo date_format(date_create($invoiceHeader->invh_date_due),'l jS F Y');?></p>
                            </div>
                        </div>
                        <div class="col-md-4 info-invoice">
                            <div class="form-group form-show-validation">
                                <label>Invoice To <span class="required-label">*</span></label>
                                <p><?= $invoiceHeader->cust_name ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="invoice-detail">
                                <div class="invoice-top">
                                    <h3 class="title"><strong>Order summary</strong></h3>
                                </div>
                                <div class="invoice-item">
                                    <div class="table-responsive">
                                        <table class="table table-striped" id="invoiceDetailsTable">
                                            <thead>
                                                <tr>
                                                    <td class="text-center" style="display:none;"><strong>Product/Service ID</strong></td>
                                                    <td><strong>Product/Services</strong></td>
                                                    <td class="text-center" style="display:none;"><strong>Cost</strong></td>
                                                    <td class="text-right"><strong>Price</strong></td>
                                                    <td class="text-center" style="display:none;"><strong>Actual Price</strong></td>
                                                    <td class="text-center" style="width: 5%;"><strong>Qty</strong></td>
                                                    <td class="text-right"><strong>Totals</strong></td>
                                                    <td class="text-center" style="display:none;"><strong>Commission</strong></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $row = 1; ?>
                                            <?php $subTotal = 0; ?>
                                            <?php $discount = 0; ?>
                                            <?php foreach ($invoiceDetails as $item) { ?>
                                                <tr id="row_<?= $row ?>">
                                                    <td style="display:none;"><?= $item->ps_id ?> <input type="hidden" name="hiddenProductID[]" id="productID<?= $row ?>" value="<?= $item->ps_id ?>" /></td>
                                                    <td><strong><?= $item->ps_name ?></strong><br>
                                                        <?= nl2br($item->invd_description) ?><textarea name="hiddenDescription[]" id="description<?= $row ?>" style="display:none;"><?= $item->invd_description ?></textarea></td>
                                                    <td style="display:none;"><?= $item->invd_cost ?> <input type="hidden" name="hiddenCost[]" id="cost<?= $row ?>" value="<?= $item->invd_cost ?>" /></td>
                                                    <td class="text-right"><span class="currency-row-<?= $row ?>"><?= $item->invd_price_display ?></span> <input type="hidden" name="hiddenDisplayPrice[]" id="displayPrice<?= $row ?>" value="<?= $item->invd_price_display ?>" /></td>
                                                    <td style="display:none;"><?= $item->invd_price_actual ?> <input type="hidden" name="hiddenActualPrice[]" id="actualPrice<?= $row ?>" value="<?= $item->invd_price_actual ?>" /></td>
                                                    <td class="text-center"><?= $item->invd_quantity ?> <input type="hidden" name="hiddenQuantity[]" id="quantity<?= $row ?>" value="<?= $item->invd_quantity ?>" /></td>
                                                    <td class="text-right"><span class="currency-row-<?= $row ?>"><?= $item->invd_total ?></span> <input type="hidden" name="hiddenTotal[]" id="total<?= $row ?>" value="<?= $item->invd_total ?>" /></td>
                                                    <td style="display:none;"><?= $item->invd_commission ?> <input type="hidden" name="hiddenCommission[]" id="commission<?= $row ?>" value="<?= $item->invd_commission ?>" /></td>
                                                </tr>
                                            <?php $row++; ?>
                                            <?php $subTotal = $subTotal + $item->invd_total; ?>
                                            <?php $discount = $discount + $item->invd_discount; ?>
                                            <?php } ?>
                                            <?php $total = $subTotal - $discount; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="seperator-solid  mb-3"></div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-6 transfer-to">
                            <h5 class="sub">Bank Transfer</h5>
                            <div class="bank-notes">
                                <p>BCA 7880501164 - Aristyo Wardiono<br>
                                BNI 0795629012 - Aristyo Wardiono<br>
                                BRI 058701000651566 - Aristyo Wardiono</p>
                            </div>
                        </div>
                        <div class="col-md-6 transfer-total">

                            <div>
                                <div class="sub" style="text-align: left; display: inline-block; margin-right: 30px;">Sub Total</div>
                                <div id="subTotal" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;"><?= $subTotal ?></div>
                            </div>
                            <div>
                            <div class="sub" style="text-align: left; display: inline-block; margin-right: 30px;">Discount</div>
                                <div id="discount" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;"><?= $discount ?></div>
                            </div>
                            <div>
                                <div class="price" style="text-align: left; display: inline-block; margin-right: 30px;">Total</div>
                                <div id="total" class="price text-right" style="float: right; clear: right; display: inline-block;"><?= $total ?></div>
                            </div>
                        </div>
                    </div>
                    <?php if($paymentHistory){?>
                    <div>
                        <div>
                            <?php foreach($paymentHistory as $item){ ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="float-right">
                                        <span class="float-left" style="margin-right:30px;margin-bottom:10px;">
                                            <strong>Payment on
                                            <?php
                                            echo date("l, jS \of F Y ", strtotime($item->ar_date));
                                            switch(strtolower($item->ar_payment_type))
                                            {
                                                case 'bni':
                                                echo "using a bank payment(BNI)";
                                                break;
                                                case 'bca':
                                                echo "using a bank payment(BCA)";
                                                break;
                                                case 'tunai':
                                                echo "using cash";
                                                break;
                                            }
                                            ?>
                                            :
                                            </strong>
                                        </span>
                                        <div class="currency float-right" style="text-align:right; min-width:100px;">
                                            <span><?= $item->ar_amount ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <hr/>
                    <div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="float-right">
                                    <span class="price float-left" style="font-size: 28px;color: #177dff;padding: 7px 0;font-weight: 600; margin-right:30px;">Amount Due</span>
                                    <div class="currency float-right" style="text-align:right; font-size: 28px;color: #177dff;padding: 7px 0;font-weight: 600;">
                                        <span><?= $amountDue ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php include('footer.php');?>
    <script>
        // Set the currency formatting
        const rupiahFormat = {
        allowDecimalPadding: false,
        currencySymbol: "Rp. ",
        decimalCharacter: ",",
        decimalPlaces: 0,
        digitGroupSeparator: ".",
        //minimumValue: '0',
        modifyValueOnWheel: false
        };

        <?php $row--; ?>
        <?php for ($i = 1; $i <= $row; $i++) { ?>
            new AutoNumeric.multiple('.currency-row-<?= $i ?>', rupiahFormat);
        <?php } ?>
        new AutoNumeric.multiple('.currency', rupiahFormat);
        anSubTotal = new AutoNumeric('#subTotal', rupiahFormat);
        anDiscount = new AutoNumeric('#discount', rupiahFormat);
        anTotal = new AutoNumeric('#total', rupiahFormat);

        setTimeout(function(){window.print();},1000);
    </script>
</body>
</html>
