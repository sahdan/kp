<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Invoices</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CInvoices/index'); ?>">Invoices</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5>Total Overdue : <span class="currency"><?= $totalOverdue ?></span></h5>
                            <h5>Overdue Within 30 Days : <span class="currency"><?= $totalOverdueIn30Days ?></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Invoices List</h4>
                                <button class="btn btn-primary btn-round ml-auto" onclick="window.location.href = '<?php echo base_url('CInvoices/add'); ?>';">
                                    <i class="la la-plus"></i>
                                    Add Invoice
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Delete Invoice -->
                            <div class="modal fade" id="deleteInvoiceModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Void</span>
                                                <span class="fw-light">
                                                    Invoice
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteInvoiceForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/deleteInvoice');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteInvoiceID" name="edID" value="">
                                                <p class="small">Are you sure you want to void invoice BB-<span id="delInvId"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CInvoices/deleteInvoice');?>" type="submit" name="action" id="deleteInvoiceButton" class="btn btn-danger">Void</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete Invoice -->

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="unpaid-tab" data-toggle="tab" href="#pane-unpaid" role="tab" aria-controls="unpaid" aria-selected="true">Unpaid</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="all-tab" data-toggle="tab" href="#pane-all" role="tab" aria-controls="all" aria-selected="false">All</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="draft-tab" data-toggle="tab" href="#pane-draft" role="tab" aria-controls="draft" aria-selected="false">Draft</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane" id="pane-unpaid" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="table-responsive">
                                        <br/>
                                        <table id="invoiceUnpaidsTable" class="display table table-striped table-hover" >
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Status</th>
                                                    <th style="width: 10%">Due Date</th>
                                                    <th style="width: 10%">Number</th>
                                                    <th>Customer</th>
                                                    <th style="width: 15%">Amount Due</th>
                                                    <!-- <th style="width: 10%">Amount Due</th> -->
                                                    <th style="width: 10%"> Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Status</th>
                                                    <th>Date</th>
                                                    <th>Number</th>
                                                    <th>Customer</th>
                                                    <th>Total</th>
                                                    <!-- <th>Amount Due</th> -->
                                                    <th> Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                //Tampilkan Semua Invoices
                                                // $unpaids dapat dari CInvoices $data['unpaids']
                                                foreach ($unpaids as $item) {
                                                ?>
                                                <tr class="<?php echo "tr-".strtolower($item->invh_status) ?>" >
                                                    <td><button onclick="location.href='<?php echo base_url('CInvoices/view/' . $item->invh_id)?>'" class="btn btn-block"><?php echo $item->invh_status ?></button></td>
                                                    <td><?php echo $item->invh_date_due ?></td>
                                                    <td><a href="<?php echo base_url('CInvoices/view/' . $item->invh_id) ?>">BB-<?php echo $item->invh_id ?></a></td>
                                                    <td><?php echo $item->cust_name ?></td>
                                                    <td class="amount-out" spellcheck="false"><?php echo $item->amount_due ?></td>
                                                    <td>
                                                        <div class="form-button-action">
                                                            <button type="button" onclick="location.href='<?php echo base_url('CInvoices/edit/' . $item->invh_id)?>'" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task" <?php if(strtolower($item->invh_status) != "draft"){ ?> style="visibility: hidden;" <?php } ?>>
                                                                <i class="la la-edit"></i>
                                                            </button>
                                                            <span data-toggle="modal" data-target="#deleteInvoiceModal">
                                                                <button type="button" onclick="deleteInvoice(<?php echo $item->invh_id ?>)" data-toggle="tooltip" title="Void" class="btn btn-link btn-danger" data-original-title="Remove" <?php if($_SESSION["emp_access_level"] == "0" or (date("m-Y",strtotime($item->invh_date_created)) != date('m-Y'))) { ?> style="visibility: hidden;" <?php } ?>>
                                                                    <i class="la la-times"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane active" id="pane-all" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="table-responsive">
                                        <br/>
                                        <table id="invoicesTable" class="display table table-striped table-hover" >
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Status</th>
                                                    <th style="width: 10%">Date</th>
                                                    <th style="width: 10%">Number</th>
                                                    <th>Customer</th>
                                                    <th style="width: 15%">Total</th>
                                                    <!-- <th style="width: 10%">Amount Due</th> -->
                                                    <th style="width: 10%"> Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Status</th>
                                                    <th>Date</th>
                                                    <th>Number</th>
                                                    <th>Customer</th>
                                                    <th>Total</th>
                                                    <!-- <th>Amount Due</th> -->
                                                    <th> Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                //Tampilkan Semua Invoices
                                                // $invoices dapat dari CInvoices $data['invoices']
                                                foreach ($invoices as $item) {
                                                ?>
                                                <tr class="<?php echo "tr-".strtolower($item->invh_status) ?>" >
                                                    <td><?php if(strtolower($item->invh_status) != "draft"){ ?>
                                                            <button onclick="location.href='<?php echo base_url('CInvoices/view/' . $item->invh_id)?>'" class="btn btn-block"><?php echo $item->invh_status ?></button>
                                                        <?php } else { ?>
                                                            <button class="btn btn-block" disabled><?php echo $item->invh_status ?></button>
                                                        <?php } ?>
                                                    </td>
                                                    <td><?php echo $item->invh_date_created ?></td>
                                                    <td><?php if(strtolower($item->invh_status) != "draft"){ ?>
                                                        <a href="<?php echo base_url('CInvoices/view/' . $item->invh_id) ?>">BB-<?php echo $item->invh_id ?></a>
                                                        <?php } else { ?>
                                                        BB-<?php echo $item->invh_id ?>
                                                        <?php } ?>
                                                    </td>
                                                    <td><?php echo $item->cust_name ?></td>
                                                    <td class="amount-in" spellcheck="false"><?php echo $item->invh_total ?></td>
                                                    <td>
                                                        <div class="form-button-action">
                                                            <button type="button" onclick="location.href='<?php echo base_url('CInvoices/edit/' . $item->invh_id)?>'" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task" <?php if(strtolower($item->invh_status) != "draft"){ ?> style="visibility: hidden;" <?php } ?>>
                                                                <i class="la la-edit"></i>
                                                            </button>
                                                            <span data-toggle="modal" data-target="#deleteInvoiceModal">
                                                                <button type="button" onclick="deleteInvoice(<?php echo $item->invh_id ?>)" data-toggle="tooltip" title="Void" class="btn btn-link btn-danger" data-original-title="Remove" <?php if(($_SESSION["emp_access_level"] == "0" and strtolower($item->invh_status) != "draft") or (date("m-Y",strtotime($item->invh_date_created)) != date('m-Y'))) { ?> style="visibility: hidden;" <?php } ?>>
                                                                    <i class="la la-times"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="pane-draft" role="tabpanel" aria-labelledby="messages-tab">
                                    <div class="table-responsive">
                                        <br/>
                                        <table id="invoiceDraftsTable" class="display table table-striped table-hover" >
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Status</th>
                                                    <th style="width: 10%">Date</th>
                                                    <th style="width: 10%">Number</th>
                                                    <th>Customer</th>
                                                    <th style="width: 15%">Total</th>
                                                    <!-- <th style="width: 10%">Amount Due</th> -->
                                                    <th style="width: 10%"> Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Status</th>
                                                    <th>Date</th>
                                                    <th>Number</th>
                                                    <th>Customer</th>
                                                    <th>Total</th>
                                                    <!-- <th>Amount Due</th> -->
                                                    <th> Action</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php
                                                //Tampilkan Semua Draft Invoices
                                                // $drafts dapat dari CInvoices $data['invoices']
                                                foreach ($drafts as $item) {
                                                ?>
                                                <tr class="<?php echo "tr-".strtolower($item->invh_status) ?>" <?php if(strtolower($item->invh_status) == "paid"){ ?> style="display:none;" <?php } ?> >
                                                    <td><button class="btn btn-block" disabled><?php echo $item->invh_status ?></button></td>
                                                    <td><?php echo $item->invh_date_created ?></td>
                                                    <td>BB-<?php echo $item->invh_id ?></td>
                                                    <td><?php echo $item->cust_name ?></td>
                                                    <td class="amount-in" spellcheck="false"><?php echo $item->invh_total ?></td>
                                                    <td>
                                                        <div class="form-button-action">
                                                            <button type="button" onclick="location.href='<?php echo base_url('CInvoices/edit/' . $item->invh_id)?>'" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                                <i class="la la-edit"></i>
                                                            </button>
                                                            <span data-toggle="modal" data-target="#deleteInvoiceModal">
                                                                <button type="button" onclick="deleteInvoice(<?php echo $item->invh_id ?>)" data-toggle="tooltip" title="Void" class="btn btn-link btn-danger" data-original-title="Remove">
                                                                    <i class="la la-times"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php');?>

<script>
    // For Datepicker to actually work
    $('#addInvoiceDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editInvoiceDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the invoices table as data table to enable search and pagination feature
    $('#invoiceUnpaidsTable').DataTable({
        "bAutoWidth": false,
        "order": [[ 1, "asc" ]]
    });

    $('#invoicesTable').DataTable({
        "bAutoWidth": false,
        "order": [[ 1, "desc" ]]
    });

    $('#invoiceDraftsTable').DataTable({
        "bAutoWidth": false,
        "order": [[ 1, "desc" ]]
    });

    $(document).ready(function() {
        if($('tbody tr:visible').length == 0){
            $('tbody').append('<tr class="odd no-data"> <td valign="top" colspan="6" class="dataTables_empty">No data available in table</td> </tr>');
        }
    });

    $('#unpaid-tab').on('click', function(event){
        $('.tr-paid').css("display","none");
        if($('tbody tr:visible').length == 0){
            $('tbody').append('<tr class="odd no-data"> <td valign="top" colspan="6" class="dataTables_empty">No data available in table</td> </tr>');
        }else{
            $('.no-data').remove();
        }
    });

    $('#all-tab').on('click', function(event){
        $('tbody tr').css("display","table-row");
        if($('tbody tr:visible').length == 0){
            $('tbody').append('<tr class="odd no-data"> <td valign="top" colspan="6" class="dataTables_empty">No data available in table</td> </tr>');
        }else{
            $('.no-data').remove();
        }
    });

    // Set Invoice Status Color for each row
    $('td .btn-block').each(function(i, obj) {
        if($(this).text() == "Sent") {
            $(this).addClass("btn-primary");
        } else if ($(this).text() == "Partial") {
            $(this).addClass("btn-warning");
        } else if ($(this).text() == "Overdue") {
            $(this).addClass("btn-danger");
        } else if ($(this).text() == "Draft") {
            $(this).addClass("btn-dark");
        } else {
            $(this).addClass("btn-success");
        }
    });

    // Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    // minimumValue: '0',
    modifyValueOnWheel: false
    };

    new AutoNumeric.multiple('.currency', rupiahFormat);
    new AutoNumeric.multiple('.amount-out', rupiahFormat);
    new AutoNumeric.multiple('.amount-in', rupiahFormat);

    <?php if(isset($notif_message)) { ?>
    swal("<?= $notif_message ?>", {
        icon : "success",
        buttons: {
            confirm: {
                className : 'btn btn-success'
            }
        },
    });
    <?php } ?>

    // Filling selected invoice name to delete invoice modal
    function deleteInvoice(invID) {
        $('#deleteInvoiceID').val(invID);
        $('#delInvId').html(invID);
    }
</script>
</body>
</html>
