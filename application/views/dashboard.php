<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
	<div class="content">
		<div class="container-fluid">
			<div class="page-header">
				<h4 class="page-title">Dashboard</h4>
				<button onclick="window.open('<?php echo base_url('CDashboard/print')?>','Dashboard','resizable,width=960','_blank')" type="button" class="btn btn-primary btn-round" id="printDashboardButton" style="float: right; margin-right: 15px; margin-bottom: 15px; margin-left: auto;">
						<i class="la la-print"></i>
						Print
				</button>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-3">
					<div class="card card-stats card-primary card-round">
						<div class="card-body ">
							<div class="row">
								<div class="col-5">
									<div class="icon-big text-center">
										<i class="flaticon-users"></i>
									</div>
								</div>
								<div class="col-7 col-stats">
									<div class="numbers">
										<p class="card-category">New Customers</p>
										<h4 class="card-title"><?php echo $newCustomersCount ?></h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="card card-stats card-success card-round">
						<div class="card-body ">
							<div class="row">
								<div class="col-5">
									<div class="icon-big text-center">
										<i class="flaticon-graph"></i>
									</div>
								</div>
								<div class="col-7 col-stats">
									<div class="numbers">
										<p class="card-category">Sales</p>
										<h4 class="card-title currency"><?php echo $sales ?></h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3">
					<div class="card card-stats card-secondary card-round">
						<div class="card-body ">
							<div class="row">
								<div class="col-5">
									<div class="icon-big text-center">
										<i class="flaticon-success"></i>
									</div>
								</div>
								<div class="col-7 col-stats">
									<div class="numbers">
										<p class="card-category">Order</p>
										<h4 class="card-title"><?php echo $invoicesCount ?></h4>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-card-no-pd">
				<div class="col-md-8">
					<div class="card">
						<div class="card-header">
							<div class="card-head-row">
								<div class="card-title">Sales in the last 3 months</div>
							</div>
						</div>
						<div class="card-body">
							<div class="chart-container">
								<canvas id="salesChart"></canvas>
							</div>
							<div id="myChartLegend"></div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Categories Percentage</h4>
							<p class="card-category">
							Categories sales percentage last 3 months</p>
						</div>
						<div class="card-body">
							<div class="chart-container">
								<canvas id="categoriesChart"></canvas>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-card-no-pd">
				<div class="col-md-4 mt-3 mb-3">
					<div class="card">
						<div class="card-body">
							<p class="fw-mediumbold mt-1">Unpaid Total</p>
							<h4 class="text-primary currency"><?php echo $unpaidTotal ?></h4>
						</div>
					</div>
				</div>
				<div class="col-md-8 mt-3 mb-3">
					<div class="card">
						<div class="card-body">
							<div class="progress-card">
								<div class="d-flex justify-content-between mb-1">
									<span class="text-muted">Sales</span>
									<span class="text-muted fw-bold currency"> <?php echo $sales ?></span>
								</div>
								<div class="progress mb-2" style="height: 7px;">
									<div class="progress-bar bg-success" role="progressbar" style="width: <?php echo floor($sales / 50000000 * 100) ?>%" aria-valuenow="<?php echo floor($sales / 50000000 * 100) ?>" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="<?php echo floor($sales / 50000000 * 100) ?>%"></div>
								</div>
							</div>
							<div class="progress-card">
								<div class="d-flex justify-content-between mb-1">
									<span class="text-muted">Orders</span>
									<span class="text-muted fw-bold"><?php echo $invoicesCount ?></span>
								</div>
								<div class="progress mb-2" style="height: 7px;">
									<div class="progress-bar bg-info" role="progressbar" style="width: <?php echo floor($invoicesCount / 50 * 100) ?>%" aria-valuenow="<?php echo floor($invoicesCount / 50 * 100) ?>" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="<?php echo floor($invoicesCount / 50 * 100) ?>%"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-card-no-pd">
				<div class="col-md-6 mt-3 mb-3">
					<div class="card">
						<div class="card-body">
							<p class="fw-mediumbold mt-1">Employees Total Sales and Commission</p>
							<div class="table-responsive">
								<table id="employeeSalesTable" class="display table table-head-bg-primary table-striped table-hover">
										<thead>
												<tr>
														<th>Name</th>
														<th style="width: 20%; text-align: right;">Sales</th>
														<th style="width: 20%; text-align: right;">Commission</th>
												</tr>
										</thead>
										<tbody>
												<?php foreach ($employees as $item) { ?>
												<tr>
														<td><?php echo $item->emp_name ?></td>
														<td class="currency"><?php echo $item->sales ?></td>
														<td class="currency"><?php echo $item->commission ?></td>
												</tr>
												<?php } ?>
										</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 mt-3 mb-3">
					<div class="card">
						<div class="card-body">
							<p class="fw-mediumbold mt-1">Product and Services Total Orders and Sales last 3 months</p>
							<div class="table-responsive">
								<table id="employeeSalesTable" class="display table table-head-bg-primary table-striped table-hover">
										<thead>
												<tr>
														<th>Name</th>
														<th style="width: 10%; text-align: center;">Orders</th>
														<th style="width: 20%; text-align: right;">Sales</th>
												</tr>
										</thead>
										<tbody>
												<?php foreach ($products as $item) { ?>
												<tr>
														<td><?php echo $item->ps_name ?></td>
														<td style="text-align: center;"><?php echo $item->count ?></td>
														<td class="currency"><?php echo $item->total ?></td>
												</tr>
												<?php } ?>
										</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('footer.php'); ?>
	<script>
		// Set the currency formatting
		const rupiahFormat = {
		allowDecimalPadding: false,
		currencySymbol: "Rp. ",
		decimalCharacter: ",",
		decimalPlaces: 0,
		digitGroupSeparator: ".",
		// minimumValue: '0',
		modifyValueOnWheel: false
		};

		new AutoNumeric.multiple('.currency', rupiahFormat);

		// Generate Line Chart
		var lineHTMLElement = document.getElementById('salesChart').getContext('2d');
		var salesChart = new Chart(lineHTMLElement, {
			type: 'line',
			data: {
			labels: [<?php foreach ($salesTotal as $item) { ?> "<?php echo $item->Month ?>", <?php } ?>],
				datasets: [{
					label: "Sales in million",
					borderColor: "#1d7af3",
					pointBorderColor: "#FFF",
					pointBackgroundColor: "#1d7af3",
					pointBorderWidth: 2,
					pointHoverRadius: 4,
					pointHoverBorderWidth: 1,
					pointRadius: 4,
					backgroundColor: 'transparent',
					fill: true,
					borderWidth: 2,
					data: [<?php foreach ($salesTotal as $item) { echo $item->Total . ","; } ?>]
				}]
			},
			options : {
				responsive: true,
				maintainAspectRatio: false,
				legend: {
					position: 'bottom',
					labels : {
						padding: 10,
						fontColor: '#1d7af3',
					}
				},
				tooltips: {
					bodySpacing: 4,
					mode:"nearest",
					intersect: 0,
					position:"nearest",
					xPadding:10,
					yPadding:10,
					caretPadding:10
				},
				layout:{
					padding:{left:15,right:15,top:15,bottom:15}
				}
			}
		});

		// Generate Pie Chart
		var pieHTMLElement = document.getElementById('categoriesChart').getContext('2d');
		var categoriesChart = new Chart(pieHTMLElement, {
			type: 'pie',
			data: {
				datasets: [{
					data: [<?php echo $categoriesTotal->car ?>, <?php echo $categoriesTotal->flight ?>, <?php echo $categoriesTotal->hotel ?>, <?php echo $categoriesTotal->tour ?>, <?php echo $categoriesTotal->train ?>],
					"backgroundColor":["rgb(23, 125, 255)","rgb(255, 100, 109)","rgb(253, 190, 70)","rgb(89,208,93)", "rgb(128,0,128)"],
					borderWidth: 0
				}],
				labels: ['Car Rental', 'Flight Ticket' , 'Hotel Voucher', 'Tour Packages', 'Train Ticket']
			},
			options : {
				responsive: true,
				maintainAspectRatio: false,
				legend: {
					position : 'bottom',
					labels : {
						fontColor: 'rgb(154, 154, 154)',
						fontSize: 11,
						usePointStyle : true,
						padding: 20
					}
				},
				pieceLabel: {
					render: 'percentage',
					fontColor: 'white',
					fontSize: 14,
				},
				tooltips: false,
				layout: {
					padding: {
						left: 20,
						right: 20,
						top: 20,
						bottom: 20
					}
				}
			}
		});
		</script>
</div>
</body>
</html>
