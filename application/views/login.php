<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require('header.php');
?>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
            <h3 class="text-center">Sign In To Admin</h3>
            <div class="alert alert-danger" role="alert" style="display: none">
                Username/Password Combination not found
            </div>
            <form method="post" action="<?php echo base_url('CLogin/login');?>">
                <div class="login-form">
                    <div class="form-group form-floating-label">
                        <input id="username" name="edUsername" type="text" class="form-control input-border-bottom" autocomplete="username" required>
                        <label for="username" class="placeholder">Username</label>
                    </div>
                    <div class="form-group form-floating-label">
                        <input id="password" name="edPassword" type="password" class="form-control input-border-bottom" autocomplete="current-password" required>
                        <label for="password" class="placeholder">Password</label>
                        <div class="show-password">
                            <i class="flaticon-interface"></i>
                        </div>
                    </div>
                    <div class="form-action">
                        <button href="<?php echo base_url('CLogin/login');?>" class="btn btn-primary btn-rounded btn-login" type="submit" name="action">Sign In</button>
                    </div>
                </div>
            </form>
		</div>
	</div>
	<script src="../assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="../assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="../assets/js/core/popper.min.js"></script>
	<script src="../assets/js/core/bootstrap.min.js"></script>
    <script src="../assets/js/ready.js"></script>
    <script async>
		$(document).ready(function() {
			// Jika gagal login atau berhasil registrasi akan muncul popup
			var url = window.location.href;
			if(url.indexOf("loginFail") != -1){
				$('div.alert').toggle();
			}
		});
	</script>
</body>
</html>
