<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Other Incomes</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('COtherIncomes/index'); ?>">Other Incomes</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Other Incomes List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addOtherIncomeModal">
                                    <i class="la la-plus"></i>
                                    Add Other Income
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add OtherIncome -->
                            <div class="modal fade" id="addOtherIncomeModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span>
                                                <span class="fw-light">
                                                    Other Income
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addOtherIncomeForm" autocomplete="off" method="post" action="<?php echo base_url('COtherIncomes/addOtherIncome');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new other income using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <input type="hidden" id="addEmployeeID" name="edEmpID" value="<?php echo $_SESSION["emp_id"];?>">
                                                            <label>DATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="addOtherIncomeDate" name="edDate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addOtherIncomeName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Category <span class="required-label">*</span></label>
                                                            <select id="addOtherIncomeCategory" name="edCategory" type="text" class="form-control" required>
                                                                <option>Pendapatan Usaha Lainnya</option>
                                                                <option>Pendapatan Lain-Lain (Jasa Giro,Bunga,dll)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Amount <span class="required-label">*</span></label>
                                                            <input id="addOtherIncomeAmount" name="edAmount" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Payment Type <span class="required-label">*</span></label>
                                                            <select id="addOtherIncomePaymentType" name="edPaymentType" type="text" class="form-control" required>
                                                                <option>Tunai</option>
                                                                <option>BCA</option>
                                                                <option>BNI</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Notes</label>
                                                            <textarea id="addOtherIncomeNotes" name="edNotes" class="form-control" rows="5" placeholder="fill notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('COtherIncomes/addOtherIncome');?>" type="submit" name="action" id="AddOtherIncomeButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add OtherIncome -->

                            <!-- Modal Edit OtherIncome -->
                            <div class="modal fade" id="editOtherIncomeModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span>
                                                <span class="fw-light">
                                                    Other Income
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editOtherIncomeForm" autocomplete="off" method="post" action="<?php echo base_url('COtherIncomes/editOtherIncome');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing vendor using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <input type="hidden" id="editOtherIncomeID" name="edID" value="">
                                                            <input type="hidden" id="editEmployeeID" name="edEmpID" value="<?php echo $_SESSION["emp_id"];?>">
                                                            <label>DATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="editOtherIncomeDate" name="edDate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editOtherIncomeName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Category <span class="required-label">*</span></label>
                                                            <select id="editOtherIncomeCategory" name="edCategory" type="text" class="form-control" required>
                                                                <option>Pendapatan Usaha Lainnya</option>
                                                                <option>Pendapatan Lain-Lain (Jasa Giro,Bunga,dll)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Amount <span class="required-label">*</span></label>
                                                            <input id="editOtherIncomeAmount" name="edAmount" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Payment Type <span class="required-label">*</span></label>
                                                            <select id="editOtherIncomePaymentType" name="edPaymentType" type="text" class="form-control" required>
                                                                <option>Tunai</option>
                                                                <option>BCA</option>
                                                                <option>BNI</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Notes</label>
                                                            <textarea id="editOtherIncomeNotes" name="edNotes" class="form-control" rows="5" placeholder="fill notes"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('COtherIncomes/editOtherIncome');?>" type="submit" name="action" id="editOtherIncomeButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit OtherIncome -->

                            <!-- Modal Delete OtherIncome -->
                            <div class="modal fade" id="deleteOtherIncomeModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span>
                                                <span class="fw-light">
                                                    Other Income
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteOtherIncomeForm" autocomplete="off" method="post" action="<?php echo base_url('COtherIncomes/deleteOtherIncome');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteOtherIncomeID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete vendor contact person <span id="delVendName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('COtherIncomes/deleteOtherIncome');?>" type="submit" name="action" id="deleteOtherIncomeButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete OtherIncome -->

                            <!-- Modal Notifikasi OtherIncome-->
                            <div class="modal fade" id="otherIncomeNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi otherIncome -->
                            <div class="table-responsive">
                                <table id="otherIncomesTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Date</th>
                                            <th>Name</th>
                                            <th style="width: 15%">Amount</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        //Tampilkan Semua OtherIncomes
                                        // $otherIncomes dapat dari COtherIncomes $data['otherIncomes']
                                        foreach ($otherIncomes as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->oi_date ?></td>
                                            <td><?php echo $item->oi_name ?></td>
                                            <td class="amount-in" contenteditable="true" spellcheck="false"><?php echo $item->oi_amount ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editOtherIncomeModal">
                                                        <button type="button" onclick="editOtherIncome(<?php echo $item->oi_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task" <?php if(date("m-Y",strtotime($item->oi_date)) != date('m-Y')) { ?> style="visibility: hidden;" <?php } ?>>
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteOtherIncomeModal">
                                                        <button type="button" onclick="deleteOtherIncome(<?php echo $item->oi_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable" <?php if(date("m-Y",strtotime($item->oi_date)) != date('m-Y')) { ?> style="visibility: hidden;" <?php } ?>>
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    // For Datepicker to actually work
    $('#addOtherIncomeDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editOtherIncomeDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the otherIncomes table as data table to enable search and pagination feature
    $('#otherIncomesTable').DataTable( { "order": [[ 0, "desc" ]] } );

    // Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    vMin: '0',
    modifyValueOnWheel: false
    };

    new AutoNumeric.multiple('.amount-in', rupiahFormat);
    new AutoNumeric('#addOtherIncomeAmount', rupiahFormat);
    editAmount = new AutoNumeric('#editOtherIncomeAmount', rupiahFormat);
    // Add Modal Form Validation
    $("#addOtherIncomeForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editOtherIncomeForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    //$('#otherIncomeNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('COtherIncomes/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#otherIncomeNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('COtherIncomes/index'); ?>");
    });

    // Filling selected otherIncome data to edit otherIncome modal
    function editOtherIncome(oiID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("COtherIncomes/getOtherIncomeByID") ?>',
            data: {
                oiID : oiID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editOtherIncomeID').val(oiID);
            $('#editOtherIncomeDate').val(result.oi_date);
            $('#editOtherIncomeName').val(result.oi_name);
            $('#editOtherIncomeCategory').val(result.oi_category);
            editAmount.set(result.oi_amount,);
        });

        $.ajax({
            method: "post",
            url: '<?= base_url("COtherIncomes/getAccountReceivableByOIID") ?>',
            data: {
                oiID : oiID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editOtherIncomePaymentType').val(result.ar_payment_type);
            $('#editOtherIncomeNotes').val(result.ar_notes);
        });
    }
    // Filling selected otherIncome name to delete otherIncome modal
    function deleteOtherIncome(oiID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("COtherIncomes/getOtherIncomeNameByID") ?>',
            data: {
                oiID : oiID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteOtherIncomeID').val(oiID);
            $('#delVendName').html(result.oi_name);
        });
    }
</script>
</body>
</html>
