<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Customers</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CCustomers/index'); ?>">Customers</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Customers List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addCustomerModal">
                                    <i class="la la-plus"></i>
                                    Add Customer
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add Customer -->
                            <div class="modal fade" id="addCustomerModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span>
                                                <span class="fw-light">
                                                    Customer
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addCustomerForm" autocomplete="off" method="post" action="<?php echo base_url('CCustomers/addCustomer');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new customer using this form</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addCustomerName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Title <span class="required-label">*</span> </label>
                                                            <div class="form-check form-check-inline">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Mr" id="addCustomerMr" name="edTitle" class="custom-control-input">
                                                                    <label class="custom-control-label" for="addCustomerMr">Mr</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Ms" id="addCustomerMs" name="edTitle" class="custom-control-input">
                                                                    <label class="custom-control-label" for="addCustomerMs">Ms</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Mrs" id="addCustomerMrs" name="edTitle" class="custom-control-input">
                                                                    <label class="custom-control-label" for="addCustomerMrs">Mrs</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>ID Number<span class="required-label">*</span></label>
                                                            <input id="addCustomerIdNumber" name="edIdNumber" type="text" class="form-control" placeholder="fill ID Number" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>ID Type<span class="required-label">*</span> </label>
                                                            <div class="form-check form-check-inline">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="KTP" id="addCustomerIdKtp" name="edIdType" class="custom-control-input">
                                                                    <label class="custom-control-label" for="addCustomerIdKtp">KTP</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Passport" id="addCustomerIdPassport" name="edIdType" class="custom-control-input">
                                                                    <label class="custom-control-label" for="addCustomerIdPassport">Passport</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="SIM" id="addCustomerIdSim" name="edIdType" class="custom-control-input">
                                                                    <label class="custom-control-label" for="addCustomerIdSim">SIM</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Email</label>
                                                            <input id="addCustomerEmail" name="edEmail" type="text" class="form-control" placeholder="fill email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="addCustomerPhone" name="edPhone" type="text" class="form-control" placeholder="fill phone" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Address</label>
                                                            <input id="addCustomerAddress" name="edAddress" type="text" class="form-control" placeholder="fill address">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <label>BIRTHDATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="addCustomerBirthdate" name="edBirthdate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Emergency Contact Name <span class="required-label">*</span></label>
                                                            <input id="addCustomerEmerName" name="edEmerName" type="text" class="form-control" placeholder="fill emergency contact name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Emergency Contact Phone <span class="required-label">*</span></label>
                                                            <input id="addCustomerEmerPhone" name="edEmerPhone" type="text" class="form-control" placeholder="fill emergency contact phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CCustomers/addCustomer');?>" type="submit" name="action" id="AddCustomerButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add Customer -->

                            <!-- Modal Edit Customer -->
                            <div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span>
                                                <span class="fw-light">
                                                    Customer
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editCustomerForm" autocomplete="off" method="post" action="<?php echo base_url('CCustomers/editCustomer');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing customer using this form</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <input type="hidden" id="editCustomerID" name="edID" value="">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editCustomerName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Title <span class="required-label">*</span> </label>
                                                            <div class="form-check form-check-inline">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Mr" id="editCustomerMr" name="edTitle" class="custom-control-input">
                                                                    <label class="custom-control-label" for="editCustomerMr">Mr</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Ms" id="editCustomerMs" name="edTitle" class="custom-control-input">
                                                                    <label class="custom-control-label" for="editCustomerMs">Ms</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Mrs" id="editCustomerMrs" name="edTitle" class="custom-control-input">
                                                                    <label class="custom-control-label" for="editCustomerMrs">Mrs</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>ID Number<span class="required-label">*</span></label>
                                                            <input id="editCustomerIdNumber" name="edIdNumber" type="text" class="form-control" placeholder="fill ID Number" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>ID Type<span class="required-label">*</span> </label>
                                                            <div class="form-check form-check-inline">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="KTP" id="editCustomerIdKtp" name="edIdType" class="custom-control-input">
                                                                    <label class="custom-control-label" for="editCustomerIdKtp">KTP</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="Passport" id="editCustomerIdPassport" name="edIdType" class="custom-control-input">
                                                                    <label class="custom-control-label" for="editCustomerIdPassport">Passport</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" value="SIM" id="editCustomerIdSim" name="edIdType" class="custom-control-input">
                                                                    <label class="custom-control-label" for="editCustomerIdSim">SIM</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Email</label>
                                                            <input id="editCustomerEmail" name="edEmail" type="text" class="form-control" placeholder="fill email">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="editCustomerPhone" name="edPhone" type="text" class="form-control" placeholder="fill phone" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Address <span class="required-label">*</span></label>
                                                            <input id="editCustomerAddress" name="edAddress" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <label>BIRTHDATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="editCustomerBirthdate" name="edBirthdate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Emergency Contact Name <span class="required-label">*</span></label>
                                                            <input id="editCustomerEmerName" name="edEmerName" type="text" class="form-control" placeholder="fill emergency contact name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Emergency Contact Phone <span class="required-label">*</span></label>
                                                            <input id="editCustomerEmerPhone" name="edEmerPhone" type="text" class="form-control" placeholder="fill emergency contact phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CCustomers/editCustomer');?>" type="submit" name="action" id="editCustomerButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit Customer -->

                            <!-- Modal Delete Customer -->
                            <div class="modal fade" id="deleteCustomerModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span>
                                                <span class="fw-light">
                                                    Customer
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteCustomerForm" autocomplete="off" method="post" action="<?php echo base_url('CCustomers/deleteCustomer');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteCustomerID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete customer <span id="delCustName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CCustomers/deleteCustomer');?>" type="submit" name="action" id="deleteCustomerButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete Customer -->

                            <!-- Modal Notifikasi Customer-->
                            <div class="modal fade" id="customerNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi Customer -->
                            <div class="table-responsive">
                                <table id="customersTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        //Tampilkan Semua Customers
                                        // $customers dapat dari CCustomers $data['customers']
                                        foreach ($customers as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->cust_name ?></td>
                                            <td><?php echo $item->cust_email ?></td>
                                            <td><?php echo $item->cust_phone ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editCustomerModal">
                                                        <button type="button" onclick="editCustomer(<?php echo $item->cust_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteCustomerModal">
                                                        <button type="button" onclick="deleteCustomer(<?php echo $item->cust_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable">
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    $('#addCustomerBirthdate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editCustomerBirthdate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the customer table as data table for search and pagination feature
    $('#customersTable').DataTable();

    // Add Modal Form Validation
    $("#addCustomerForm").validate({
        validClass: "success",
        rules: {
            edBirthdate: {date: true},
            edTitle : {required :true},
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editCustomerForm").validate({
        validClass: "success",
        rules: {
            edBirthdate: {date: true},
            edTitle : {required :true},
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    //$('#customerNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CCustomers/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#customerNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CCustomers/index'); ?>");
    });

    // Filling selected customer data to edit customer modal
    function editCustomer(custID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CCustomers/getCustomerByID") ?>',
            data: {
                custID : custID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editCustomerID').val(custID);
            $('#editCustomerName').val(result.cust_name);
            if(result.cust_title == "Mr") {
                $("#editCustomerMr").prop("checked", true);
            } else if (result.cust_title == "Ms") {
                $("#editCustomerMs").prop("checked", true);
            } else {
                $("#editCustomerMrs").prop("checked", true);
            }
            $('#editCustomerIdNumber').val(result.cust_id_number);
            if(result.cust_id_type == "KTP") {
                $("#editCustomerIdKtp").prop("checked", true);
            } else if (result.cust_id_type == "Passport") {
                $("#editCustomerIdPassport").prop("checked", true);
            } else {
                $("#editCustomerIdSim").prop("checked", true);
            }
            $('#editCustomerEmail').val(result.cust_email);
            $('#editCustomerPhone').val(result.cust_phone);
            $('#editCustomerAddress').val(result.cust_address);
            $('#editCustomerBirthdate').val(result.cust_birthdate);
            $('#editCustomerEmerName').val(result.cust_emer_name);
            $('#editCustomerEmerPhone').val(result.cust_emer_phone);
        });
    }

    // Filling selected customer name to delete customer modal
    function deleteCustomer(custID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CCustomers/getCustomerNameByID") ?>',
            data: {
                custID : custID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteCustomerID').val(custID);
            $('#delCustName').html(result.cust_name);
        });
    }
</script>
</body>
</html>
