<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Commissions</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CInvoices/index'); ?>">Commissions</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Commissions List</h4>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Modal Edit Commission -->
                            <div class="modal fade" id="editCommissionModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span>
                                                <span class="fw-light">
                                                    Commission
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editCommissionForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/editCommission');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing Commission using this form</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <input type="hidden" id="editInvoiceID" name="edInvoiceID" value="">
                                                            <input type="hidden" id="editProductID" name="edProductID" value="">
                                                            Invoice: BB-<span id="invoiceID"></span><br>
                                                            Product & Service: <span id="productName"></span><br>
                                                            Sales: <span id="salesCom"></span><br>
                                                            Profit: <span id="profit"></span><br>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Commission <span class="required-label">*</span></label>
                                                            <input id="editCommissionAmount" name="edAmount" type="tel" class="form-control" placeholder="fill commission" spellcheck="false" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CInvoices/editCommission');?>" type="submit" name="action" id="editCommissionButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit Commission -->

                            <div class="table-responsive">
                                <table id="CommissionsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th style="width: 10%">Date</th>
                                            <th style="width: 5%">No Invoice</th>
                                            <th>Product & Service</th>
                                            <th style="width: 15%">Commission</th>
                                            <th style="width: 5%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th style="width: 10%">Date</th>
                                            <th>No Invoice</th>
                                            <th>Product & Service</th>
                                            <th>Commission</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        //Tampilkan Semua Commissions
                                        // $Commissions dapat dari CInvoices $data['Commissions']
                                        foreach ($commissions as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->invh_date_created ?></td>
                                            <td>BB-<?php echo $item->invh_id ?></td>
                                            <td><?php echo $item->ps_name ?></td>
                                            <td class="amount-in" contenteditable="true" spellcheck="false"><?php echo $item->invd_commission ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editCommissionModal">
                                                        <button type="button" onclick="editCommission(<?php echo $item->invh_id ?>,<?php echo $item->ps_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task" <?php if(date("m-Y",strtotime($item->invh_date_created)) != date('m-Y')) { ?> style="visibility: hidden;" <?php } ?>>
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    // For Datepicker to actually work
    $('#addCommissionDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editCommissionDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the Commissions table as data table to enable search and pagination feature
    $('#CommissionsTable').DataTable( { "order": [[ 0, "desc" ]] } );

    // Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    //vMin: '0',
    modifyValueOnWheel: false
    };

    new AutoNumeric.multiple('.amount-in', rupiahFormat);
    anSales = new AutoNumeric('#salesCom', rupiahFormat);
    anProfit = new AutoNumeric('#profit', rupiahFormat);
    editAmount = new AutoNumeric('#editCommissionAmount', rupiahFormat);

    // edit Modal Form Validation
    $("#editCommissionForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    //$('#CommissionNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {
            confirm: {
                className : 'btn btn-success'
            }
        }
    })
    <?php } ?>

    // Filling selected Commission data to edit Commission modal
    function editCommission(invhID, psID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CInvoices/getInvoiceDetailByIDs") ?>',
            data: {
                invhID : invhID,
                psID : psID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editInvoiceID').val(invhID);
            $('#editProductID').val(psID);
            $('#invoiceID').html(invhID);
            $('#productName').html(result.ps_name);
            let sales = result.invd_price_actual * result.invd_quantity;
            let profit = sales - (result.invd_cost * result.invd_quantity);
            anSales.set(sales);
            anProfit.set(profit);
            editAmount.set(result.invd_commission);
        });
    }
</script>
</body>
</html>
