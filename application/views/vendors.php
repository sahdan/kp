<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Vendors</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CVendors/index'); ?>">Vendors</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Vendors List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addVendorModal">
                                    <i class="la la-plus"></i>
                                    Add Vendor
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add Vendor -->
                            <div class="modal fade" id="addVendorModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span> 
                                                <span class="fw-light">
                                                    Vendor
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addVendorForm" autocomplete="off" method="post" action="<?php echo base_url('CVendors/addVendor');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new vendor using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addVendorName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Email <span class="required-label">*</span></label>
                                                            <input id="addVendorEmail" name="edEmail" type="text" class="form-control" placeholder="fill email" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="addVendorPhone" name="edPhone" type="text" class="form-control" placeholder="fill phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Address <span class="required-label">*</span></label>
                                                            <input id="addVendorAddress" name="edAddress" type="text" class="form-control" placeholder="fill address" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CVendors/addVendor');?>" type="submit" name="action" id="AddVendorButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add Vendor -->
                            
                            <!-- Modal Edit Vendor -->
                            <div class="modal fade" id="editVendorModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span> 
                                                <span class="fw-light">
                                                    Vendor
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editVendorForm" autocomplete="off" method="post" action="<?php echo base_url('CVendors/editVendor');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing vendor using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <input type="hidden" id="editVendorID" name="edID" value="">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editVendorName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Email <span class="required-label">*</span></label>
                                                            <input id="editVendorEmail" name="edEmail" type="text" class="form-control" placeholder="fill position" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="editVendorPhone" name="edPhone" type="text" class="form-control" placeholder="fill office" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Address <span class="required-label">*</span></label>
                                                            <input id="editVendorAddress" name="edAddress" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CVendors/editVendor');?>" type="submit" name="action" id="editVendorButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit Vendor -->

                            <!-- Modal Delete Vendor -->
                            <div class="modal fade" id="deleteVendorModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span> 
                                                <span class="fw-light">
                                                    Vendor
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteVendorForm" autocomplete="off" method="post" action="<?php echo base_url('CVendors/deleteVendor');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteVendorID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete vendor <span id="delVendName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CVendors/deleteVendor');?>" type="submit" name="action" id="deleteVendorButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete Vendor -->

                            <!-- Modal Notifikasi Vendor-->
                            <div class="modal fade" id="vendorNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi vendor -->
                            <div class="table-responsive">
                                <table id="vendorsTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                        //Tampilkan Semua Vendors
                                        // $vendors dapat dari CVendors $data['vendors']
                                        foreach ($vendors as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->vend_name ?></td>
                                            <td><?php echo $item->vend_email ?></td>
                                            <td><?php echo $item->vend_phone ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editVendorModal">
                                                        <button type="button" onclick="editVendor(<?php echo $item->vend_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteVendorModal">
                                                        <button type="button" onclick="deleteVendor(<?php echo $item->vend_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable">
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                    <form method="post" action="<?php echo base_url('CVendorCPs');?>" enctype="multipart/form-data">
                                                        <input type="hidden" value="<?php echo $item->vend_name ?>" name="edVendorName">
                                                        <button type="submit" class="btn btn-link">
                                                            See Contact Persons
                                                        </button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include('footer.php');?>
</div>


<script>
    // Set the vendors table as data table to enable search and pagination feature
    $('#vendorsTable').DataTable();

    // Add Modal Form Validation
    $("#addVendorForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editVendorForm").validate({
        validClass: "success",
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    // $('#vendorNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {        			
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CVendors/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#vendorNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CVendors/index'); ?>");
    });

    // Filling selected vendor data to edit vendor modal
    function editVendor(vendID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CVendors/getVendorByID") ?>',
            data: {
                vendID : vendID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editVendorID').val(vendID);
            $('#editVendorName').val(result.vend_name);
            $('#editVendorEmail').val(result.vend_email);
            $('#editVendorPhone').val(result.vend_phone);
            $('#editVendorAddress').val(result.vend_address);
        });
    }
    // Filling selected vendor name to delete vendor modal
    function deleteVendor(vendID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CVendors/getVendorNameByID") ?>',
            data: {
                vendID : vendID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteVendorID').val(vendID);
            $('#delVendName').html(result.vend_name);
        });
    }
</script>
</body>
</html>