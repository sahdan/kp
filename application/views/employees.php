<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
    <div class="content">
		<div class="container-fluid">
            <div class="page-header">
                <h4 class="page-title">Employees</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php echo base_url('CDashboard/index'); ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('CEmployees/index'); ?>">Employees</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title">Employees List</h4>
                                <button class="btn btn-primary btn-round ml-auto" data-toggle="modal" data-target="#addEmployeeModal">
                                    <i class="la la-plus"></i>
                                    Add Employee
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Modal Add Employee -->
                            <div class="modal fade" id="addEmployeeModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                New</span>
                                                <span class="fw-light">
                                                    Employee
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="addEmployeeForm" autocomplete="off" method="post" action="<?php echo base_url('CEmployees/addEmployee');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Create a new employee using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="addEmployeeName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>ID Card Number (Nomor KTP) <span class="required-label">*</span></label>
                                                            <input id="addEmployeeIdCardNumber" name="edIdCardNumber" type="text" class="form-control" placeholder="fill ID card number" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Email <span class="required-label">*</span></label>
                                                            <input id="addEmployeeEmail" name="edEmail" type="text" class="form-control" placeholder="fill email" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="addEmployeePhone" name="edPhone" type="text" class="form-control" placeholder="fill phone" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Address <span class="required-label">*</span></label>
                                                            <input id="addEmployeeAddress" name="edAddress" type="text" class="form-control" placeholder="fill address" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <label>BIRTHDATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="addEmployeeBirthdate" name="edBirthdate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Username <span class="required-label">*</span></label>
                                                            <input id="addEmployeeUsername" name="edUsername" type="text" class="form-control" autocomplete="username" placeholder="fill username" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Password <span class="required-label">*</span></label>
                                                            <input id="addEmployeePassword" name="edPassword" type="password" class="form-control" autocomplete="new-password" placeholder="fill password" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Confirm Password <span class="required-label">*</span></label>
                                                            <input id="addEmployeeCPassword" name="edCPassword" type="password" class="form-control" autocomplete="new-password" placeholder="fill password" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CEmployees/addEmployee');?>" type="submit" name="action" id="AddEmployeeButton" class="btn btn-primary">Add</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Add Employee -->

                            <!-- Modal Edit Employee -->
                            <div class="modal fade" id="editEmployeeModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Edit</span>
                                                <span class="fw-light">
                                                    Employee
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="editEmployeeForm" autocomplete="off" method="post" action="<?php echo base_url('CEmployees/editEmployee');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <p class="small">Edit an existing employee using this form, make sure you fill them all</p>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <input type="hidden" id="editEmployeeID" name="edID" value="">
                                                            <label>Name <span class="required-label">*</span></label>
                                                            <input id="editEmployeeName" name="edName" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>ID Card Number (Nomor KTP) <span class="required-label">*</span></label>
                                                            <input id="editEmployeeIdCardNumber" name="edIdCardNumber" type="text" class="form-control" placeholder="fill ID card number" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Email <span class="required-label">*</span></label>
                                                            <input id="editEmployeeEmail" name="edEmail" type="text" class="form-control" placeholder="fill position" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Phone <span class="required-label">*</span></label>
                                                            <input id="editEmployeePhone" name="edPhone" type="text" class="form-control" placeholder="fill office" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Address <span class="required-label">*</span></label>
                                                            <input id="editEmployeeAddress" name="edAddress" type="text" class="form-control" placeholder="fill name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group form-show-validation">
                                                            <label>BIRTHDATE <span class="required-label">*</span></label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="editEmployeeBirthdate" name="edBirthdate" required>
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">
                                                                        <i class="la la-calendar-o"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group  form-group-default form-show-validation">
                                                            <label>Username <span class="required-label">*</span></label>
                                                            <input id="editEmployeeUsername" name="edUsername" type="text" class="form-control" autocomplete="username" placeholder="fill username" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <button id="resetPasswordButton" type="button" onclick="resetPassword();" class="btn btn-primary btn-round ml-auto">
                                                            <i class="la la-key"></i>
                                                            Reset Password
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer no-bd">
                                                <button href="<?php echo base_url('CEmployees/editEmployee');?>" type="submit" name="action" id="editEmployeeButton" class="btn btn-primary">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- End of Modal Edit Employee -->

                            <!-- Modal Delete Employee -->
                            <div class="modal fade" id="deleteEmployeeModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <span class="fw-mediumbold">
                                                Delete</span>
                                                <span class="fw-light">
                                                    Employee
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form id="deleteEmployeeForm" autocomplete="off" method="post" action="<?php echo base_url('CEmployees/deleteEmployee');?>" enctype="multipart/form-data">
                                            <div class="modal-body">
                                                <input type="hidden" id="deleteEmployeeID" name="edID" value="">
                                                <p class="small">Are you sure you want to delete employee <span id="delEmpName"></span></p>
                                                <div class="modal-footer no-bd">
                                                    <button href="<?php echo base_url('CEmployees/deleteEmployee');?>" type="submit" name="action" id="deleteEmployeeButton" class="btn btn-danger">Delete</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Delete Employee -->

                            <!-- Modal Notifikasi Employee-->
                            <div class="modal fade" id="employeeNotifModal" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header no-bd">
                                            <h5 class="modal-title">
                                                <?php if (isset($notif_message)) echo $notif_message ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Modal Notifikasi Employee -->
                            <div class="table-responsive">
                                <table id="employeesTable" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th style="width: 10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php
                                        //Tampilkan Semua Employees
                                        // $employees dapat dari CEmployees $data['employees']
                                        foreach ($employees as $item) {
                                        ?>
                                        <tr>
                                            <td><?php echo $item->emp_name ?></td>
                                            <td><?php echo $item->emp_email ?></td>
                                            <td><?php echo $item->emp_phone ?></td>
                                            <td>
                                                <div class="form-button-action">
                                                    <span data-toggle="modal" data-target="#editEmployeeModal">
                                                        <button type="button" onclick="editEmployee(<?php echo $item->emp_id ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                                            <i class="la la-edit"></i>
                                                        </button>
                                                    </span>
                                                    <span data-toggle="modal" data-target="#deleteEmployeeModal">
                                                        <button type="button" onclick="deleteEmployee(<?php echo $item->emp_id ?>)" data-toggle="tooltip" title="Disable" class="btn btn-link btn-danger" data-original-title="Disable">
                                                            <i class="la la-times"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php');?>
</div>

<script>
    $('#addEmployeeBirthdate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#editEmployeeBirthdate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    // Set the employee table as data table for search and pagination feature
    $('#employeesTable').DataTable();

    // Add Modal Form Validation
    $("#addEmployeeForm").validate({
        validClass: "success",
        rules: {
            edBirthdate: {date: true},
            edPassword: {
                minlength: 8,
                maxlength: 20,
                remote: "<?= base_url("CEmployees/checkPasswordStrength") ?>"
            },
            edCPassword: {
                equalTo: "#addEmployeePassword"
            },
            edUsername: {
                remote: function(){
                    var r = {
                        url: '<?= base_url("CEmployees/checkUsernameExist") ?>',
                        type: "post",
                        data: {
                            empUsername : $('#addEmployeeUsername').val(),
                        },
                    }
                    return r;
                }
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    // edit Modal Form Validation
    $("#editEmployeeForm").validate({
        validClass: "success",
        rules: {
            edBirthdate: {date: true},
            edPassword: {
                minlength: 8,
                maxlength: 20,
                remote: "<?= base_url("CEmployees/checkPasswordStrength") ?>"
            },
            edCPassword: {
                equalTo: "#editEmployeePassword"
            }
        },
        messages: {
            edPassword: {
                remote: jQuery.validator.format("{0} is already in use")
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

    <?php if(isset($notif_message)) { ?>
    // $('#employeeNotifModal').modal('show');
    swal('<?= $notif_message ?>', {
        buttons: {
            confirm: {
                className : 'btn btn-success'
            }
        }
    }).then(function(){
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CEmployees/index'); ?>");
    });
    <?php } ?>

    // Remove the notif parameter from the URL
    $("#employeeNotifModal").on("hidden.bs.modal", function () {
        window.history.replaceState("BB Tour & Travel Object", "BB Tour & Travel", "<?php echo base_url('CEmployees/index'); ?>");
    });

    // Filling selected employee data to edit employee modal
    function editEmployee(empID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CEmployees/getEmployeeByID") ?>',
            data: {
                empID : empID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#editEmployeeID').val(empID);
            $('#editEmployeeName').val(result.emp_name);
            $('#editEmployeeIdCardNumber').val(result.emp_id_card_number);
            $('#editEmployeeEmail').val(result.emp_email);
            $('#editEmployeePhone').val(result.emp_phone);
            $('#editEmployeeAddress').val(result.emp_address);
            $('#editEmployeeBirthdate').val(result.emp_birthdate);
            // if(result.emp_access_level == 0) {
            //     $('#editEmployeeAccessLevel').val('Operator');
            // } else {
            //     $('#editEmployeeAccessLevel').val('Administrator');
            // }
            $('#editEmployeeUsername').val(result.emp_username);
            $('#editEmployeePassword').val('');
            $('#editEmployeeCPassword').val('');
        });
    }

    // Filling selected employee name to delete employee modal
    function deleteEmployee(empID) {
        $.ajax({
            method: "post",
            url: '<?= base_url("CEmployees/getEmployeeNameByID") ?>',
            data: {
                empID : empID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            var result = JSON.parse(data);
            $('#deleteEmployeeID').val(empID);
            $('#delEmpName').html(result.emp_name);
        });
    }

    function resetPassword() {
        $('#resetPasswordButton').attr('disabled', 'disabled');
        let empID = $('#editEmployeeID').val();
        $.ajax({
            method: "post",
            url: '<?= base_url("CEmployees/resetPassword") ?>',
            data: {
                empID : empID
            },
        }).fail(function(jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }).done(function(data) {
            alert(data);
            $('#resetPasswordButton').removeAttr('disabled');
        });

    }
</script>
</body>
</html>
