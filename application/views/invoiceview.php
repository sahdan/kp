<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
	<div class="content">
		<div class="container-fluid">
			<h4 class="page-title">Invoice</h4>
			<div class="row">
				<div class="col-sm-12">
					<div>
						<span class="float-right">
							<button class="btn btn-primary btn-round" data-toggle="modal" data-target="#addPaymentModal" style="margin-bottom:15px;">
								<i class="la la-credit-card"></i>
								Record Payment
							</button>
						</span>
						<span class="float-right">
							<button onclick="window.open('<?php echo base_url('CInvoices/print/' . $invoiceHeader->invh_id)?>','_blank')" type="button" class="btn btn-primary btn-round" id="printInvoiceButton" style="margin-bottom:15px;margin-right:15px;">
								<i class="la la-print"></i>
								Print Invoice
							</button>
						</span>
					</div>
				</div>
				<!-- Modal Sent Reminder -->
				<div class="modal fade" id="setReminderModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header no-bd">
								<h5 class="modal-title">
									<span class="fw-mediumbold">
									Send</span>
									<span class="fw-light">
										Reminder
									</span>
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<p class="small">Add new reminder or remove reminder for this invoice using this form</p>

							</div>
							<div class="modal-footer no-bd">
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- End of Modal Sent Reminder -->
				<!-- Modal Add Payment -->
				<div class="modal fade" id="addPaymentModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header no-bd">
								<h5 class="modal-title">
									<span class="fw-mediumbold">
									Record</span>
									<span class="fw-light">
										a payment for this invoice
									</span>
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form id="addPaymentForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/addPayment');?>" enctype="multipart/form-data">
								<div class="modal-body">
									<p class="small">Record a payment you’ve already received, such as cash, cheque, or bank payment.</p>
									<input type="hidden" id="invoiceID" name="edInvoiceID" value="<?= $invoiceHeader->invh_id ?>"/>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group form-show-validation" style="padding-left:0px;padding-right:0px;">
												<label>Payment Date <span class="required-label">*</span></label>
												<div class="input-group">
													<input type="text" class="form-control" id="addPaymentDate" name="edDate" required>
													<div class="input-group-append">
														<span class="input-group-text">
															<i class="la la-calendar-o"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group form-group-default form-show-validation">
												<label>Amount <span class="required-label">*</span></label>
												<input id="addPaymentAmount" name="edAmount" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group  form-group-default form-show-validation">
												<label>Payment Type <span class="required-label">*</span></label>
												<select id="addPaymentType" name="edPaymentType" type="text" class="form-control" required>
													<option>Tunai</option>
													<option>BCA</option>
													<option>BNI</option>
												</select>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group  form-group-default form-show-validation">
												<label>Memo /notes</label>
												<textarea id="addPaymentNotes" name="edNotes" rows="5" class="form-control" placeholder="fill notes" ></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer no-bd">
									<button id="addPaymentButton" class="btn btn-primary">Submit</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- End of Modal Add Payment -->
				<!-- Modal Edit Payment -->
				<div class="modal fade" id="editPaymentModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header no-bd">
								<h5 class="modal-title">
									<span class="fw-mediumbold">
									Edit</span>
									<span class="fw-light">
										payment for this invoice
									</span>
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form id="editPaymentForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/editPayment');?>" enctype="multipart/form-data">
								<div class="modal-body">
									<input type="hidden" id="invoiceID" name="edInvoiceID" value="<?= $invoiceHeader->invh_id ?>"/>
									<input type="hidden" id="editPaymentID" name="edID" value="">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group form-show-validation" style="padding-left:0px;padding-right:0px;">
												<label>Payment Date <span class="required-label">*</span></label>
												<div class="input-group">
													<input type="text" class="form-control" id="editPaymentDate" name="edDate" required>
													<div class="input-group-append">
														<span class="input-group-text">
															<i class="la la-calendar-o"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group form-group-default form-show-validation">
												<label>Amount <span class="required-label">*</span></label>
												<input id="editPaymentAmount" name="edAmount" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group  form-group-default form-show-validation">
												<label>Payment Type <span class="required-label">*</span></label>
												<select id="editPaymentType" name="edPaymentType" type="text" class="form-control" required>
													<option>Tunai</option>
													<option>BCA</option>
													<option>BNI</option>
												</select>
											</div>
										</div>
										<div class="col-sm-12">
											<div class="form-group  form-group-default form-show-validation">
												<label>Memo /notes</label>
												<textarea id="editPaymentNotes" name="edNotes" rows="5" class="form-control" placeholder="fill notes" ></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer no-bd">
									<button id="editPaymentButton" class="btn btn-primary">Submit</button>
									<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- End of Modal Edit Payment -->
				<!-- Modal Delete Payment -->
				<div class="modal fade" id="deletePaymentModal" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header no-bd">
								<h5 class="modal-title">
									<span class="fw-mediumbold">
									Remove</span>
									<span class="fw-light">
										invoice payment
									</span>
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<form id="deleteCustomerForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/deletePayment');?>" enctype="multipart/form-data">
								<div class="modal-body">
									<input type="hidden" name="edInvoiceID" value="<?= $invoiceHeader->invh_id ?>">
									<input type="hidden" id="deletePaymentID" name="edID" value="">
									<p class="small"><strong id="deletePaymentText"></strong>Are you sure you want to remove this invoice payment ?</p>
									<div class="modal-footer no-bd">
										<button class="btn btn-danger">Delete</button>
										<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- End of Modal Delete Payment-->
			</div>
			<!-- Invoice status card -->
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<!-- Amount due & status -->
							<span>
								<span>
									<span>
										<strong>Amount due:&nbsp;</strong>
										<span class="currency"><?= $amountDue ?></span>
									</span>
								</span>
								<span>
									<span>
										<strong>Due on:&nbsp;</strong>
										<span><?= date("l, jS \of F Y ",strtotime($invoiceHeader->invh_date_due)) ?></span>
									</span>
								</span>
								&nbsp;
								<?php switch(strtolower($invoiceHeader->invh_status)){
									case "paid":
									echo '<span style="border-radius:4px" class="badge badge-success">Paid</span>';
									break;
									case "overdue":
									echo '<span style="border-radius:4px" class="badge badge-danger">Overdue</span>';
									break;
									case "sent":
									echo '<span style="border-radius:4px" class="badge badge-primary">Sent</span>';
									break;
									case "partial":
									echo '<span style="border-radius:4px" class="badge badge-primary">Partial</span>';
									break;
								} ?>
								<span style="float:right">
									<span>
										<strong>Status:&nbsp;</strong>
										<?php switch(strtolower($invoiceHeader->invh_status)){
											case "paid":
											echo 'Your invoice is paid in full';
											break;
											default:
											echo 'Your invoice is awaiting payment';
										} ?>
									</span>
								</span>
							</span>
						</div>
						<div class="card-header">
							<!-- Reminder section -->
							<div class="invoice-payment-reminders">
								<div>
									<strong>Get paid on time by scheduling payment reminders for your customer :</strong>
								</div>
							</div>
							<input disabled value="<?= $invoiceHeader->invh_id ?>" id="addInvoiceID" name="edInvoiceID" type="hidden" class="form-control" required>
							<div class="row">
								<div class="col-sm-12">
									<div style="padding:5px 10px 0px 5px;">
										<span class="no-reminder">
										<?php
										if(!$reminders){
										?>
											No reminders have been set for this invoice.
										<?php
										}
										?>
										</span>
										<div class="reminder-list">
										<?php
										foreach($reminders as $item){
											?>
											<div class="row">
												<div class="col-sm-12 reminder-item">
													<input class="reminder-id" name="edReminderId" type="hidden" value="<?= $item->rem_id ?>"/>
													<?= date("l, jS \of F Y",strtotime($item->rem_date_remind)) ?>
													<button style="padding: 5px 7px;" type="button" class="remove-reminder btn btn-link btn-danger" data-toggle="tooltip" title="Remove reminder" class="btn btn-link btn-danger" data-original-title="Remove">
														<i class="la la-times"></i>
													</button>
												</div>
											</div>
											<?php
										}
										?>
										</div>
									</div>
								</div>
							</div>
							<form id="setReminderForm" autocomplete="off">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group form-show-validation" style="padding:5px;">
											<div class="input-group">
												<div>
													<label style="margin:10px 10px 10px 0px;">New date to remind :</label>
												</div>
												<input type="text" class="form-control" id="addDateToRemind" name="edDateToRemind" style="max-width:150px;" required>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-o"></i>
													</span>
												</div>
												<button style="margin-left:10px;"type="button" onclick="addReminder()" class="btn btn-primary">Add Reminder</button>
											</div>
										</div>
									</div>
								</div>
							</form>
							<div>
								<button id="sendReminderButton" data-toggle="modal" href="#" onclick="sendReminder()" class="btn btn-primary">Send a reminder now</button>
							</div>
						</div>
						<?php if($paymentHistory){?>
						<div class="card-body invoice-payment-history">
							<!-- Payment history section -->
							<div>
								<div>
									<strong>Payments received:</strong>
								</div>
								<?php foreach($paymentHistory as $item){ ?>
								<div style="margin:10px 0px;">
									<span><?= date("l, jS \of F Y", strtotime($item->ar_date)) ?> - A payment for
										<strong class="currency"><?= $item->ar_amount ?></strong> was made
										<span>
										<?php
										switch(strtolower($item->ar_payment_type))
										{
											case 'bni':
											echo "using a bank payment(BNI)";
											break;
											case 'bca':
											echo "using a bank payment(BCA)";
											break;
											case 'tunai':
											echo "using cash";
											break;
										}
										?>
										</span>.
									</span><br>
									<div>
										<input class="paymentID" type="hidden" value="<?= $item->ar_id ?>"/>
										<button id="sendReceiptButton" onclick="sendEmailByARID(<?= $item->ar_id ?>)" class="btn btn-primary">Send a receipt</button>
										<span class="bullet-divider">·</span>
										<button class="editPaymentLink btn btn-primary" data-toggle="modal" href="#editPaymentModal">Edit payment</button>
										<span class="bullet-divider">·</span>
										<button class="deletePaymentLink btn btn-primary" data-toggle="modal" href="#deletePaymentModal">Remove payment</button>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<!--  -->
			<div id="section-to-print" class="row">
				<div class="col-md-12">
					<div class="card card-invoice">
						<form id="addInvoiceForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/editInvoice');?>" enctype="multipart/form-data">
							<div class="card-header">
								<div class="invoice-header">
									<input type="hidden" id="editInvoiceHeaderID" name="edInvID" value="<?= $invoiceHeader->invh_id ?>">
									<h3 class="invoice-title">
										Invoice BB-<?= $invoiceHeader->invh_id ?>
									</h3>
									<div class="invoice-logo">
										<img src="<?php echo base_url('/assets/img/logoinvoice.png'); ?>" alt="company logo">
									</div>
								</div>
								<div class="invoice-desc" style="text-align: left; display: inline-block">
								<strong> BB Tour and Travel Surabaya </strong><br>
								Apartemen Puri Mas G-07<br>
								Jl I Gusti Ngurah Rai No 44, Gn. Anyar<br>
								Surabaya, Jawa Timur 60294<br>
								Indonesia<br>
								</div>
								<div class="invoice-desc" style="text-align: right; float: right; clear: right; display: inline-block">
								<strong> Contact Information </strong><br>
								Phone: (031) 582 007 40<br>
								Fax: (031) 582 007 40<br>
								Mobile: 0822 9923 8008<br>
								</div>
							</div>
							<div class="card-body">
								<div class="seperator-solid"></div>
								<div class="row">
									<div class="col-md-4 info-invoice">
										<div class="form-group form-show-validation">
											<label>Invoice Date </label>
											<p><?php echo date_format(date_create($invoiceHeader->invh_date_created),'l jS F Y');?></p>
										</div>
									</div>
									<div class="col-md-4 info-invoice">
										<div class="form-group form-show-validation">
											<label>Payment Due Date </label>
											<p><?php echo date_format(date_create($invoiceHeader->invh_date_due),'l jS F Y');?></p>
										</div>
									</div>
									<div class="col-md-4 info-invoice">
										<div class="form-group form-show-validation">
											<label>Invoice To <span class="required-label">*</span></label>
                                            <p><?= $invoiceHeader->cust_name ?></p>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="invoice-detail">
											<div class="invoice-top">
												<h3 class="title"><strong>Order summary</strong></h3>
											</div>
											<div class="invoice-item">
												<div class="table-responsive">
													<table class="table table-striped" id="invoiceDetailsTable">
														<thead>
															<tr>
																<td class="text-center" style="display:none;"><strong>Product/Service ID</strong></td>
																<td><strong>Product/Services</strong></td>
																<td class="text-center" style="display:none;"><strong>Cost</strong></td>
																<td class="text-right"><strong>Price</strong></td>
																<td class="text-center" style="display:none;"><strong>Actual Price</strong></td>
																<td class="text-center" style="width: 5%;"><strong>Qty</strong></td>
																<td class="text-right"><strong>Totals</strong></td>
																<td class="text-center" style="display:none;"><strong>Commission</strong></td>
															</tr>
														</thead>
														<tbody>
														<?php $row = 1; ?>
														<?php $subTotal = 0; ?>
														<?php $discount = 0; ?>
														<?php foreach ($invoiceDetails as $item) { ?>
															<tr id="row_<?= $row ?>">
																<td style="display:none;"><?= $item->ps_id ?> <input type="hidden" name="hiddenProductID[]" id="productID<?= $row ?>" value="<?= $item->ps_id ?>" /></td>
																<td><strong><?= $item->ps_name ?></strong><br>
																	<?= nl2br($item->invd_description) ?><textarea name="hiddenDescription[]" id="description<?= $row ?>" style="display:none;"><?= $item->invd_description ?></textarea></td>
																<td style="display:none;"><?= $item->invd_cost ?> <input type="hidden" name="hiddenCost[]" id="cost<?= $row ?>" value="<?= $item->invd_cost ?>" /></td>
																<td class="text-right"><span class="currency-row-<?= $row ?>"><?= $item->invd_price_display ?></span> <input type="hidden" name="hiddenDisplayPrice[]" id="displayPrice<?= $row ?>" value="<?= $item->invd_price_display ?>" /></td>
																<td style="display:none;"><?= $item->invd_price_actual ?> <input type="hidden" name="hiddenActualPrice[]" id="actualPrice<?= $row ?>" value="<?= $item->invd_price_actual ?>" /></td>
																<td class="text-center"><?= $item->invd_quantity ?> <input type="hidden" name="hiddenQuantity[]" id="quantity<?= $row ?>" value="<?= $item->invd_quantity ?>" /></td>
																<td class="text-right"><span class="currency-row-<?= $row ?>"><?= $item->invd_total ?></span> <input type="hidden" name="hiddenTotal[]" id="total<?= $row ?>" value="<?= $item->invd_total ?>" /></td>
																<td style="display:none;"><?= $item->invd_commission ?> <input type="hidden" name="hiddenCommission[]" id="commission<?= $row ?>" value="<?= $item->invd_commission ?>" /></td>
															</tr>
														<?php $row++; ?>
														<?php $subTotal = $subTotal + $item->invd_total; ?>
														<?php $discount = $discount + $item->invd_discount; ?>
														<?php } ?>
														<?php $total = $subTotal - $discount; ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										<div class="seperator-solid  mb-3"></div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-md-6 transfer-to">
										<h5 class="sub">Bank Transfer</h5>
										<div class="bank-notes">
											<p>BCA 7880501164 - Aristyo Wardiono<br>
											BNI 0795629012 - Aristyo Wardiono<br>
											BRI 058701000651566 - Aristyo Wardiono</p>
										</div>
									</div>
									<div class="col-md-6 transfer-total">

										<div>
											<div class="sub" style="text-align: left; display: inline-block; margin-right: 30px;">Sub Total</div>
											<div id="subTotal" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;"><?= $subTotal ?></div>
										</div>
										<div>
										<div class="sub" style="text-align: left; display: inline-block; margin-right: 30px;">Discount</div>
											<div id="discount" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;"><?= $discount ?></div>
										</div>
										<div>
											<div class="price" style="text-align: left; display: inline-block; margin-right: 30px;">Total</div>
											<div id="total" class="price text-right" style="float: right; clear: right; display: inline-block;"><?= $total ?></div>
										</div>
									</div>
								</div>
								<?php if($paymentHistory){?>
								<div>
									<div>
										<?php foreach($paymentHistory as $item){ ?>
										<div class="row">
											<div class="col-sm-12">
												<div class="float-right">
													<span class="float-left" style="margin-right:30px;margin-bottom:10px;">
														<strong>Payment on
														<?php
														echo date("l, jS \of F Y ", strtotime($item->ar_date));
														switch(strtolower($item->ar_payment_type))
														{
															case 'bni':
															echo "using a bank payment(BNI)";
															break;
															case 'bca':
															echo "using a bank payment(BCA)";
															break;
															case 'tunai':
															echo "using cash";
															break;
														}
														?>
														:
														</strong>
													</span>
													<div class="currency float-right" style="text-align:right; min-width:100px;">
														<span><?= $item->ar_amount ?></span>
													</div>
												</div>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
								<?php } ?>
								<hr/>
								<div>
									<div class="row">
										<div class="col-sm-12">
											<div class="float-right">
												<span class="price float-left" style="font-size: 28px;color: #177dff;padding: 7px 0;font-weight: 600; margin-right:30px;">Amount Due</span>
												<div class="currency float-right" style="text-align:right; font-size: 28px;color: #177dff;padding: 7px 0;font-weight: 600;">
													<span><?= $amountDue ?></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
<?php include('footer.php');?>
<script>

	// For Datepicker to actually work
	$('#addDateToRemind').datetimepicker({
		format: 'YYYY-MM-DD',
		minDate: moment().add(1, 'd').startOf('d')
	});

    $('#addPaymentDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#addInvoiceDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#addPaymentDueDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

	//Reminder validation
	$("#setReminderForm").validate({
        validClass: "success"
	});

	//Add reminder
	function addReminder(){
		if($("#addDateToRemind").val()){
			var edDateToRemind = $("#addDateToRemind").val();
			var edInvoiceID = <?= $invoiceHeader->invh_id ?>;
			$.ajax({
				method:"post",
				url: '<?= base_url("CReminders/addReminder")?>',
				data:{
					edInvoiceID : edInvoiceID,
					edDateToRemind : edDateToRemind,
				},
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(errorThrown);
			}).done(function(data) {
				$(".no-reminder").html("");
				$(".reminder-list").html(data);
			})
		}else{
			console.log("empty");
		}
	};

	//Remove reminder
	$(".reminder-list").on("click", ".remove-reminder", function(){
		var edID = $(this).siblings(".reminder-id").val();
		var row = $(this).parent().parent();
		$(this).tooltip('hide');
		$(row).remove();
		if($(".reminder-item").length == 0){
			$(".no-reminder").text("No reminders have been set for this invoice.");
		}
		$.ajax({
			method:"post",
			url: '<?= base_url("CReminders/deleteReminder")?>',
			data:{
				edID : edID,
			},
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
		}).done(function(data) {
		})
	});

	// Filling selected payment info to delete payment modal
	$('.invoice-payment-history').on('click','.deletePaymentLink', function(){
		var edID = $(this).siblings(".paymentID").val();
		$.ajax({
			method: "post",
			url: '<?= base_url("CInvoices/deletePaymentInfo") ?>',
			data: {
				edID : edID
			},
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
		}).done(function(data) {
			$('#deletePaymentID').val(edID);
			$('#deletePaymentText').html(data);
		});
	});

	// Filling selected payment info to edit payment modal
	$('.invoice-payment-history').on('click','.editPaymentLink', function(){
		var edID = $(this).siblings(".paymentID").val();
		$.ajax({
			method: "post",
			url: '<?= base_url("CInvoices/editPaymentInfo") ?>',
			data: {
				edID : edID
			},
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
		}).done(function(data) {
			var result = JSON.parse(data);
			$('#editPaymentID').val(edID);
			$('#editPaymentDate').val(result.ar_date);
			anEditPaymentAmount.set(parseInt(result.ar_amount));
			amountDueEdit = amountDue+parseInt(result.ar_amount);
			$('#editPaymentType').val(result.ar_payment_type)
			$('#editPaymentNotes').html(result.ar_notes);
		});
	});

	// Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    //minimumValue: '0',
    modifyValueOnWheel: false
    };

	<?php $row--; ?>
	<?php for ($i = 1; $i <= $row; $i++) { ?>
		new AutoNumeric.multiple('.currency-row-<?= $i ?>', rupiahFormat);
	<?php } ?>
	new AutoNumeric.multiple('.currency', rupiahFormat);
	anAddPaymentAmount = new AutoNumeric('#addPaymentAmount', rupiahFormat);
	anEditPaymentAmount = new AutoNumeric('#editPaymentAmount', rupiahFormat);
	anSubTotal = new AutoNumeric('#subTotal', rupiahFormat);
	anDiscount = new AutoNumeric('#discount', rupiahFormat);
	anTotal = new AutoNumeric('#total', rupiahFormat);


	// variable for custom jquery validator method
	let amountDue = <?= $amountDue ?>;

	// variable for custom jquery validator method for edit modal
	let amountDueEdit = amountDue;


	// Custom Validator Method to make sure actual price is lower or equal to display price
	$.validator.addMethod("lowerThanAmountDue",
    function (value, element) {
          return parseInt(value.replace(/,.*|[^0-9]/g, ''), 10) <= amountDue;
  });

	// Custom Validator Method to make sure actual price is lower or equal to display price
	$.validator.addMethod("lowerThanAmountDueEdit",
    function (value, element) {
          return parseInt(value.replace(/,.*|[^0-9]/g, ''), 10) <= amountDueEdit;
  }, function(params, element) {
  	return "Amount must be lower or equal to Rp. " + amountDueEdit
	});

	// Add Payment Modal Form Validation
	$("#addPaymentForm").validate({
		validClass: "success",
		rules: {
			edAmount: {
				lowerThanAmountDue: true
			}
		},
		messages: {
			edAmount: {
				lowerThanAmountDue: "Amount must be lower or equal to Rp. " + amountDue
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
	});

	// Edit Payment Modal Form Validation
	$("#editPaymentForm").validate({
		validClass: "success",
		rules: {
			edAmount: {
				lowerThanAmountDueEdit: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
	});

	//Delete Payment Modal Form Validation
	$("#deletePaymentForm").validate({
		validClass: "success",
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		success: function(element) {
			$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		},
	});

	function sendEmailByARID (arID) {
		$('#sendReceiptButton').attr('disabled', 'disabled');
		$.ajax({
			method: "post",
			url: "<?= base_url('CInvoices/sendReceipt') ?>",
			data: {
				arID : arID,
				invhID : "<?= $invoiceHeader->invh_id ?>"
			},
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
		}).done(function(data) {
			swal(data, {
				icon : "info",
				buttons: {
					confirm: {
						className : 'btn btn-info'
					}
				}
			});
			$('#sendReceiptButton').removeAttr('disabled');
		});
	}

	function sendReminder () {
		$('#sendReminderButton').attr('disabled', 'disabled');
		$.ajax({
			method: "post",
			url: "<?= base_url('CReminders/sendReminderByInvID/' . $invoiceHeader->invh_id ) ?>",
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(errorThrown);
			alert(textStatus);
			alert(jqXHR);
		}).done(function(data) {
			swal(data, {
				icon : "info",
				buttons: {
					confirm: {
						className : 'btn btn-info'
					}
				}
			});
			$('#sendReminderButton').removeAttr('disabled');
		});
	}

</script>
</body>
</html>
