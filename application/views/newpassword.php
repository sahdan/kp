<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require('header.php');
?>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
            <h3 class="text-center">Assign new password</h3>
            <div class="alert alert-danger" role="alert" style="display: none">
                Username/Password Combination not found
            </div>
            <form id="newPasswordForm" method="post" action="<?php echo base_url('CEmployees/updatePassword');?>">
                <div class="login-form">
                    <div class="col-sm-12">
                    <input type="hidden" id="employeeID" name="edID" value="<?= $employee->emp_id ?>">
                        <div class="form-group form-floating-label form-show-validation">
                            <label>Password <span class="required-label">*</span></label>
                            <input id="password" name="edPassword" type="password" class="form-control input-border-bottom" autocomplete="new-password" placeholder="fill password" required>
                            <div class="show-password">
                                <i class="flaticon-interface"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group form-floating-label form-show-validation">
                            <label>Confirm Password <span class="required-label">*</span></label>
                            <input id="cPassword" name="edCPassword" type="password" class="form-control input-border-bottom" autocomplete="new-password" placeholder="fill password" required>
                            <div class="show-password">
                                <i class="flaticon-interface"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-action">
                        <button class="btn btn-primary btn-rounded btn-login" type="submit" name="action">Submit</button>
                    </div>
                </div>
            </form>
		</div>
	</div>
    <?php include('footer.php');?>
    <script async>
		$(document).ready(function() {
			// Jika gagal login atau berhasil registrasi akan muncul popup
			var url = window.location.href;
			if(url.indexOf("loginFail") != -1){
				$('div.alert').toggle();
			}

            $("#newPasswordForm").validate({
                validClass: "success",
                rules: {
                    edPassword: {
                        minlength: 8,
                        maxlength: 20,
                        remote: "<?= base_url("CEmployees/checkPasswordStrength") ?>"
                    },
                    edCPassword: {
                        equalTo: "#password"
                    }
                },
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function(element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
            });
		});
	</script>
</body>
</html>
