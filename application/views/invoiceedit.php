<?php
defined('BASEPATH') or exit('No direct script access allowed');
include('navbar.php');
?>
<div class="main-panel">
	<div class="content">
		<div class="container-fluid">
			<h4 class="page-title">Invoice</h4>
			<div class="row">
				<div class="col-md-12">
					<div class="card card-invoice">
						<form id="addInvoiceForm" autocomplete="off" method="post" action="<?php echo base_url('CInvoices/editInvoice');?>" enctype="multipart/form-data">
							<div class="card-header">
								<div class="invoice-header">
									<input type="hidden" id="editInvoiceHeaderID" name="edInvID" value="<?= $invoiceHeader->invh_id ?>">
									<h3 class="invoice-title">
										Invoice BB-<?= $invoiceHeader->invh_id ?>
									</h3>
									<div class="invoice-logo">
										<img src="<?php echo base_url('/assets/img/logo.png'); ?>" alt="company logo">
									</div>
								</div>
								<div class="invoice-desc" style="text-align: left; display: inline-block">
								<strong> BB Tour and Travel Surabaya </strong><br>
								Apartemen Puri Mas G-07<br>
								Jl I Gusti Ngurah Rai No 44, Gn. Anyar<br>
								Surabaya, Jawa Timur 60294<br>
								Indonesia<br>
								</div>
								<div class="invoice-desc" style="text-align: right; float: right; clear: right; display: inline-block">
								<strong> Contact Information </strong><br>
								Phone: (031) 582 007 40<br>
								Fax: (031) 582 007 40<br>
								Mobile: 0822 9923 8008<br>
								</div>
							</div>
							<div class="card-body">
								<div class="seperator-solid"></div>
								<div class="row">
									<div class="col-md-4 info-invoice">
										<div class="form-group form-show-validation">
											<label>Invoice Date <span class="required-label">*</span></label>
											<div class="input-group">
												<input type="hidden" id="addEmployeeID" name="edEmpID" value="<?php echo $_SESSION["emp_id"];?>">
												<input type="text" class="form-control" id="addInvoiceDate" name="edDate" value="<?= $invoiceHeader->invh_date_created ?>" required>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-o"></i>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 info-invoice">
										<div class="form-group form-show-validation">
											<label>Payment Due Date <span class="required-label">*</span></label>
											<div class="input-group">
												<input type="text" class="form-control" id="addPaymentDueDate" name="edDueDate" value="<?= $invoiceHeader->invh_date_due ?>" required>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-o"></i>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 info-invoice">
										<div class="form-group form-show-validation">
											<label>Invoice To <span class="required-label">*</span></label>
											<!--<select id="addCustomer" name="edCustomer" type="text" class="form-control" required>
												<?php
												// Tampilkan Semua Vendors
												// $customers dapat dari CInvoices $data['customers']
												foreach ($customers as $item) {
												?>
												<option value="<?php echo $item->cust_id ?>"><?php echo $item->cust_name ?></option>
												<?php } ?>
											</select>-->
											<input type="hidden" id="addCustomerID" name="edCustomerID" value="<?= $invoiceHeader->cust_id ?>">
                                            <input id="addCustomerName" name="edName" type="text" class="form-control" placeholder="fill name" value="<?= $invoiceHeader->cust_name ?>" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="invoice-detail">
											<div class="invoice-top">
												<h3 class="title"><strong>Order summary</strong></h3>
											</div>
											<div class="invoice-item">
												<div class="table-responsive">
													<table class="table table-striped" id="invoiceDetailsTable">
														<thead>
															<tr>
																<td class="text-center" style="display:none;"><strong>Product/Service ID</strong></td>
																<td><strong>Product/Services</strong></td>
																<td class="text-center" style="display:none;"><strong>Cost</strong></td>
																<td class="text-right"><strong>Price</strong></td>
																<td class="text-center" style="display:none;"><strong>Actual Price</strong></td>
																<td class="text-center" style="width: 5%;"><strong>Qty</strong></td>
																<td class="text-right"><strong>Totals</strong></td>
																<td class="text-center" style="width: 10%;"><strong>Actions</strong></td>
															</tr>
														</thead>
														<tbody>
														<?php $row = 1; ?>
														<?php $subTotal = 0; ?>
														<?php $discount = 0; ?>
														<?php foreach ($invoiceDetails as $item) { ?>
															<tr id="row_<?= $row ?>">
																<td style="display:none;"><?= $item->ps_id ?> <input type="hidden" name="hiddenProductID[]" id="productID<?= $row ?>" value="<?= $item->ps_id ?>" /></td>
																<td><strong><?= $item->ps_name ?></strong><br>
																	<?= nl2br($item->invd_description) ?><textarea name="hiddenDescription[]" id="description<?= $row ?>" style="display:none;"><?= $item->invd_description ?></textarea></td>
																<td style="display:none;"><?= $item->invd_cost ?> <input type="hidden" name="hiddenCost[]" id="cost<?= $row ?>" value="<?= $item->invd_cost ?>" /></td>
																<td class="text-right"><span class="currency-row-<?= $row ?>"><?= $item->invd_price_display ?></span> <input type="hidden" name="hiddenDisplayPrice[]" id="displayPrice<?= $row ?>" value="<?= $item->invd_price_display ?>" /></td>
																<td style="display:none;"><?= $item->invd_price_actual ?> <input type="hidden" name="hiddenActualPrice[]" id="actualPrice<?= $row ?>" value="<?= $item->invd_price_actual ?>" /></td>
																<td class="text-center"><?= $item->invd_quantity ?> <input type="hidden" name="hiddenQuantity[]" id="quantity<?= $row ?>" value="<?= $item->invd_quantity ?>" /></td>
																<td class="text-right"><span class="currency-row-<?= $row ?>"><?= $item->invd_total ?></span> <input type="hidden" name="hiddenTotal[]" id="total<?= $row ?>" value="<?= $item->invd_total ?>" /></td>
																<td><div class="form-button-action">
																			<span data-toggle="modal" data-target="#editInvoiceDetailModal">
																			<button type="button" onclick="editInvoiceDetail(<?= $row ?>)" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
																			<i class="la la-edit"></i>
																			</button>
																			</span>
																			<span data-toggle="modal" data-target="#deleteInvoiceDetailModal">
																			<button type="button" onclick="deleteInvoiceDetail(<?= $row ?>)" data-toggle="tooltip" title="Remove" class="btn btn-link btn-danger" data-original-title="Remove">
																			<i class="la la-times"></i>
																			</button>
																			</span>
																			</div></td>
															</tr>
														<?php $row++; ?>
														<?php $subTotal = $subTotal + $item->invd_total; ?>
														<?php $discount = $discount + $item->invd_discount; ?>
														<?php } ?>
														<?php $total = $subTotal - $discount; ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										<button type="button" class="btn btn-primary btn-round ml-auto btn-block" data-toggle="modal" data-target="#addInvoiceDetailModal">
											<i class="la la-plus"></i>
											Add Item
                                		</button>
										<div class="seperator-solid  mb-3"></div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-md-6 transfer-to">
										<h5 class="sub">Bank Transfer</h5>
										<div class="bank-notes">
											<p>BCA 7880501164 - Aristyo Wardiono<br>
											BNI 0795629012 - Aristyo Wardiono<br>
											BRI 058701000651566 - Aristyo Wardiono</p>
										</div>
									</div>
									<div class="col-md-6 transfer-total">

										<div>
											<div class="sub" style="text-align: left; display: inline-block; margin-right: 30px;">Sub Total</div>
											<div id="subTotal" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;"><?= $subTotal ?></div>
										</div>
										<div>
										<div class="sub" style="text-align: left; display: inline-block; margin-right: 30px;">Discount</div>
											<div id="discount" class="text-right" style="text-align: right; float: right; clear: right; display: inline-block;"><?= $discount ?></div>
										</div>
										<div>
											<div class="price" style="text-align: left; display: inline-block; margin-right: 30px;">Total</div>
											<div id="total" class="price text-right" style="float: right; clear: right; display: inline-block;"><?= $total ?></div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<button href="<?php echo base_url('CInvoices/editInvoice');?>" type="submit" name="save" class="btn btn-primary btn-round" id="saveInvoiceButton" style="float: right; margin-right: 15px; margin-bottom: 15px; margin-left: auto;">
										<i class="la la-save"></i>
										Save Draft
									</button>
									<button href="<?php echo base_url('CInvoices/editInvoice');?>" type="submit" name="action" class="btn btn-primary btn-round" id="editInvoiceButton" style="float: right; margin-right: 15px; margin-bottom: 15px;">
										<i class="la la-plus"></i>
										Submit Invoice
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- Modal Add InvoiceDetail -->
			<div class="modal fade" id="addInvoiceDetailModal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header no-bd">
							<h5 class="modal-title">
								<span class="fw-mediumbold">
								New</span>
								<span class="fw-light">
									Invoice Detail
								</span>
							</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form id="addInvoiceDetailForm" autocomplete="off" method="post" action="#" enctype="multipart/form-data">
							<div class="modal-body">
								<p class="small">Create a new Invoice Detail using this form, make sure you fill them all</p>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group  form-group-default form-show-validation">
											<label>Product/Services <span class="required-label">*</span></label>
											<select id="addInvoiceDetailProduct" name="edProduct" type="text" class="form-control" required>
												<?php
												// Tampilkan Semua Products
												// $products dapat dari CInvoices $data['products']
												foreach ($products as $item) {
												?>
												<option value="<?php echo $item->ps_id ?>"><?php echo $item->ps_name ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group  form-group-default form-show-validation">
											<label>Description<span class="required-label">*</span></label>
											<textarea id="addInvoiceDetailDescription" name="edDescription" class="form-control" rows="5" placeholder="fill description" required></textarea>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-group-default form-show-validation">
											<label>Cost <span class="required-label">*</span></label>
                                            <input id="addInvoiceDetailCost" name="edCost" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group form-group-default form-show-validation">
											<label>Display Price <span class="required-label">*</span></label>
                                            <input id="addInvoiceDetailDisplayPrice" name="edDisplayPrice" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
									<div class="col-sm-12">
									<div class="form-group form-group-default form-show-validation">
											<label>Actual Price <span class="required-label">*</span></label>
                                            <input id="addInvoiceDetailActualPrice" name="edActualprice" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group  form-group-default form-show-validation">
											<label>Quantity <span class="required-label">*</span></label>
                                            <input id="addInvoiceDetailQuantity" name="edQuantity" type="number" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer no-bd">
								<button href="#" type="submit" name="action" id="addInvoiceDetailButton" class="btn btn-primary">Add</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- End of Modal Add InvoiceDetail -->

			<!-- Modal Edit InvoiceDetail -->
			<div class="modal fade" id="editInvoiceDetailModal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header no-bd">
							<h5 class="modal-title">
								<span class="fw-mediumbold">
								Edit</span>
								<span class="fw-light">
									Invoice Detail
								</span>
							</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form id="editInvoiceDetailForm" autocomplete="off" method="post" action="#" enctype="multipart/form-data">
							<div class="modal-body">
								<p class="small">Edit Invoice Detail using this form, make sure you fill them all</p>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group  form-group-default form-show-validation">
											<input type="hidden" id="editProductID" name="edID" value="">
											<input type="hidden" id="rowNumber" name="edNumber" value="">
											<label>Product/Services <span class="required-label">*</span></label>
											<select id="editInvoiceDetailProduct" name="edProduct" type="text" class="form-control" disabled="true" required>
												<?php
												// Tampilkan Semua Products
												// $products dapat dari CInvoices $data['products']
												foreach ($products as $item) {
												?>
												<option value="<?php echo $item->ps_id ?>"><?php echo $item->ps_name ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group  form-group-default form-show-validation">
											<label>Description<span class="required-label">*</span></label>
											<textarea id="editInvoiceDetailDescription" name="edDescription" class="form-control" rows="5" placeholder="fill description" required></textarea>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group form-group-default form-show-validation">
											<label>Cost <span class="required-label">*</span></label>
                                            <input id="editInvoiceDetailCost" name="edCost" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group form-group-default form-show-validation">
											<label>Display Price <span class="required-label">*</span></label>
                                            <input id="editInvoiceDetailDisplayPrice" name="edDisplayPrice" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
									<div class="col-sm-12">
									<div class="form-group form-group-default form-show-validation">
											<label>Actual Price <span class="required-label">*</span></label>
                                            <input id="editInvoiceDetailActualPrice" name="edActualprice" type="tel" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group  form-group-default form-show-validation">
											<label>Quantity <span class="required-label">*</span></label>
                                            <input id="editInvoiceDetailQuantity" name="edQuantity" type="number" class="form-control" placeholder="fill amount" spellcheck="false" required>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer no-bd">
								<button href="#" type="submit" name="action" id="editInvoiceDetailButton" class="btn btn-primary">Save</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							</div>
						</form>
					</div>
				</div>
			</div>

			<!-- End of Modal Edit InvoiceDetail -->

			<!-- Modal Delete InvoiceDetail -->
			<div class="modal fade" id="deleteInvoiceDetailModal" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header no-bd">
							<h5 class="modal-title">
								<span class="fw-mediumbold">
								Delete</span>
								<span class="fw-light">
									Invoice Detail
								</span>
							</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form id="deleteInvoiceDetailForm" autocomplete="off" method="post" action="#" enctype="multipart/form-data">
							<div class="modal-body">
								<input type="hidden" id="deleteInvoiceDetailID" name="edID" value="">
								<p class="small">Are you sure you want to delete item in row <span id="delRowNumber"></span></p>
								<div class="modal-footer no-bd">
									<button href="#" type="submit" name="action" id="deleteInvoiceDetailButton" class="btn btn-danger">Delete</button>
									<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- End of Modal Delete InvoiceDetail -->
		</div>
	</div>

</div>
<?php include('footer.php');?>
<script>

	// For Datepicker to actually work
    $('#addInvoiceDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});

    $('#addPaymentDueDate').datetimepicker({
		format: 'YYYY-MM-DD',
	});


	// Set the currency formatting
    const rupiahFormat = {
    allowDecimalPadding: false,
    currencySymbol: "Rp. ",
    decimalCharacter: ",",
    decimalPlaces: 0,
    digitGroupSeparator: ".",
    //minimumValue: '0',
    modifyValueOnWheel: false
    };

	<?php $row--; ?>
	<?php for ($i = 1; $i <= $row; $i++) { ?>
		new AutoNumeric.multiple('.currency-row-<?= $i ?>', rupiahFormat);
	<?php } ?>
	new AutoNumeric.multiple('.currency', rupiahFormat);
	anAddCost = new AutoNumeric('#addInvoiceDetailCost', rupiahFormat);
	anAddActualPrice = new AutoNumeric('#addInvoiceDetailActualPrice', rupiahFormat);
	anAddDisplayPrice = new AutoNumeric('#addInvoiceDetailDisplayPrice', rupiahFormat);
	anEditCost = new AutoNumeric('#editInvoiceDetailCost', rupiahFormat);
	anEditActualPrice = new AutoNumeric('#editInvoiceDetailActualPrice', rupiahFormat);
	anEditDisplayPrice = new AutoNumeric('#editInvoiceDetailDisplayPrice', rupiahFormat);
	anSubTotal = new AutoNumeric('#subTotal', rupiahFormat);
	anDiscount = new AutoNumeric('#discount', rupiahFormat);
	anTotal = new AutoNumeric('#total', rupiahFormat);

	// Custom Validator Method to make sure actual price is lower or equal to display price
	$.validator.addMethod("lowerThan",
    function (value, element, param) {
          var $otherElement = $(param);
          return parseInt(value.replace(/,.*|[^0-9]/g, ''), 10) <= parseInt($otherElement.val().replace(/,.*|[^0-9]/g, ''), 10);
  });

	// Add Modal Form Validation
    $("#addInvoiceDetailForm").validate({
        validClass: "success",
				rules: {
					edActualPrice: {
						lowerThan: "#addInvoiceDetailDisplayPrice"
					}
				},
				messages: {
					edActualPrice: {
						lowerThan: "Actual Price must be lower or equal to Display Price"
					}
				},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

	// Edit Modal Form Validation
    $("#editInvoiceDetailForm").validate({
        validClass: "success",
				rules: {
					edActualPrice: {
						lowerThan: "#addInvoiceDetailDisplayPrice"
					}
				},
				messages: {
					edActualPrice: {
						lowerThan: "Actual Price must be lower or equal to Display Price"
					}
				},
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
	});

	// Attach a submit handler to the add form
	$( "#addInvoiceDetailForm" ).submit(function( event ) {

		let isvalid = $("#addInvoiceDetailForm").valid();
		if (isvalid) {
			// Stop form from submitting normally
			event.preventDefault();

			// Counter for Invoice Detail Rows
			let rowNumber = $('#invoiceDetailsTable tbody tr').length;

			// Searching for duplicate Product/Service
			let duplicate = "false";
			for (let i = 0 ; i < rowNumber; i++) {
				let productID = $('#addInvoiceDetailProduct').val();
				if (productID == $('#productID'+rowNumber+'').val()) {
					duplicate = "true";
				}
			}

			// Check for duplicate Product/Service
			if (duplicate == "true") {
				swal("Product/Service already added", "Please add different Product/Service", {
					icon : "error",
					buttons: {
						confirm: {
							className : 'btn btn-danger'
						}
					},
				});
			} else {
				// Grabs all data from the from
				let productID = $('#addInvoiceDetailProduct').val();
				let productName = $('#addInvoiceDetailProduct option:selected').text();
				let description = $('#addInvoiceDetailDescription').val();
				let cost = parseInt($('#addInvoiceDetailCost').val().replace(/,.*|[^0-9]/g, ''));
				let displayPrice = parseInt($('#addInvoiceDetailDisplayPrice').val().replace(/,.*|[^0-9]/g, ''));
				let actualPrice = parseInt($('#addInvoiceDetailActualPrice').val().replace(/,.*|[^0-9]/g, ''));
				let quantity = $('#addInvoiceDetailQuantity').val();
				let rowTotal = displayPrice * quantity;
				let rowDiscount = rowTotal - (actualPrice*quantity);

				//Build HTML Table Row
				rowNumber = rowNumber + 1;
				output = '<tr id="row_'+rowNumber+'">';
				output += '<td style="display:none;">'+productID+' <input type="hidden" name="hiddenProductID[]" id="productID'+rowNumber+'" value="'+productID+'" /></td>';
				output += '<td><strong>'+productName+'</strong><br>'
							+description.replace(/(?:\r\n|\r|\n)/g, '<br>')+'<textarea name="hiddenDescription[]" id="description'+rowNumber+'" style="display:none;">'+description+'</textarea></td>';
				output += '<td style="display:none;">'+cost+' <input type="hidden" name="hiddenCost[]" id="cost'+rowNumber+'" value="'+cost+'" /></td>';
				output += '<td class="text-right"><span class="currency-row-'+rowNumber+'">'+displayPrice+'</span> <input type="hidden" name="hiddenDisplayPrice[]" id="displayPrice'+rowNumber+'" value="'+displayPrice+'" /></td>';
				output += '<td style="display:none;">'+actualPrice+' <input type="hidden" name="hiddenActualPrice[]" id="actualPrice'+rowNumber+'" value="'+actualPrice+'" /></td>';
				output += '<td class="text-center">'+quantity+' <input type="hidden" name="hiddenQuantity[]" id="quantity'+rowNumber+'" value="'+quantity+'" /></td>';
				output += '<td class="text-right"><span class="currency-row-'+rowNumber+'">'+rowTotal+'</span> <input type="hidden" name="hiddenTotal[]" id="total'+rowNumber+'" value="'+rowTotal+'" /></td>';
				output += '<td><div class="form-button-action">'
							+'<span data-toggle="modal" data-target="#editInvoiceDetailModal">'
							+'<button type="button" onclick="editInvoiceDetail('+rowNumber+')" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">'
							+'<i class="la la-edit"></i>'
							+'</button>'
							+'</span>'
							+'<span data-toggle="modal" data-target="#deleteInvoiceDetailModal">'
							+'<button type="button" onclick="deleteInvoiceDetail('+rowNumber+')" data-toggle="tooltip" title="Remove" class="btn btn-link btn-danger" data-original-title="Remove">'
							+'<i class="la la-times"></i>'
							+'</button>'
							+'</span>'
							+'</div></td>';
				output += '</tr>';

				//Append the HTML Table Row to the table
				$('#invoiceDetailsTable').append(output);

				//Update Sub Total, Discount, and Total via AutoNumeric Instance Method;
				let subTotal = anSubTotal.getNumber();
				subTotal = subTotal + rowTotal;
				anSubTotal.set(subTotal);
				let discount = anDiscount.getNumber();
				discount = discount + rowDiscount
				anDiscount.set(discount);
				let total = anTotal.getNumber();
				total = subTotal - discount;
				anTotal.set(total);

				// Set Autonumeric rupiah currency to the new row appended
				new AutoNumeric.multiple('.currency-row-'+rowNumber, rupiahFormat);

				// Hide modal
				$('#addInvoiceDetailModal').modal('hide');
				let form = $("#addInvoiceDetailForm");
				let validator = form.validate();
				validator.resetForm();
				anAddCost.clear();
				anAddActualPrice.clear();
				anAddDisplayPrice.clear();
				form.find(".has-success").removeClass("has-success");
				form.find(".has-error").removeClass("has-error");
				$("#addInvoiceDetailModal input").val("");
				$("#addInvoiceDetailDescription").val("");
			}
		}
	});

	// Attach a submit handler to the edit form
	$( "#editInvoiceDetailForm" ).submit(function( event ) {

		let isvalid = $("#editInvoiceDetailForm").valid();
		if (isvalid) {
			// Stop form from submitting normally
			event.preventDefault();

			let rowNumber = $('#rowNumber').val();

			//For Debugging purpose
			console.log('oldTotal: '+ parseInt($('#total'+rowNumber+'').val(), 10));
			console.log('newSubTotal: '+ anSubTotal.getNumber());
			//console.log('oldActualPrice: '+ parseInt($('#actualPrice'+rowNumber+'').val(), 10));
			//console.log('oldQuantity: '+ parseInt($('#quantity'+rowNumber+'').val(), 10));

			//Substract Sub Total, Discount, and Total with the old edited row value via AutoNumeric Instance Method;
			let rowOldTotal = parseInt($('#total'+rowNumber+'').val(), 10);
			let subTotal = anSubTotal.getNumber();
			subTotal = subTotal - rowOldTotal;
			anSubTotal.set(subTotal);

			let oldActualPrice = parseInt($('#actualPrice'+rowNumber+'').val(), 10); //6000
			let oldQuantity = parseInt($('#quantity'+rowNumber+'').val(), 10); //2
			let rowOldDiscount = rowOldTotal - (oldActualPrice * oldQuantity) //14000 - 12000
			let discount = anDiscount.getNumber(); //2001
			discount = discount - rowOldDiscount
			anDiscount.set(discount); //6001

			let total = anTotal.getNumber();
			total = subTotal - discount;
			anTotal.set(total);

			// Grabs all data from the from

			let productID = $('#editInvoiceDetailProduct').val();
			let productName = $('#editInvoiceDetailProduct option:selected').text();
			let description = $('#editInvoiceDetailDescription').val();
			let cost = parseInt($('#editInvoiceDetailCost').val().replace(/,.*|[^0-9]/g, ''));
			let displayPrice = parseInt($('#editInvoiceDetailDisplayPrice').val().replace(/,.*|[^0-9]/g, ''));
			let actualPrice = parseInt($('#editInvoiceDetailActualPrice').val().replace(/,.*|[^0-9]/g, ''));
			let quantity = $('#editInvoiceDetailQuantity').val();
			let rowTotal = displayPrice * quantity;
			let rowDiscount = rowTotal - (actualPrice*quantity);

			//Build new HTML Table Row
			output = '<td style="display:none;">'+productID+' <input type="hidden" name="hiddenProductID[]" id="productID'+rowNumber+'" value="'+productID+'" /></td>';
			output += '<td><strong>'+productName+'</strong><br>'
						+description.replace(/(?:\r\n|\r|\n)/g, '<br>')+'<textarea name="hiddenDescription[]" id="description'+rowNumber+'" style="display:none;">'+description+'</textarea></td>';
			output += '<td style="display:none;">'+cost+' <input type="hidden" name="hiddenCost[]" id="cost'+rowNumber+'" value="'+cost+'" /></td>';
			output += '<td class="text-right"><span class="currency-row-'+rowNumber+'">'+displayPrice+'</span> <input type="hidden" name="hiddenDisplayPrice[]" id="displayPrice'+rowNumber+'" value="'+displayPrice+'" /></td>';
			output += '<td style="display:none;">'+actualPrice+' <input type="hidden" name="hiddenActualPrice[]" id="actualPrice'+rowNumber+'" value="'+actualPrice+'" /></td>';
			output += '<td class="text-center">'+quantity+' <input type="hidden" name="hiddenQuantity[]" id="quantity'+rowNumber+'" value="'+quantity+'" /></td>';
			output += '<td class="text-right"><span class="currency-row-'+rowNumber+'">'+rowTotal+'</span> <input type="hidden" name="hiddenTotal[]" id="total'+rowNumber+'" value="'+rowTotal+'" /></td>';
			output += '<td><div class="form-button-action">'
						+'<span data-toggle="modal" data-target="#editInvoiceDetailModal">'
						+'<button type="button" onclick="editInvoiceDetail('+rowNumber+')" data-toggle="tooltip" title="Edit" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">'
						+'<i class="la la-edit"></i>'
						+'</button>'
						+'</span>'
						+'<span data-toggle="modal" data-target="#deleteInvoiceDetailModal">'
						+'<button type="button" onclick="deleteInvoiceDetail('+rowNumber+')" data-toggle="tooltip" title="Remove" class="btn btn-link btn-danger" data-original-title="Remove">'
						+'<i class="la la-times"></i>'
						+'</button>'
						+'</span>'
						+'</div></td>';
			// Rewrite Edited HTML row
			$('#row_'+rowNumber+'').html(output);

			//Update Sub Total, Discount, and Total via AutoNumeric Instance Method;
			subTotal = subTotal + rowTotal;
			anSubTotal.set(subTotal);
			discount = discount + rowDiscount
			anDiscount.set(discount);
			total = subTotal - discount;
			anTotal.set(total);

			// Set Autonumeric rupiah currency to the edited row appended
			new AutoNumeric.multiple('.currency-row-'+rowNumber, rupiahFormat);

			// Hide Edit modal
			$('#editInvoiceDetailModal').modal('hide');
		}

	});

	// Attach a submit handler to the delete form
	$( "#deleteInvoiceDetailForm" ).submit(function( event ) {
		// Stop form from submitting normally
		event.preventDefault();

		let rowNumber = $('#delRowNumber').text();

		//For Debugging purpose
		console.log('oldRowTotal: '+ parseInt($('#total'+rowNumber+'').val(), 10));
		console.log('oldSubTotal: '+ anSubTotal.getNumber());
		//console.log('oldActualPrice: '+ parseInt($('#actualPrice'+rowNumber+'').val(), 10));
		//console.log('oldQuantity: '+ parseInt($('#quantity'+rowNumber+'').val(), 10));

		//Substract Sub Total, Discount, and Total with the old edited row value via AutoNumeric Instance Method;
		let rowOldTotal = parseInt($('#total'+rowNumber+'').val(), 10);
		let subTotal = anSubTotal.getNumber();
		subTotal = subTotal - rowOldTotal;
		anSubTotal.set(subTotal);

		let oldActualPrice = parseInt($('#actualPrice'+rowNumber+'').val(), 10); //6000
		let oldQuantity = parseInt($('#quantity'+rowNumber+'').val(), 10); //2
		let rowOldDiscount = rowOldTotal - (oldActualPrice * oldQuantity) //14000 - 12000
		let discount = anDiscount.getNumber(); //2001
		discount = discount - rowOldDiscount
		anDiscount.set(discount); //6001

		let total = anTotal.getNumber();
		total = subTotal - discount;
		anTotal.set(total);

		// Remove Deleted HTML row
		$('#row_'+rowNumber+'').remove();

		// Hide Delete modal
		$('#deleteInvoiceDetailModal').modal('hide');
	});

	function editInvoiceDetail(rowNumber) {

		//Filling field from the data set in hidden input fields
		$('#editInvoiceDetailProduct').val($('#productID'+rowNumber+'').val());
		$('#editInvoiceDetailDescription').val($('#description'+rowNumber+'').val());
		$('#editInvoiceDetailQuantity').val($('#quantity'+rowNumber+'').val());
		$('#rowNumber').val(rowNumber);

		//filling field input cost, actual price, and display price via AutonumericInstance method
		anEditCost.set($('#cost'+rowNumber+'').val());
		anEditActualPrice.set($('#actualPrice'+rowNumber+'').val());
		anEditDisplayPrice.set($('#displayPrice'+rowNumber+'').val());
	}

	function deleteInvoiceDetail(rowNumber) {
		$('#delRowNumber').html(rowNumber);
	}

	//Disable Submit Invoice Button after first click
	$('#addInvoiceForm').submit(function() {
		$('#saveInvoiceButton, #editInvoiceButton').hide();
	});

	// For Customer name input autocomplete, which then grab the cust name and cust id.
	$( "#addCustomerName" ).autocomplete({
		source: "<?php echo base_url('CInvoices/getCustomersByName');?>",
		//source: [ { label: "Syabith Umar Ahdan", value: "1" }],
		select: function( event, ui ) {
			$( "#addCustomerName" ).val(ui.item.value);
			$( "#addCustomerID" ).val(ui.item.id);
		},
		response: function( event, ui ) {
			//console.log(ui);
		}
	});

</script>
</body>
</html>
