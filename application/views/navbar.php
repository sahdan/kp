<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	require('header.php');
	if(!isset($_SESSION["emp_id"])){
		// var_dump($_SESSION["emp_id"]);
		header("Location: " . base_url('/CLogin/'));
	}
?>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header">
				<!--
					Tip 1: You can change the background color of the logo header using: data-background-color="black | dark | blue | purple | light-blue | green | orange | red"
				-->
				<a href="<?php echo base_url('CDashboard/index'); ?>" class="big-logo">
					<img src="<?php echo base_url('/assets/img/logoresponsive.png'); ?>" alt="logo img" class="logo-img">
				</a>
				<a href="<?php echo base_url('CDashboard/index'); ?>" class="logo">
					<img src="<?php echo base_url('/assets/img/logoheader.png'); ?>" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="la la-bars"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="la la-ellipsis-v"></i></button>
			</div>
			<!-- End Logo Header -->
			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="dark">
				<!--
					Tip 1: You can change the background color of the navbar header using: data-background-color="black | dark | blue | purple | light-blue | green | orange | red"
				-->
				<div class="container-fluid">
					<div class="navbar-minimize">
						<button class="btn btn-minimize btn-rounded">
							<i class="la la-navicon"></i>
						</button>
					</div>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>
		<!-- Sidebar -->
		<div class="sidebar">
			<!--
				Tip 1: You can change the background color of the sidebar using: data-background-color="black | dark | blue | purple | light-blue | green | orange | red"
				Tip 2: you can also add an image using data-image attribute
			-->
			<div class="sidebar-wrapper scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="photo">
							<?php if($_SESSION["emp_access_level"] != "1") { ?>
								<img src="<?php echo base_url('/assets/img/staff_profile.png'); ?>" alt="image profile">
							<?php } else { ?>
								<img src="<?php echo base_url('/assets/img/boss_profile.jpg'); ?>" alt="image profile">
							<?php } ?>
						</div>
						<div class="info">
							<a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<?php echo $_SESSION["emp_name"]; ?>
									<span class="user-level">
										<?php if($_SESSION["emp_access_level"] != "1") { ?>
											Operator
										<?php } else { ?>
											Owner
										<?php } ?>
									</span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="<?php echo base_url('CLogin/logout'); ?>">
											<span class="link-collapse">Log Out</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav">
						<?php if($_SESSION["emp_access_level"] == "1") { ?>
						<li class="nav-item">
							<a href="<?php echo base_url('CDashboard/index'); ?>">
								<i class="flaticon-home"></i>
								<p>Dashboard</p>
							</a>
						</li>
						<?php } ?>
						<li class="nav-item">
							<a data-toggle="collapse" href="#sales">
								<i class="flaticon-credit-card-1"></i>
								<p>Sales</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="sales">
								<ul class="nav nav-collapse">
									<li>
										<a href="<?php echo base_url('CInvoices/index'); ?>">
											<span class="sub-item">Invoices</span>
										</a>
									</li>
									<?php if($_SESSION["emp_access_level"] == "1") { ?>
									<li>
										<a href="<?php echo base_url('CInvoices/commissions'); ?>">
										<span class="sub-item">Commissions</span>
										</a>
									</li>
									<?php } ?>
									<li>
										<a href="<?php echo base_url('CCustomers/index'); ?>">
											<span class="sub-item">Customers</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('CProducts/index'); ?>">
											<span class="sub-item">Product & Services</span>
										</a>
									</li>
									<!--<li>
										<a href="<?php echo base_url('CReminders/index'); ?>">
											<span class="sub-item">Reminders</span>
										</a>
									</li>-->
									<li>
										<a href="<?php echo base_url('COtherIncomes/index'); ?>">
											<span class="sub-item">Other Incomes</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#purchases">
								<i class="flaticon-cart"></i>
								<p>Purchases</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="purchases">
								<ul class="nav nav-collapse">
									<li>
										<a href="<?php echo base_url('CCostJournals/index'); ?>">
											<span class="sub-item">Cost Journal</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('CVendors/index'); ?>">
											<span class="sub-item">Vendors</span>
										</a>
									</li>
									<li>
										<a href="<?php echo base_url('CVendorCPs/index'); ?>">
											<span class="sub-item">Vendors Contact Person</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#accounting">
								<i class="flaticon-coins"></i>
								<p>Accounting</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="accounting">
								<ul class="nav nav-collapse">
									<li>
										<a href="<?php echo base_url('CTransactions/index'); ?>">
											<span class="sub-item">Transactions</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<?php if($_SESSION["emp_access_level"] == "1") { ?>
						<li class="nav-item">
							<a href="<?php echo base_url('CReports/index'); ?>">
								<i class="flaticon-graph"></i>
								<p>Reports</p>
							</a>
						</li>
						<?php } ?>
						<?php if($_SESSION["emp_access_level"] == "1") { ?>
						<li class="nav-item">
						<a href="<?php echo base_url('CEmployees/index'); ?>">
							<i class="flaticon-users"></i>
							<p>Employees</p>
						</a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
        </div>
