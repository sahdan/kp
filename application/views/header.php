<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<title>BB Tour & Travel</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico') ?>" type="image/x-icon">
    
    <!-- Fonts and icons -->
    <script src="<?php echo base_url('/assets/js/plugin/webfont/webfont.min.js'); ?>"></script>
    <script>
            WebFont.load({
            google: {"families":["Montserrat:100,200,300,400,500,600,700,800,900"]},
            custom: {"families":["Flaticon", "LineAwesome"], urls: ['<?php echo base_url('/assets/css/fonts.css'); ?>']},
            active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!-- CSS Files -->
    <link href="<?php echo base_url('/assets/css/jquery-ui.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('/assets/css/ready.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('/assets/css/demo.css'); ?>" rel="stylesheet">
    <!-- Custom CSS file made for this website -->
    <link href="<?php echo base_url('/assets/css/custom.css'); ?>" rel="stylesheet">        

    <!-- 
    <link href="<?php echo base_url('/assets/css/ready.min.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js') ?>"></script>
	<link href="<?php echo base_url('assets/css/materialize.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/css.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/highcharts.css'); ?>" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/dropify.min.css'); ?>" rel="stylesheet">
	

	<script src="<?php echo base_url('assets/js/materialize.min.js') ?>"></script>
	
	<script src="<?php echo base_url('assets/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/dropify.min.js') ?>"></script>
	
	
	<script src="<?php echo base_url('assets/js/highstock.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/modules/series-label.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/modules/exporting.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/modules/export-data.js'); ?>"></script>
	-->
</head>