<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReports extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('mInvoices');
		$this->load->model('mOtherIncomes');
		$this->load->model('mCostJournals');
    }

	public function index()
	{
		if($_SESSION["emp_access_level"] != "1") {
			redirect('CInvoices/index');
		} else {
			if ($this->input->post_get('edMonth') !== null && $this->input->post_get('edYear') !== null) {
				$month = $this->input->post_get('edMonth');
				$year = $this->input->post_get('edYear');
				$data["bulan"] = $this->getMonthNameIndonesia($month);
				$data["month"] = $month;
				$data["year"] = $year;


				// Getting Sales and Income Data
				$data['flightTicketSales'] = $this->mInvoices->selectCategoryTotalSalesByMonthYearAndCategory($month, $year, 'Flight Ticket')->total;
				$data['trainTicketSales'] = $this->mInvoices->selectCategoryTotalSalesByMonthYearAndCategory($month, $year, 'Train Ticket')->total;
				$data['hotelVoucherSales'] = $this->mInvoices->selectCategoryTotalSalesByMonthYearAndCategory($month, $year, 'Hotel Voucher')->total;
				$data['tourPackageSales'] = $this->mInvoices->selectCategoryTotalSalesByMonthYearAndCategory($month, $year, 'Tour Package')->total;
				$data["carRentalSales"] = $this->mInvoices->selectCategoryTotalSalesByMonthYearAndCategory($month, $year, 'Car Rental')->total;
				$data["operasionalIncomeTotal"] = $data['flightTicketSales'] + $data['trainTicketSales'] + $data['hotelVoucherSales'] + $data['tourPackageSales'] + $data["carRentalSales"];
				$data["otherIncomes"] = $this->mOtherIncomes->selectOtherIncomesNonBankByMonthYear($month, $year);
				$oiTotal = 0;

				foreach ($data["otherIncomes"] as $item) {
					$oiTotal = $oiTotal + $item->oi_amount;
				}

				$data["otherIncomesTotal"] = $oiTotal;
				$data["incomeTotal"] = $data["operasionalIncomeTotal"] + $data["otherIncomesTotal"];


				// Getting Spending and Costs Data
				$data['flightTicketSpending'] = $this->mInvoices->selectCategoryTotalSpendingByMonthYearAndCategory($month, $year, 'Flight Ticket')->total;
				$data['trainTicketSpending'] = $this->mInvoices->selectCategoryTotalSpendingByMonthYearAndCategory($month, $year, 'Train Ticket')->total;
				$data['hotelVoucherSpending'] = $this->mInvoices->selectCategoryTotalSpendingByMonthYearAndCategory($month, $year, 'Hotel Voucher')->total;
				$data['tourPackageSpending'] = $this->mInvoices->selectCategoryTotalSpendingByMonthYearAndCategory($month, $year, 'Tour Package')->total;
				$data["carRentalSpending"] = $this->mInvoices->selectCategoryTotalSpendingByMonthYearAndCategory($month, $year, 'Car Rental')->total;
				$data["operasionalSpendingTotal"] = $data['flightTicketSpending'] + $data['trainTicketSpending'] + $data['hotelVoucherSpending'] + $data['tourPackageSpending'] + $data["carRentalSpending"];

				$data["grossProfit"] = $data["incomeTotal"] - $data["operasionalSpendingTotal"];


				// Getting Business Costs Data
				$data["businessCosts"] = $this->mCostJournals->selectCostJournalsByMonthYearCategory($month, $year, "Biaya Usaha");
				$bcTotal = 0;

				foreach ($data["businessCosts"] as $item) {
					$bcTotal = $bcTotal + $item->cj_amount;
				}

				$data["businessCostsTotal"] = $bcTotal;


				// Getting General and Administrative Cost Data
				$data["commissionsTotal"] = $this->mInvoices->selectCommissionTotalByMonthYear($month, $year)->total;

				$data["GeneralAndAdministrativeCosts"] = $this->mCostJournals->selectCostJournalsByMonthYearCategory($month, $year, "Biaya Umum dan Administrasi Lainnya");

				$gacTotal = 0;
				foreach ($data["GeneralAndAdministrativeCosts"] as $item) {
					$gacTotal = $gacTotal + $item->cj_amount;
				}

				$data["GeneralAndAdministrativeCostsTotal"] = $gacTotal + $data["commissionsTotal"];
				$data["costsTotal"] = $data["GeneralAndAdministrativeCostsTotal"] + $data["businessCostsTotal"];
				$data["netProfitLoss"] = $data["grossProfit"] - $data["costsTotal"];

				// Getting Income and Cost for Banking data
				$data["otherBankIncome"] = $this->mOtherIncomes->selectOtherIncomesBankTotalByMonthYear($month, $year)->total;
				$data["OtherCost"] = $this->mCostJournals->selectCostJournalsTotalByMonthYearCategory($month, $year, "Biaya Lain-lain")->total;
				$data["bankAdminCost"] = $this->mCostJournals->selectCostJournalsTotalByMonthYearCategory($month, $year, "Biaya Admin dan Provisi Bank")->total;
				$data["bankInterestCost"] = $this->mCostJournals->selectCostJournalsTotalByMonthYearCategory($month, $year, "Biaya Bunga Bank")->total;
				$data["otherBankIncomeAndCostTotal"] = $data["otherBankIncome"] - $data["OtherCost"] - $data["bankAdminCost"] - $data["bankInterestCost"];

				// Calculate net Income
				$data["earningBeforeTax"] = $data["netProfitLoss"] + $data["otherBankIncomeAndCostTotal"];

				if ($data["earningBeforeTax"] < 0) {
					$data["tax"] = 0;
				} else {
					$data["tax"] = 0.1 * $data["earningBeforeTax"];
				}

				$data["earningAfterTax"] = $data["earningBeforeTax"] - $data["tax"];

				$data["retainedEarning"] = 0.2 * $data["earningAfterTax"];

				$data["netIncome"] = $data["earningAfterTax"] - $data["retainedEarning"];

				// Getting Employees Sales and Commission
				$data["employees"] = $this->mInvoices->selectEmployeesSalesCommissionByMonthYear($month, $year);

				$data["load"] = true;
			} else {
				$data["load"] = false;
			}
			if ($this->input->post_get('edAction') == 'print')
				$this->load->view('reportprint', $data);
			else
				$this->load->view('reports', $data);
		}
	}

	private function getMonthNameIndonesia($num)
	{
		if ($num == 1)
			return 'Januari';
		else if ($num == 2)
			return 'Februari';
		else if ($num == 3)
			return 'Maret';
		else if ($num == 4)
			return 'April';
		else if ($num == 5)
			return 'Mei';
		else if ($num == 6)
			return 'Juni';
		else if ($num == 7)
			return 'Juli';
		else if ($num == 8)
			return 'Agustus';
		else if ($num == 9)
			return 'September';
		else if ($num == 10)
			return 'Oktober';
		else if ($num == 11)
			return 'November';
		else if ($num == 12)
			return 'Desember';
		else
			return 'number of Month Invalid';
	}
}
