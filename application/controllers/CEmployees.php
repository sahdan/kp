<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CEmployees extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mEmployees');
		$this->load->library('session');
		$this->load->library("phpmailer_library");

	}

	public function index($code = 0)
	{
		if($_SESSION["emp_access_level"] != "1") {
			redirect('CInvoices/index');
		} else {
			$data["employees"] = $this->mEmployees->selectAllEmployees();
			if($code == 1) {
				$data["notif_message"] = "New employee is succesfully added";
			} elseif ($code == 2) {
				$data["notif_message"] = "Employee is succesfully edited";
			} elseif ($code == 3) {
				$data["notif_message"] = "Employee is succesfully deleted";
			}
			$this->load->view('employees', $data);
		}

	}

	public function addEmployee()
	{
		$empData["emp_username"] = $this->input->get_post('edUsername');
		$empData["emp_password"] = md5($this->input->get_post('edPassword'));
		$empData["emp_name"] = $this->input->get_post('edName');
		$empData["emp_id_card_number"] = $this->input->get_post('edIdCardNumber');
		$empData["emp_email"] = $this->input->get_post('edEmail');
		$empData["emp_phone"] = $this->input->get_post('edPhone');
		$empData["emp_address"] = $this->input->get_post('edAddress');
		$empData["emp_birthdate"] = $this->input->get_post('edBirthdate');
		// if ($this->input->get_post('edAccessLevel') == 'Operator') {
		// 	$empData["emp_access_Level"] = 0;
		// } else {
		// 	$empData["emp_access_Level"] = 1;
		// }
		$empData["emp_access_Level"] = 0;
		$empData["emp_status"] = 1;
		$this->mEmployees->addEmployee($empData);
		redirect('CEmployees/index/1');
	}

	public function editEmployee()
	{
		$empID = $this->input->get_post('edID');
		if ($this->input->get_post('edPassword') != '') {
			$empData["emp_password"] = md5($this->input->get_post('edPassword'));
		}
		$empData["emp_name"] = $this->input->get_post('edName');
		$empData["emp_id_card_number"] = $this->input->get_post('edIdCardNumber');
		$empData["emp_email"] = $this->input->get_post('edEmail');
		$empData["emp_phone"] = $this->input->get_post('edPhone');
		$empData["emp_address"] = $this->input->get_post('edAddress');
		$empData["emp_birthdate"] = $this->input->get_post('edBirthdate');
		// if ($this->input->get_post('edAccessLevel') == 'Operator') {
		// 	$empData["emp_access_Level"] = 0;
		// } else {
		// 	$empData["emp_access_Level"] = 1;
		// }
		$this->mEmployees->editEmployee($empID, $empData);
		redirect('CEmployees/index/2');
	}

	public function deleteEmployee()
	{
		$empID = $this->input->get_post('edID');
		$empData["emp_status"] = 0;
		$this->mEmployees->deleteEmployee($empID, $empData);
		redirect('CEmployees/index/3');
	}

	public function getEmployeeByID()
	{
		$empID = $this->input->get_post('empID');
		$empData = $this->mEmployees->selectEmployeeByID($empID);
		echo json_encode($empData);
	}

	public function getEmployeeNameByID()
	{
		$empID = $this->input->get_post('empID');
		$empName = $this->mEmployees->selectEmployeeNameByID($empID);
		echo json_encode($empName);

	}

	public function checkUsernameExist()
	{
		$empUsername = $this->input->get_post('empUsername');
		$count = $this->mEmployees->countEmployeeByUsername($empUsername);
		$return = "This username is already taken";
		if($count == 0){
			$return = true;
		}
		echo json_encode($return);
	}

	public function checkPasswordStrength()
	{
		$pwd = $this->input->get_post('edPassword');
		$failed = false;
		$error = "Password must include a number, an uppercase and lowercase letter, and a symbol";

		if( !preg_match("#[0-9]+#", $pwd) ) {
		$failed = true;
		}

		if( !preg_match("#[a-z]+#", $pwd) ) {
		$failed = true;
		}

		if( !preg_match("#[A-Z]+#", $pwd) ) {
		$failed = true;
		}

		if( !preg_match("#\W+#", $pwd) ) {
		$failed = true;
		}

		if($failed){
			echo json_encode($error);
		} else {
			echo json_encode(true);
		}
	}

	public function resetPassword()
	{
		$empID = $this->input->get_post('empID');
		$emp["emp_reset_token"] = bin2hex(openssl_random_pseudo_bytes(16));
		$tomorrow = time() + 60 * 60;
		$emp["emp_token_expire"] = date('Y-m-d H:i:s', $tomorrow);
		$this->mEmployees->editEmployee($empID, $emp);
		$empData = $this->mEmployees->selectEmployeeByID($empID);

		$mail = $this->phpmailer_library->load();
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';

		/* Username (email address). */
		$mail->Username = 'bbtourtravel.surabaya@gmail.com';

		/* Google account password. */
		$mail->Password = 'bbtourtravelsurabaya';

		try {

			/* Set the mail sender. */
			$mail->setFrom('bbtourtravel.surabaya@gmail.com', 'BB Tour & Travel Surabaya');

			/* Add a recipient. */
			$mail->addAddress($empData->emp_email, $empData->emp_name);

			/* Set the subject. */
			$mail->Subject = 'Reset Password Request';

			/* Set the mail message body as HTML type */
			$mail->isHTML(TRUE);

			/* Set the mail message body. */
			$mail->Body = "<p>Please click this <a href='". base_url("CEmployees/newPassword") . "?token=" . $emp["emp_reset_token"] ."'>link</a> to reset your password. " .
										"The link is valid until " . $empData->emp_token_expire;

			/* Finally send the mail. */
			$mail->send();
		 }
		 catch (Exception $e)
		 {
			/* PHPMailer exception. */
			echo $e->errorMessage();
		 }
		 catch (\Exception $e)
		 {
			/* PHP exception (note the backslash to select the global namespace Exception class). */
			echo $e->getMessage();
		 }

		 echo "The password reset link has been sent to " . $empData->emp_email;
	}

	public function newPassword()
	{
		$token = $this->input->get_post('token');
		$time = date('Y-m-d H:i:s', time());
		$data["employee"] = $this->mEmployees->selectEmployeeByToken($token, $time);
		if ($data["employee"] != null) {
			$this->load->view('newpassword', $data);
		} else {
			$this->output->append_output("Token has expired. Please request for a new token.");
		}
	}

	public function updatePassword()
	{
		$empID = $this->input->get_post('edID');
		$empData["emp_password"] = md5($this->input->get_post('edPassword'));
		$empData["emp_reset_token"] = null;
		$empData["emp_token_expire"] = null;
		$this->mEmployees->editEmployee($empID, $empData);
		redirect('CLogin/login');
	}
}
