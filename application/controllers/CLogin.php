<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLogin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mEmployees');
		$this->load->library('session');
	}
	
	public function index()
	{
		$this->load->view('login');
	}
	
	public function loginFail()
	{
        $this->load->view('login');
	}
	
	public function login()
	{
		if(!isset($_SESSION["emp_id"])){
            $data["username"] = $this->input->get_post('edUsername');
			$data["password"] = $this->input->get_post('edPassword');
			$check = 3;
			foreach($this->mEmployees->checkLogin($data["username"], md5($data["password"])) as $data){
				$_SESSION["emp_id"] = $data->emp_id;
				$_SESSION["emp_name"] = $data->emp_name;
				$_SESSION["emp_access_level"] = $data->emp_access_level;
				$check = $data->emp_access_level;
			}
			if($check == 1){ // Admin/Boss access level
				Redirect(base_url('CDashboard'), false);
			}elseif ($check == 0) { //Employee access level
				Redirect(base_url('CInvoices'), false);
			} else {
				Redirect(base_url('CLogin/loginFail'), false);
			}
		}
		// If Session exists, check access level
		else {
			if($_SESSION["emp_access_level"] == 1) { // Admin/Boss access level
				Redirect(base_url('CDashboard'), false);
			} else { //Employee access level
				Redirect(base_url('CInvoices'), false);
			}
		}
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('CLogin/index'), false);
	}
}
