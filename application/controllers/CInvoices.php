<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CInvoices extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mInvoices');
		$this->load->model('mCustomers');
		$this->load->model('mProducts');
		$this->load->model('mReminders');
		$this->load->model('mTransactions');
		$this->load->library('session');
		$this->load->library("phpmailer_library");
	}

	public function index()
	{
		$data["invoices"] = $this->mInvoices->selectAllInvoices();
		$data["unpaids"] = $this->mInvoices->selectAllUnpaidInvoices();
		$data["drafts"] = $this->mInvoices->selectAllDraftInvoices();
		$data["totalOverdue"] = $this->mInvoices->selectAmountOverDueTotal()->due_total;
		$data["totalOverdueIn30Days"] = $this->mInvoices->selectAmountDueTotalIn30Days()->due_total;
		$code = $this->session->invoiceNotif;
		if($code == 1) {
			$data["notif_message"] = "New Invoice is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Invoice is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Invoice is succesfully voided";
		} elseif ($code == 4) {
			$data["notif_message"] = "Invoice draft is succesfully saved";
		}
		unset($_SESSION['invoiceNotif']);
		$this->load->view('invoices', $data);
	}

	public function add()
	{
		$data["products"] = $this->mProducts->selectAllProductsName();
		$data["customers"] = $this->mCustomers->selectAllCustomersName();
		$this->load->view('invoiceadd', $data);
	}

	public function edit($invoiceID)
	{
		$data["products"] = $this->mProducts->selectAllProductsName();
		$data["customers"] = $this->mCustomers->selectAllCustomersName();
		$data["invoiceHeader"] = $this ->mInvoices->selectInvoiceHeaderById($invoiceID);
		$data["invoiceDetails"] = $this ->mInvoices->selectInvoiceDetailById($invoiceID);
		$this->load->view('invoiceedit', $data);
	}

	public function view($invoiceID)
	{
		$data["products"] = $this->mProducts->selectAllProductsName();
		$data["customers"] = $this->mCustomers->selectAllCustomersName();
		$data["invoiceHeader"] = $this ->mInvoices->selectInvoiceHeaderById($invoiceID);
		$data["invoiceDetails"] = $this ->mInvoices->selectInvoiceDetailById($invoiceID);
		$data["reminders"] = $this->mReminders->selectRemindersByInvoiceID($invoiceID);
		$data["paymentHistory"] = $this->mTransactions->selectPaymentByInvoiceID($invoiceID);
		$data["amountDue"] = $data["invoiceHeader"]->invh_total - $this->mInvoices->selectAmountPaid($invoiceID)->ar_total;
		$this->load->view('invoiceview', $data);
	}

	public function commissions()
	{
		$data["commissions"] = $this->mInvoices->selectAllInvoiceDetailsCommissions();
		$code = $this->session->commissionNotif;
		if($code == 1) {
			$data["notif_message"] = "The commission is succesfully edited";
		}
		unset($_SESSION['commissionNotif']);
		$this->load->view('commissions', $data);
	}

	public function print($invoiceID)
	{
		$data["products"] = $this->mProducts->selectAllProductsName();
		$data["customers"] = $this->mCustomers->selectAllCustomersName();
		$data["invoiceHeader"] = $this ->mInvoices->selectInvoiceHeaderById($invoiceID);
		$data["invoiceDetails"] = $this ->mInvoices->selectInvoiceDetailById($invoiceID);
		$data["paymentHistory"] = $this->mTransactions->selectPaymentByInvoiceID($invoiceID);
		$data["amountDue"] = $data["invoiceHeader"]->invh_total - $this->mInvoices->selectAmountPaid($invoiceID)->ar_total;
		$this->load->view('invoiceprint', $data);
	}

	public function email($invoiceID)
	{
		$data["invoiceHeader"] = $this ->mInvoices->selectInvoiceHeaderById($invoiceID);
		$data["invoiceDetails"] = $this ->mInvoices->selectInvoiceDetailById($invoiceID);
		$this->load->view('invoiceemail', $data);
	}

	public function addInvoice()
	{
		$invhData["invh_date_created"] = $this->input->get_post('edDate');
		$invhData["invh_date_due"] = $this->input->get_post('edDueDate');
		$invhData["cust_id"] = $this->input->get_post('edCustomerID');
		$invhData["invh_created_by"] = $this->input->get_post('edEmpID');

		if (isset($_POST['save'])) {
			$invhData["invh_status"] = "Draft";
			$notifCode = 2;
		} else {
			$invhData["invh_status"] = "Sent";
			$notifCode = 1;
		}

		$this->mInvoices->addInvoiceHeader($invhData);

		// Obtain the ID of newly submitted Invoice Header
		foreach($this->mInvoices->selectMaxInvoiceId() as $data){
			$invh_id = $data->maxId;
		}


		for($count = 0; $count<count($this->input->get_post('hiddenProductID')); $count++)
		{
			//Enter the invoice detail to tabel t_invoice_d
			$invdData["invh_id"] = $invh_id;
			$invdData["ps_id"] = $this->input->get_post('hiddenProductID')[$count];
			$invdData["invd_description"] = $this->input->get_post('hiddenDescription')[$count];
			$invdData["invd_quantity"] = $this->input->get_post('hiddenQuantity')[$count];
			$invdData["invd_cost"] = $this->input->get_post('hiddenCost')[$count];
			$invdData["invd_price_display"] = $this->input->get_post('hiddenDisplayPrice')[$count];
			$invdData["invd_price_actual"] = $this->input->get_post('hiddenActualPrice')[$count];
			$invdData["invd_commission"] = 0; //Place Holder for now
			$this->mInvoices->addInvoiceDetail($invdData);

			if ($notifCode == 1) {
				//Enter the account payable detail to table t_ap
				$apData["ps_id"] = $this->input->get_post('hiddenProductID')[$count];
				$apData["ap_bill_id"] = $invh_id;
				$apData["ap_bill_type"] = 'Invoice';
				$apData["ap_date"] = $this->input->get_post('edDate');
				$apData["ap_amount"] = $invdData["invd_cost"] * $invdData["invd_quantity"];
				$apData["ap_payment_type"] = 'BCA';
				$apData["ap_notes"] = $invdData["invd_description"];
				$apData["ap_created_by"] = $this->input->get_post('edEmpID');
				$apData["ap_status"] = 1;
				$this->mTransactions->addAccountPayable($apData);
			}
		}

		if ($notifCode == 1) {
			$this->sendInvoiceEmailByInvoiceID($invh_id);
		}

		if ($notifCode == 1) {
			redirect('CInvoices/view/' . $invh_id);
		} else {
		    $this->session->set_userdata('invoiceNotif', $notifCode);
		    redirect('CInvoices/index');
		}
	}

	public function editInvoice()
	{
		$invh_id = $this->input->get_post('edInvID');
		$invhData["invh_date_created"] = $this->input->get_post('edDate');
		$invhData["invh_date_due"] = $this->input->get_post('edDueDate');
		$invhData["cust_id"] = $this->input->get_post('edCustomerID');

		// If not boss, update the creator employee ID to the newest one.
		if ($_SESSION["emp_access_level"] != "1") {
			$invhData["invh_created_by"] = $this->input->get_post('edEmpID');
		}

		if (isset($_POST['save'])) {
			$invhData["invh_status"] = "Draft";
			$notifCode = 2;
		} else {
			$invhData["invh_status"] = "Sent";
			$notifCode = 1;
		}

		$this->mInvoices->editInvoiceHeader($invh_id, $invhData);

		// Delete all invoice detail from the edited Invoice Header
		$this->mInvoices->deleteInvoiceDetailById($invh_id);

		//Enter the invoice detail to tabel t_invoice_d
		for($count = 0; $count<count($this->input->get_post('hiddenProductID')); $count++)
		{
			$invdData["invh_id"] = $invh_id;
			$invdData["ps_id"] = $this->input->get_post('hiddenProductID')[$count];
			$invdData["invd_description"] = $this->input->get_post('hiddenDescription')[$count];
			$invdData["invd_quantity"] = $this->input->get_post('hiddenQuantity')[$count];
			$invdData["invd_cost"] = $this->input->get_post('hiddenCost')[$count];
			$invdData["invd_price_display"] = $this->input->get_post('hiddenDisplayPrice')[$count];
			$invdData["invd_price_actual"] = $this->input->get_post('hiddenActualPrice')[$count];
			$this->mInvoices->addInvoiceDetail($invdData);

			if ($notifCode == 1) {
				//Enter the account payable detail to table t_ap
				$apData["ps_id"] = $this->input->get_post('hiddenProductID')[$count];
				$apData["ap_bill_id"] = $invh_id;
				$apData["ap_bill_type"] = 'Invoice';
				$apData["ap_date"] = $this->input->get_post('edDate');
				$apData["ap_amount"] = $invdData["invd_cost"] * $invdData["invd_quantity"];
				$apData["ap_payment_type"] = 'BCA';
				$apData["ap_notes"] = $invdData["invd_description"];
				$apData["ap_created_by"] = $this->input->get_post('edEmpID');
				$apData["ap_status"] = 1;
				$this->mTransactions->addAccountPayable($apData);
			}
		}

		if ($notifCode == 1) {
			$this->sendInvoiceEmailByInvoiceID($invh_id);
		}

		if ($notifCode == 1) {
			redirect('CInvoices/view/' . $invh_id);
		} else {
		    $this->session->set_userdata('invoiceNotif', $notifCode);
		    redirect('CInvoices/index');
		}
	}

	public function editCommission()
	{
		$invoiceID = $this->input->get_post('edInvoiceID');
		$psID = $this->input->get_post('edProductID');
		$data["invd_commission"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$this->mInvoices->editInvoiceDetailByIDs($invoiceID, $psID, $data);
		$this->session->set_userdata('commissionNotif', '1');
		redirect('CInvoices/commissions');

	}

	public function deleteInvoice()
	{
		$invhID = $this->input->get_post('edID');
		$invhData["invh_status"] = "Voided";
		$this->mInvoices->deleteInvoiceHeaderById($invhID, $invhData);

		$apData["ap_status"] = 0;
		$this->mInvoices->deleteAccountPayableByInvoiceID($invhID, $apData);

		$arData["ar_status"] = 0;
		$this->mInvoices->deleteAccountReceivableByInvoiceID($invhID, $arData);

		$this->session->set_userdata('invoiceNotif', '3');
		redirect('CInvoices/index');
	}

	public function getInvoiceByID()
	{
		$invhID = $this->input->get_post('invhID');
		$invhData = $this->mInvoices->selectInvoiceByID($invhID);
		echo json_encode($invhData);
	}

	public function getAcountPayableByCJID()
	{
		$invhID = $this->input->get_post('invhID');
		$apData = $this->mInvoices->selectAcountPayableByCJID($invhID);
		echo json_encode($apData);
	}

	public function getInvoiceNameByID()
	{
		$invhID = $this->input->get_post('invhID');
		$invhName = $this->mInvoices->selectInvoiceNameByID($invhID);
		echo json_encode($invhName);

	}

	public function getCustomersByName()
	{
		if (isset($_GET['term'])) {
			$result = $this->mCustomers->selectCustomersByName($_GET['term']);
			if (count($result) > 0) {
				foreach ($result as $row)
					$arr_result[] = array(
						'value'         => $row->cust_name,
						'id'   => $row->cust_id,
				 );
					echo json_encode($arr_result);
			}
        }
	}

	public function getInvoiceDetailByIDs()
	{
		$invoiceID = $this->input->get_post('invhID');
		$psID = $this->input->get_post('psID');
		$invdData = $this->mInvoices->selectInvoiceDetailByIDs($invoiceID, $psID);
		echo json_encode($invdData);
	}

	public function parseCurrency($str)
	{
    	return intval(preg_replace("/[^0-9]/", "", $str));
	}

	public function sendInvoiceEmailByInvoiceID($invoiceID)
	{
		$mail = $this->phpmailer_library->load();
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';

		/* Username (email address). */
		$mail->Username = 'bbtourtravel.surabaya@gmail.com';

		/* Google account password. */
		$mail->Password = 'bbtourtravelsurabaya';

		$data["invoiceHeader"] = $this ->mInvoices->selectInvoiceHeaderById($invoiceID);
		$data["invoiceDetails"] = $this ->mInvoices->selectInvoiceDetailById($invoiceID);
		$customer = $this->mCustomers->selectCustomerByID($data["invoiceHeader"]->cust_id);

		if ($customer->cust_email != '') {
			try {

				/* Set the mail sender. */
				$mail->setFrom('info@bbtourtravel.com', 'BB Tour & Travel Surabaya');

				/* Add a recipient. */
				$mail->addAddress($customer->cust_email, $customer->cust_name);

				/* Set the subject. */
				$mail->Subject = 'Inv BB-' . $invoiceID;


				$invoice = $this->load->view('invoiceemail', $data, TRUE);

				/* Set the mail message body as HTML type */
				$mail->isHTML(TRUE);

				/* Set the mail message body. */
				$mail->Body = $invoice;

				/* Finally send the mail. */
				$mail->send();
			 }
			 catch (Exception $e)
			 {
				/* PHPMailer exception. */
				echo $e->errorMessage();
			 }
			 catch (\Exception $e)
			 {
				/* PHP exception (note the backslash to select the global namespace Exception class). */
				echo $e->getMessage();
			 }
		}


	}

	public function sendReceipt()
	{
		$mail = $this->phpmailer_library->load();
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';

		/* Username (email address). */
		$mail->Username = 'bbtourtravel.surabaya@gmail.com';

		/* Google account password. */
		$mail->Password = 'bbtourtravelsurabaya';

		$ar_id = $this->input->get_post('arID');
		$invoiceID = $this->input->get_post('invhID');
		$ar = $this->mTransactions->selectAccountReceivableByID($ar_id);
		$invh = $this->mInvoices->selectInvoiceHeaderById($invoiceID);

		$customer = $this->mCustomers->selectCustomerByID($invh->cust_id);

		if ($customer->cust_email != '') {
			try {

				/* Set the mail sender. */
				$mail->setFrom('info@bbtourtravel.com', 'BB Tour & Travel Surabaya');

				/* Add a recipient. */
				$mail->addAddress($customer->cust_email, $customer->cust_name);

				/* Set the subject. */
				$mail->Subject = 'Payment Receipt for Inv BB-' . $invoiceID;

				/* Set the mail message body as HTML type */
				$mail->isHTML(TRUE);

				/* Set the mail message body. */
				$mail->Body = "Terima kasih kepada Bapak/Ibu " . $customer->cust_name . " atas transaksi pembayaran tagihan invoice BB Tour Travel Anda sebesar Rp. " . $ar->ar_amount . " pada tanggal " . date("d-m-Y", strtotime($ar->ar_date)) .  ". ID Pembayaran anda adalah " . $ar_id . ". Mohon simpan email sebagai bukti pembayaran anda.";

				/* Finally send the mail. */
				$mail->send();
			 }
			 catch (Exception $e)
			 {
				/* PHPMailer exception. */
				echo $e->errorMessage();
			 }
			 catch (\Exception $e)
			 {
				/* PHP exception (note the backslash to select the global namespace Exception class). */
				echo $e->getMessage();
			 }

			 echo "The receipt has been sent to " . $customer->cust_email;
		} else {
			echo "The customer has no registered email.";
		}

	}

	public function checkInvoiceStatus($invoiceID)
	{
		$invoiceHeader = $this->mInvoices->selectInvoiceHeaderById($invoiceID);
		$amountDue = $invoiceHeader->invh_total - $this->mInvoices->selectAmountPaid($invoiceID)->ar_total;
		if($amountDue > 0){
			if(time() - strtotime($invoiceHeader->invh_date_due) > 0){
				return "Overdue";
			}else{
				if($amountDue > 0){
					return "Partial";
				}else{
					return "Sent";
				}
			}
		}else{
			return "Paid";
		}
	}

	public function updateInvoiceStatus()
	{
		$this->mInvoices->updateOverdueStatus();
	}

	public function addPayment()
	{
		$invoiceID = $this->input->get_post('edInvoiceID');
		$paymentData["ar_income_id"] = $invoiceID;
		$paymentData["ar_income_type"] = 'Invoice';
		$paymentData["ar_date"] = $this->input->get_post('edDate');
		$paymentData["ar_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$paymentData["ar_payment_type"] = $this->input->get_post('edPaymentType');
		$paymentData["ar_notes"] = $this->input->get_post('edNotes');
		$paymentData["ar_created_by"] = $_SESSION["emp_id"];
		$paymentData["ar_status"] = 1;
		$this->mTransactions->addAccountReceivable($paymentData);
		$invhData["invh_status"] = $this->checkInvoiceStatus($invoiceID);
		$this->mInvoices->editInvoiceHeader($invoiceID, $invhData);
		redirect('CInvoices/view/'.$invoiceID);
	}

	public function deletePaymentInfo()
	{
		$paymentID = $this->input->get_post('edID');
		$data = $this->mTransactions->selectAccountReceivableById($paymentID);
		$return = date("jS \of F Y, ", strtotime($data->ar_date)).'Payment for Rp. '.number_format($data->ar_amount, 0, '.','.').' using ';
		switch(strtolower($data->ar_payment_type)){
			case "bni":
			$return .= 'bank transfer(BNI)';
			break;
			case "bca":
			$return .= 'bank transfer(BCA)';
			break;
			case "tunai":
			$return .= 'cash';
			break;
		}
		$return .= '<br/>';
		echo $return;
	}

	public function deletePayment()
	{
		$invoiceID = $this->input->get_post('edInvoiceID');
		$paymentID = $this->input->get_post('edID');
		$data["ar_status"] = '0';
		$this->mTransactions->deleteAccountReceivable($paymentID, $data);
		$invhData["invh_status"] = $this->checkInvoiceStatus($invoiceID);
		$this->mInvoices->editInvoiceHeader($invoiceID, $invhData);
		redirect('CInvoices/view/'.$invoiceID);
	}

	public function editPaymentInfo()
	{
		$paymentID = $this->input->get_post('edID');
		$data = $this->mTransactions->selectAccountReceivableById($paymentID);
		echo json_encode($data);
	}

	public function editPayment()
	{
		$invoiceID = $this->input->get_post('edInvoiceID');
		$paymentID = $this->input->get_post('edID');
		// $paymentData["invh_id"] = $invoiceID;
		$paymentData["ar_date"] = $this->input->get_post('edDate');
		$paymentData["ar_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$paymentData["ar_payment_type"] = $this->input->get_post('edPaymentType');
		$paymentData["ar_notes"] = $this->input->get_post('edNotes');
		// $paymentData["ar_created_by"] = $_SESSION["emp_id"];
		// $paymentData["ar_status"] = 1;
		$this->mTransactions->editAccountReceivable($paymentID, $paymentData);
		$invhData["invh_status"] = $this->checkInvoiceStatus($invoiceID);
		$this->mInvoices->editInvoiceHeader($invoiceID, $invhData);
		redirect('CInvoices/view/'.$invoiceID);
	}
}
