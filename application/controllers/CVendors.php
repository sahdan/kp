<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CVendors extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mVendors');
		$this->load->library('session');
	}
	
	public function index($code = 0)
	{
		$data["vendors"] = $this->mVendors->selectAllVendors();
		if($code == 1) {
			$data["notif_message"] = "New vendor is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Vendor is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Vendor is succesfully deleted";
		}
		$this->load->view('vendors', $data);
	}

	public function addVendor()
	{
		$vendData["vend_name"] = $this->input->get_post('edName');
		$vendData["vend_email"] = $this->input->get_post('edEmail');
		$vendData["vend_phone"] = $this->input->get_post('edPhone');
		$vendData["vend_address"] = $this->input->get_post('edAddress');
		$vendData["vend_status"] = 1;
		$this->mVendors->addVendor($vendData);
		redirect('CVendors/index/1');
	}

	public function editVendor()
	{
		$vendID = $this->input->get_post('edID');
		$vendData["vend_name"] = $this->input->get_post('edName');
		$vendData["vend_email"] = $this->input->get_post('edEmail');
		$vendData["vend_phone"] = $this->input->get_post('edPhone');
		$vendData["vend_address"] = $this->input->get_post('edAddress');
		$this->mVendors->editVendor($vendID, $vendData);
		redirect('CVendors/index/2');
	}

	public function deleteVendor()
	{
		$vendID = $this->input->get_post('edID');
		$vendData["vend_status"] = 0;
		$this->mVendors->deleteVendor($vendID, $vendData);
		redirect('CVendors/index/3');
	}

	public function getVendorByID()
	{
		$vendID = $this->input->get_post('vendID');
		$vendData = $this->mVendors->selectVendorByID($vendID);
		echo json_encode($vendData);
	}

	public function getVendorNameByID()
	{
		$vendID = $this->input->get_post('vendID');
		$vendName = $this->mVendors->selectVendorNameByID($vendID);
		echo json_encode($vendName);

	}
}
