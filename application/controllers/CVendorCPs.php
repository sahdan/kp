<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CVendorCPs extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mVendorCPs');
		$this->load->model('mVendors');
		$this->load->library('session');
	}
	
	public function index($code = 0)
	{
		if($this->input->get_post('edVendorName')){
			$data["selected_vendor"] = $this->input->get_post('edVendorName');
		}
		$data["vendorCPs"] = $this->mVendorCPs->selectAllVendorCPs();
		$data["vendors"] = $this->mVendors->selectAllVendorsName();
		if($code == 1) {
			$data["notif_message"] = "New vendor contact person is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Vendor contact person is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Vendor contact person is succesfully deleted";
		}
		$this->load->view('vendorcps', $data);
	}

	public function addVendorCP()
	{
		$vendData["vendcp_name"] = $this->input->get_post('edName');
		$vendData["vendcp_phone"] = $this->input->get_post('edPhone');
		$vendData["vend_id"] = $this->input->get_post('edVendor');
		$vendData["vendcp_position"] = $this->input->get_post('edPosition');
		$vendData["vendcp_status"] = 1;
		$this->mVendorCPs->addVendorCP($vendData);
		redirect('CVendorCPs/index/1');
	}

	public function editVendorCP()
	{
		$vendCPID = $this->input->get_post('edID');
		$vendData["vendcp_name"] = $this->input->get_post('edName');
		$vendData["vendcp_phone"] = $this->input->get_post('edPhone');
		$vendData["vend_id"] = $this->input->get_post('edVendor');
		$vendData["vendcp_position"] = $this->input->get_post('edPosition');
		$this->mVendorCPs->editVendorCP($vendCPID, $vendData);
		redirect('CVendorCPs/index/2');
	}

	public function deleteVendorCP()
	{
		$vendCPID = $this->input->get_post('edID');
		$vendData["vendcp_status"] = 0;
		$this->mVendorCPs->deleteVendorCP($vendCPID, $vendData);
		redirect('CVendorCPs/index/3');
	}

	public function getVendorCPByID()
	{
		$vendCPID = $this->input->get_post('vendCPID');
		$vendCPData = $this->mVendorCPs->selectVendorCPByID($vendCPID);
		echo json_encode($vendCPData);
	}

	public function getVendorCPNameByID()
	{
		$vendCPID = $this->input->get_post('vendCPID');
		$vendCPName = $this->mVendorCPs->selectVendorCPNameByID($vendCPID);
		echo json_encode($vendCPName);

	}
}
