<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CTransactions extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mTransactions');
		$this->load->model('mTransactions');
		$this->load->library('session');
	}
	
	public function index($code = 0)
	{
		$data["transactions"] = $this->mTransactions->selectAllTransactions();
		if($code == 1) {
			$data["notif_message"] = "New cost journal is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Cost Journal is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Cost Journal is succesfully deleted";
		}
		$this->load->view('transactions', $data);
	}

	public function addTransaction()
	{
		$tranData["tran_date"] = $this->input->get_post('edDate');
		$tranData["tran_name"] = $this->input->get_post('edName');
		$tranData["tran_category"] = $this->input->get_post('edCategory');
		$tranData["tran_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$tranData["tran_created_by"] = $this->input->get_post('edEmpID');
		$tranData["tran_status"] = 1;
		$this->mTransactions->addTransaction($tranData);

		//Enter the transaction to tabel t_ap
		foreach($this->mTransactions->selectMaxTransactionId() as $data){
			$apData["ap_bill_id"] = $data->maxId;
		}
		$apData["ap_bill_type"] = "Biaya";
		$apData["ap_date"] = $tranData["tran_date"] = $this->input->get_post('edDate');
		$apData["ap_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$apData["ap_payment_type"] = $this->input->get_post('edPaymentType');
		$apData["ap_notes"] = $this->input->get_post('edNotes');
		$apData["ap_created_by"] = $this->input->get_post('edEmpID');
		$apData["ap_status"] = 1;
		$this->mTransactions->addAccountPayable($apData);
		redirect('CTransactions/index/1');
	}

	public function editTransaction()
	{
		$tranID = $this->input->get_post('edID');
		$tranData["tran_date"] = $this->input->get_post('edDate');
		$tranData["tran_name"] = $this->input->get_post('edName');
		$tranData["tran_category"] = $this->input->get_post('edCategory');
		$tranData["tran_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$tranData["tran_created_by"] = $this->input->get_post('edEmpID');
		$this->mTransactions->editTransaction($tranID, $tranData);

		//Edit the transaction to tabel t_ap
		foreach($this->mTransactions->selectMaxTransactionId() as $data){
			$apData["ap_bill_id"] = $data->maxId;
		}
		$apData["ap_date"] = $tranData["tran_date"] = $this->input->get_post('edDate');
		$apData["ap_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$apData["ap_payment_type"] = $this->input->get_post('edPaymentType');
		$apData["ap_notes"] = $this->input->get_post('edNotes');
		$apData["ap_created_by"] = $this->input->get_post('edEmpID');
		$this->mTransactions->editAccountPayable($tranID, $apData);
		redirect('CTransactions/index/2');
	}

	public function deleteTransaction()
	{
		$tranID = $this->input->get_post('edID');
		$tranData["tran_status"] = 0;
		$this->mTransactions->deleteTransaction($tranID, $tranData);

		$apData["ap_status"] = 0;
		$this->mTransactions->deleteAccountPayable($tranID, $apData);

		redirect('CTransactions/index/3');
	}

	public function getTransactionByID()
	{
		$tranID = $this->input->get_post('tranID');
		$tranData = $this->mTransactions->selectTransactionByID($tranID);
		echo json_encode($tranData);
	}

	public function getAcountPayableByCJID()
	{
		$tranID = $this->input->get_post('tranID');
		$apData = $this->mTransactions->selectAcountPayableByCJID($tranID);
		echo json_encode($apData);
	}

	public function getTransactionNameByID()
	{
		$tranID = $this->input->get_post('tranID');
		$tranName = $this->mTransactions->selectTransactionNameByID($tranID);
		echo json_encode($tranName);

	}

	public function parseCurrency($str)
	{
    	return intval(preg_replace("/[^0-9]/", "", $str));
	}
}
