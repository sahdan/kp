<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CCostJournals extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mCostJournals');
		$this->load->model('mTransactions');
		$this->load->library('session');
	}
	
	public function index($code = 0)
	{
		$data["costJournals"] = $this->mCostJournals->selectAllCostJournals();
		if($code == 1) {
			$data["notif_message"] = "New cost journal is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Cost Journal is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Cost Journal is succesfully deleted";
		}
		$this->load->view('costjournals', $data);
	}

	public function addCostJournal()
	{
		$cjData["cj_date"] = $this->input->get_post('edDate');
		$cjData["cj_name"] = $this->input->get_post('edName');
		$cjData["cj_category"] = $this->input->get_post('edCategory');
		$cjData["cj_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$cjData["cj_created_by"] = $this->input->get_post('edEmpID');
		$cjData["cj_status"] = 1;
		$this->mCostJournals->addCostJournal($cjData);

		//Enter the transaction to tabel t_ap
		foreach($this->mCostJournals->selectMaxCostJournalId() as $data){
			$apData["ap_bill_id"] = $data->maxId;
		}
		$apData["ap_bill_type"] = "Biaya";
		$apData["ap_date"] = $cjData["cj_date"] = $this->input->get_post('edDate');
		$apData["ap_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$apData["ap_payment_type"] = $this->input->get_post('edPaymentType');

		// If notes is empty, automatically set is as cost journal's name
		if ($this->input->get_post('edNotes') == "") {
			$apData["ap_notes"] = $this->input->get_post('edName');
		} else {
			$apData["ap_notes"] = $this->input->get_post('edNotes');
		}
		$apData["ap_created_by"] = $this->input->get_post('edEmpID');
		$apData["ap_status"] = 1;
		$this->mTransactions->addAccountPayable($apData);
		redirect('CCostJournals/index/1');
	}

	public function editCostJournal()
	{
		$cjID = $this->input->get_post('edID');
		$cjData["cj_date"] = $this->input->get_post('edDate');
		$cjData["cj_name"] = $this->input->get_post('edName');
		$cjData["cj_category"] = $this->input->get_post('edCategory');
		$cjData["cj_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$cjData["cj_created_by"] = $this->input->get_post('edEmpID');
		$this->mCostJournals->editCostJournal($cjID, $cjData);

		//Edit the transaction to tabel t_ap
		$apData["ap_date"] = $cjData["cj_date"] = $this->input->get_post('edDate');
		$apData["ap_amount"] = $this->parseCurrency($this->input->get_post('edAmount'));
		$apData["ap_payment_type"] = $this->input->get_post('edPaymentType');
		$apData["ap_notes"] = $this->input->get_post('edNotes');
		$apData["ap_created_by"] = $this->input->get_post('edEmpID');
		$this->mTransactions->editAccountPayableByCJID($cjID, $apData);
		redirect('CCostJournals/index/2');
	}

	public function deleteCostJournal()
	{
		$cjID = $this->input->get_post('edID');
		$cjData["cj_status"] = 0;
		$this->mCostJournals->deleteCostJournal($cjID, $cjData);

		$apData["ap_status"] = 0;
		$this->mTransactions->deleteAccountPayableByCJID($cjID, $apData);

		redirect('CCostJournals/index/3');
	}

	public function getCostJournalByID()
	{
		$cjID = $this->input->get_post('cjID');
		$cjData = $this->mCostJournals->selectCostJournalByID($cjID);
		echo json_encode($cjData);
	}

	public function getAccountPayableByCJID()
	{
		$cjID = $this->input->get_post('cjID');
		$apData = $this->mTransactions->selectAccountPayableByCJID($cjID);
		echo json_encode($apData);
	}

	public function getCostJournalNameByID()
	{
		$cjID = $this->input->get_post('cjID');
		$cjName = $this->mCostJournals->selectCostJournalNameByID($cjID);
		echo json_encode($cjName);

	}

	public function parseCurrency($str)
	{
    	return intval(preg_replace("/[^0-9]/", "", $str));
	}
}
