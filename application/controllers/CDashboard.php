<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDashboard extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('mCustomers');
		$this->load->model('mInvoices');
		$this->load->model('mProducts');
    }

	public function index()
	{
		if($_SESSION["emp_access_level"] != "1") {
			redirect('CInvoices/index');
		} else {
			$data = $this->getData();
			$this->load->view('dashboard', $data);
		}
	}

	public function print()
	{
		$data = $this->getData();
		$this->load->view('dashboardprint', $data);
	}

	public function getData()
	{
		$data['newCustomersCount'] = $this->mCustomers->countNewCustomersThisMonth()->total;
		$data['invoicesCount'] = $this->mInvoices->countInvoicesThisMonth()->total;
		$data['sales'] = $this->mInvoices->selectSalesThisMonth()->sales;
		$data["unpaidTotal"] = $this->mInvoices->selectAmountDueTotal()->due_total;
		$data["categoriesTotal"] = $this->mInvoices->selectCategoriesTotalLast3Months();
		$data["salesTotal"] = $this->mInvoices->selectSalesTotalLast3Months();
		$data["employees"] = $this->mInvoices->selectEmployeesSalesCommissionThisMonth();
		$data["products"] = $this->mProducts->selectProductsOrdersSalesLast3Months();
		return $data;
	}
}
