<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CCustomers extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mCustomers');
		$this->load->library('session');
	}
	
	public function index($code = 0)
	{
		$data["customers"] = $this->mCustomers->selectAllCustomers();
		if($code == 1) {
			$data["notif_message"] = "New customer is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Customer is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Customer is succesfully deleted";
		}
		$this->load->view('customers', $data);
	}

	public function addCustomer()
	{
		$custData["cust_name"] = $this->input->get_post('edName');
		$custData["cust_title"] = $this->input->get_post('edTitle');
		$custData["cust_id_number"] = $this->input->get_post('edIdNumber');
		$custData["cust_id_type"] = $this->input->get_post('edIdType');
		$custData["cust_email"] = $this->input->get_post('edEmail');
		$custData["cust_phone"] = $this->input->get_post('edPhone');
		$custData["cust_address"] = $this->input->get_post('edAddress');
		$custData["cust_birthdate"] = $this->input->get_post('edBirthdate');
		$custData["cust_emer_name"] = $this->input->get_post('edEmerName');
		$custData["cust_emer_phone"] = $this->input->get_post('edEmerPhone');
		$custData["cust_date_created"] = date('Y-m-d');
		$custData["cust_status"] = 1;
		$this->mCustomers->addCustomer($custData);
		redirect('CCustomers/index/1');
	}

	public function editCustomer()
	{
		$custID = $this->input->get_post('edID');
		$custData["cust_name"] = $this->input->get_post('edName');
		$custData["cust_title"] = $this->input->get_post('edTitle');
		$custData["cust_id_number"] = $this->input->get_post('edIdNumber');
		$custData["cust_id_type"] = $this->input->get_post('edIdType');
		$custData["cust_email"] = $this->input->get_post('edEmail');
		$custData["cust_phone"] = $this->input->get_post('edPhone');
		$custData["cust_address"] = $this->input->get_post('edAddress');
		$custData["cust_birthdate"] = $this->input->get_post('edBirthdate');
		$custData["cust_emer_name"] = $this->input->get_post('edEmerName');
		$custData["cust_emer_phone"] = $this->input->get_post('edEmerPhone');
		$this->mCustomers->editCustomer($custID, $custData);
		redirect('CCustomers/index/2');
	}

	public function deleteCustomer()
	{
		$custID = $this->input->get_post('edID');
		$custData["cust_status"] = 0;
		$this->mCustomers->deleteCustomer($custID, $custData);
		redirect('CCustomers/index/3');
	}

	public function getCustomerByID()
	{
		$custID = $this->input->get_post('custID');
		$custData = $this->mCustomers->selectCustomerByID($custID);
		echo json_encode($custData);
	}

	public function getCustomerNameByID()
	{
		$custID = $this->input->get_post('custID');
		$custName = $this->mCustomers->selectCustomerNameByID($custID);
		echo json_encode($custName);

	}
}
