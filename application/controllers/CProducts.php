<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CProducts extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mProducts');
		$this->load->library('session');
	}
	
	public function index($code = 0)
	{
		$data["products"] = $this->mProducts->selectAllProducts();
		if($code == 1) {
			$data["notif_message"] = "New product & service is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Product & Service is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Product & Service is succesfully deleted";
		}
		$this->load->view('products', $data);
	}

	public function addProduct()
	{
		$psData["ps_name"] = $this->input->get_post('edName');
		$psData["ps_description"] = $this->input->get_post('edDescription');
		$psData["ps_category"] = $this->input->get_post('edCategory');
		$psData["ps_status"] = 1;
		$this->mProducts->addProduct($psData);
		redirect('CProducts/index/1');
	}

	public function editProduct()
	{
		$psID = $this->input->get_post('edID');
		$psData["ps_name"] = $this->input->get_post('edName');
		$psData["ps_description"] = $this->input->get_post('edDescription');
		$psData["ps_category"] = $this->input->get_post('edCategory');
		$this->mProducts->editProduct($psID, $psData);
		redirect('CProducts/index/2');
	}

	public function deleteProduct()
	{
		$psID = $this->input->get_post('edID');
		$psData["ps_status"] = 0;
		$this->mProducts->deleteProduct($psID, $psData);
		redirect('CProducts/index/3');
	}

	public function getProductByID()
	{
		$psID = $this->input->get_post('psID');
		$psData = $this->mProducts->selectProductByID($psID);
		echo json_encode($psData);
	}

	public function getProductNameByID()
	{
		$psID = $this->input->get_post('psID');
		$psName = $this->mProducts->selectProductNameByID($psID);
		echo json_encode($psName);

	}
}
