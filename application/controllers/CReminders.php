<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReminders extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('mReminders');
		$this->load->model('mCustomers');
		$this->load->model('mInvoices');
		$this->load->library('session');
		$this->load->library("phpmailer_library");
	}

	public function index($code = 0)
	{
		$data["reminders"] = $this->mReminders->selectAllReminders();
		if($code == 1) {
			$data["notif_message"] = "New reminder is succesfully added";
		} elseif ($code == 2) {
			$data["notif_message"] = "Reminder is succesfully edited";
		} elseif ($code == 3) {
			$data["notif_message"] = "Reminder is succesfully deleted";
		}
		$this->load->view('reminders', $data);
	}

	public function addReminder()
	{
		$remData["rem_date_created"] = date("Y-m-d");
		$remData["rem_date_remind"] = $this->input->get_post('edDateToRemind');
		$remData["invh_id"] = $this->input->get_post('edInvoiceID');
		$this->mReminders->addReminder($remData);
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$data["reminders"] = $this->mReminders->selectRemindersByInvoiceID($remData["invh_id"]);
			$this->load->view("ajax/reminder-list", $data);
		}else{
			redirect('CReminders/index/1');
		}
	}

	public function getReminderByID()
	{
		$remID = $this->input->get_post('remID');
		$remData = $this->mReminders->selectReminderByID($remID);
		echo json_encode($remData);
	}

	public function editReminder()
	{
		$remID = $this->input->get_post('edID');
		$remData["rem_date_created"] = date("Y-m-d");
		$remData["rem_date_remind"] = $this->input->get_post('edDateToRemind');
		$remData["invh_id"] = $this->input->get_post('edInvoiceID');
		$this->mReminders->editReminder($remID, $remData);
		redirect('CReminders/index/2');
	}

	public function deleteReminder()
	{
		$remID = $this->input->get_post('edID');
		$this->mReminders->deleteReminder($remID);
		redirect('CReminders/index/3');
	}

	public function checkAndSendReminder()
	{
		$invoiceIDs = $this->mReminders->selectRemindersForToday();
		foreach ($invoiceIDs as $item) {
			$this->sendReminderByInvID($item->invh_id);
		}
	}

	public function sendReminderByInvID($invoiceID)
	{
		$mail = $this->phpmailer_library->load();
		$mail->isSMTP();
		$mail->Host = 'smtp.gmail.com';
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';

		/* Username (email address). */
		$mail->Username = 'bbtourtravel.surabaya@gmail.com';

		/* Google account password. */
		$mail->Password = 'bbtourtravelsurabaya';

		$invh = $this->mInvoices->selectInvoiceHeaderById($invoiceID);

		$customer = $this->mCustomers->selectCustomerByID($invh->cust_id);
		$amountDue = $invh->invh_total - $this->mInvoices->selectAmountPaid($invoiceID)->ar_total;

		if ($customer->cust_email != '') {
			try {

				/* Set the mail sender. */
				$mail->setFrom('info@bbtourtravel.com', 'BB Tour & Travel Surabaya');

				/* Add a recipient. */
				$mail->addAddress($customer->cust_email, $customer->cust_name);

				/* Set the subject. */
				$mail->Subject = 'Reminder for Inv BB-' . $invoiceID;

				/* Set the mail message body as HTML type */
				$mail->isHTML(TRUE);

				/* Set the mail message body. */
				$mail->Body = "<p>Email ini sebagai pengingat bahwa Bapak/Ibu " . $customer->cust_name .
					" masih memiliki tagihan invoice BB Tour Travel sebesar Rp. " . $amountDue .
					" yang harus dibayar paling lambat pada tanggal " . date("d-m-Y", strtotime($invh->invh_date_due)) .
					". Terima kasih atas perhatiannya.</p>" .
					"<p>BB Tour & Travel</p>";

				/* Finally send the mail. */
				$mail->send();
			 }
			 catch (Exception $e)
			 {
				/* PHPMailer exception. */
				echo $e->errorMessage();
			 }
			 catch (\Exception $e)
			 {
				/* PHP exception (note the backslash to select the global namespace Exception class). */
				echo $e->getMessage();
			 }

			 echo "The reminder has been sent to " . $customer->cust_email;
		} else {
			echo "The customer has no registered email.";
		}

	}
}
