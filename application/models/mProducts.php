<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mProducts extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function selectAllProducts()
	{
		$select = $this->db
					->select('*')
					->from("t_products_services")
					->where("ps_status", 1)
					->order_by("ps_id", "asc")
					->get();
		return $select->result();
	}

	function selectAllProductsName()
	{
		$select = $this->db
					->select('ps_id, ps_name')
					->from('t_products_services')
					->where('ps_status', 1)
					->order_by('ps_id', 'asc')
					->get();
		return $select->result();
	}

	function selectProductByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_products_services")
					->where("ps_id", $id)
					->get();
		return $select->row();
	}

	function selectProductNameByID($id)
	{
		$select = $this->db
					->select("ps_name")
					->from("t_products_services")
					->where("ps_id", $id)
					->get();
		return $select->row();
	}

	function selectProductsOrdersSalesLast3Months()
	{
		$select = $this->db
					->select("*")
					->from("v_total_per_ps_last_3_month")
					->get();
		return $select->result();
	}

	function addProduct($dataInsert)
	{
		$this->db->insert("t_products_services", $dataInsert);
		return $this->db->affected_rows();
	}

	function editProduct($id, $dataUpdate)
	{
		$this->db->where('ps_id', $id);
		$this->db->update("t_products_services", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteProduct($id, $dataUpdate)
	{
		$this->db->where('ps_id', $id);
		$this->db->update("t_products_services", $dataUpdate);
		return $this->db->affected_rows();
	}
}
