<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mEmployees extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function selectAllEmployees()
	{
		$select = $this->db
					->select('*')
					->from("t_employees")
					->where("emp_status", 1)
					->order_by("emp_id", "asc")
					->get();
		return $select->result();
	}

	function selectEmployeeByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_employees")
					->where("emp_id", $id)
					->get();
		return $select->row();
	}

	function selectEmployeeByToken($token, $time)
	{
		$select = $this->db
					->select("*")
					->from("t_employees")
					->where("emp_token_expire >", $time)
					->where("emp_reset_token", $token)
					->get();
		return $select->row();
	}

	function selectEmployeeNameByID($id)
	{
		$select = $this->db
					->select("emp_name")
					->from("t_employees")
					->where("emp_id", $id)
					->get();
		return $select->row();
	}

	function countEmployeeByUsername($username)
	{
		$select = $this->db
					->select("emp_username")
					->from("t_employees")
					->where("emp_username", $username)
					->get();
		return $select->num_rows();
	}

	function checkLogin($username, $password)
	{
		$select = $this->db
					->select('emp_id, emp_name, emp_access_level')
					->from("t_employees")
					->where("emp_username", $username)
					->where("emp_password", $password)
					->where("emp_status", 1)
					->get();
		return $select->result();
    }


	function addEmployee($dataInsert)
	{
		$this->db->insert("t_employees", $dataInsert);
		return $this->db->affected_rows();
	}

	function editEmployee($id, $dataUpdate)
	{
		$this->db->where('emp_id', $id);
		$this->db->update("t_employees", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteEmployee($id, $dataUpdate)
	{
		$this->db->where('emp_id', $id);
		$this->db->update("t_employees", $dataUpdate);
		return $this->db->affected_rows();
	}
}
