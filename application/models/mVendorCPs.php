<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mVendorCPs extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function selectAllVendorCPs()
	{
		$select = $this->db
					->select('*')
					->from("v_vend_cps")
					->where("vendcp_status", 1)
					->order_by("vendcp_id", "asc")
					->get();
		return $select->result();
	}

	function selectVendorCPByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_vend_cps")
					->where("vendcp_id", $id)
					->get();
		return $select->row();
	}

	function selectVendorCPNameByID($id)
	{
		$select = $this->db
					->select("vendcp_name")
					->from("t_vend_cps")
					->where("vendcp_id", $id)
					->get();
		return $select->row();
	}
	
	function addVendorCP($dataInsert)
	{
		$this->db->insert("t_vend_cps", $dataInsert);
		return $this->db->affected_rows();
	}
	
	function editVendorCP($id, $dataUpdate)
	{
		$this->db->where('vendcp_id', $id);
		$this->db->update("t_vend_cps", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteVendorCP($id, $dataUpdate)
	{
		$this->db->where('vendcp_id', $id);
		$this->db->update("t_vend_cps", $dataUpdate);
		return $this->db->affected_rows();
	}
}