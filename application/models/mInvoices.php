<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mInvoices extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function selectAllInvoices()
	{
		$select = $this->db
					->select('*')
					->from("v_invoice_h")
					->where("invh_status !=", "Voided")
					->order_by("invh_id", "desc")
					->get();
		return $select->result();
	}

	function selectAllUnpaidInvoices()
	{
		$select = $this->db
					->select('*')
					->from("v_unpaid_invoice_h")
					->where("invh_status !=", "Voided")
					->where("invh_status !=", "Draft")
					->order_by("invh_id", "desc")
					->get();
		return $select->result();
	}

	function selectAllDraftInvoices()
	{
		$select = $this->db
					->select('*')
					->from("v_invoice_h")
					->where("invh_status", "Draft")
					->order_by("invh_id", "desc")
					->get();
		return $select->result();
	}

	function selectAllInvoiceDetailsCommissions()
	{
		$select = $this->db
					->select('*')
					->from("v_active_invoice_d")
					->where("invh_created_by !=", "0")
					->order_by("invh_id", "desc")
					->get();
		return $select->result();
	}

	function selectMaxInvoiceId()
	{
		$select = $this->db
					->select('max(invh_id) as maxId')
					->from("t_invoice_h")
					->get();
		return $select->result();
	}

	function selectInvoiceHeaderById($id)
	{
		$select = $this->db
					->select("*")
					->from("v_invoice_h")
					->where("invh_id", $id)
					->get();
		return $select->row();
	}

	function selectInvoiceDetailById($id)
	{
		$select = $this->db
					->select("*")
					->from("v_invoice_d")
					->where("invh_id", $id)
					->get();
		return $select->result();
	}

	function selectInvoiceDetailByIDs($invoiceID, $psID)
	{
		$select = $this->db
					->select("*")
					->from("v_active_invoice_d")
					->where("invh_id", $invoiceID)
					->where("ps_id", $psID)
					->get();
		return $select->row();
	}
	// Transpose Rows to Columns
	function selectCategoriesTotalLast3Months()
	{
		$select = $this->db
					->select("MAX(CASE WHEN t.ps_category = 'Car Rental' THEN t.total ELSE NULL END) AS car,
					MAX(CASE WHEN t.ps_category = 'Flight Ticket' THEN t.total ELSE NULL END) AS flight,
					MAX(CASE WHEN t.ps_category = 'Hotel Voucher' THEN t.total ELSE NULL END) AS hotel,
					MAX(CASE WHEN t.ps_category = 'Tour Package' THEN t.total ELSE NULL END) AS tour,
					MAX(CASE WHEN t.ps_category = 'Train Ticket' THEN t.total ELSE NULL END) AS train")
					->from("v_total_per_cat_last_3_months t")
					->get();
		return $select->row();
	}

	function selectSalesTotalLast3Months()
	{
		$select = $this->db
					->select("*")
					->from("v_sales_total_last_3_months")
					->get();
		return $select->result();
	}

	function selectCategoryTotalSalesByMonthYearAndCategory($month, $year, $category)
	{
		$select = $this->db
					->select("ps.ps_category AS ps_category, ifnull(sum((aid.invd_quantity * aid.invd_price_actual)),0) AS total")
					->from("t_products_services ps")
					->join("v_active_invoice_d aid", "ps.ps_id = aid.ps_id AND MONTH(aid.invh_date_created) = ".$month." AND YEAR(aid.invh_date_created) = ".$year, "left")
					->where("ps_category", $category)
					->group_by("ps_category")
					->get();
		return $select->row();
	}

	function selectCategoryTotalSpendingByMonthYearAndCategory($month, $year, $category)
	{
		$select = $this->db
					->select("ps.ps_category AS ps_category, ifnull(sum((aid.invd_quantity * aid.invd_cost)),0) AS total")
					->from("t_products_services ps")
					->join("v_active_invoice_d aid", "ps.ps_id = aid.ps_id AND MONTH(aid.invh_date_created) = ".$month." AND YEAR(aid.invh_date_created) = ".$year, "left")
					->where("ps_category", $category)
					->group_by("ps_category")
					->get();
		return $select->row();
	}

	function selectCommissionTotalByMonthYear($month, $year)
	{
		$select = $this->db
					->select("SUM(invd_commission) as total")
					->from("v_active_invoice_d")
					->where("MONTH(invh_date_created)", $month)
					->where("YEAR(invh_date_created)", $year)
					->get();
		return $select->row();
	}

	function selectEmployeesSalesCommissionThisMonth()
	{
		$select = $this->db
					->select("e.emp_name, SUM(invd_quantity * invd_price_actual) as sales, SUM(aid.invd_commission) as commission")
					->from("v_active_invoice_d aid")
					->join("t_employees e","aid.invh_created_by = e.emp_id")
					->where("MONTH(invh_date_created) = MONTH(curdate())")
					->where("YEAR(invh_date_created) = YEAR(curdate())")
					->group_by("e.emp_name")
					->get();
	return $select->result();
	}

	function selectEmployeesSalesCommissionByMonthYear($month, $year)
	{
		$select = $this->db
					->select("e.emp_name, SUM(invd_quantity * invd_price_actual) as sales, SUM(aid.invd_commission) as commission")
					->from("v_active_invoice_d aid")
					->join("t_employees e","aid.invh_created_by = e.emp_id")
					->where("MONTH(invh_date_created)", $month)
					->where("YEAR(invh_date_created)", $year)
					->group_by("e.emp_name")
					->get();
	return $select->result();
	}

	function selectAmountPaid($id)
	{
		$select = $this->db
					->select("ar_total")
					->from("v_invoice_h AS v_invoice_h")
					->join("(SELECT t_ar.ar_income_id, SUM(t_ar.ar_amount) ar_total, t_ar.ar_status FROM t_ar WHERE t_ar.ar_status = 1 AND t_ar.ar_income_type LIKE 'Invoice' GROUP BY t_ar.ar_income_id) AS ar", "ar.ar_income_id = v_invoice_h.invh_id", "left")
					->where("v_invoice_h.invh_id", $id)
					->get();
		return $select->row();
	}

	function selectAmountDueTotal()
	{
		$select = $this->db
					->select("SUM(invh.invh_total - IFNULL(ar.ar_total,0)) due_total")
					->from("v_invoice_h AS invh")
					->join("(SELECT t_ar.ar_income_id, SUM(t_ar.ar_amount) ar_total, t_ar.ar_status FROM t_ar WHERE t_ar.ar_status = 1 AND t_ar.ar_income_type LIKE 'Invoice' GROUP BY t_ar.ar_income_id) AS ar", "ar.ar_income_id = invh.invh_id", "left")
					->where("invh.invh_status !=","Voided")
					->where("invh.invh_status !=","Draft")
					->get();
		return $select->row();
	}

	function selectAmountOverDueTotal()
	{
		$select = $this->db
					->select("SUM(invh.invh_total - IFNULL(ar.ar_total,0)) due_total")
					->from("v_invoice_h AS invh")
					->join("(SELECT t_ar.ar_income_id, SUM(t_ar.ar_amount) ar_total, t_ar.ar_status FROM t_ar WHERE t_ar.ar_status = 1 AND t_ar.ar_income_type LIKE 'Invoice' GROUP BY t_ar.ar_income_id) AS ar", "ar.ar_income_id = invh.invh_id", "left")
					->where("invh.invh_status","Overdue")
					->get();
		return $select->row();
	}

	function selectAmountDueTotalIn30Days()
	{
		$select = $this->db
					->select("SUM(invh.invh_total - IFNULL(ar.ar_total,0)) due_total")
					->from("v_invoice_h AS invh")
					->join("(SELECT t_ar.ar_income_id, SUM(t_ar.ar_amount) ar_total, t_ar.ar_status FROM t_ar WHERE t_ar.ar_status = 1 AND t_ar.ar_income_type LIKE 'Invoice' GROUP BY t_ar.ar_income_id) AS ar", "ar.ar_income_id = invh.invh_id", "left")
					->where("invh.invh_status","Overdue")
					->or_group_start()
					->where("invh.invh_status","Sent")
					->where("(CURDATE() + INTERVAL 30 DAY) >", "invh.invh_due_date")
					->group_end()
					->get();
		return $select->row();
	}

	function selectAcountPayableByCJID($cjid) {
		$select = $this->db
					->select("ap_payment_type, ap_notes")
					->from("t_ap")
					->where("ap_bill_id", $cjid)
					->where("ap_bill_type", "Biaya")
					->get();
		return $select->row();
	}

	function countInvoicesThisMonth()
	{
		$select = $this->db
					->select("count(*) as total")
					->from("v_inv_this_month")
					->get();
		return $select->row();
	}

	function selectSalesThisMonth()
	{
		$select = $this->db
					->select("sales")
					->from("v_sales_this_month")
					->get();
		return $select->row();
	}

	function addInvoiceHeader($dataInsert)
	{
		$this->db->insert("t_invoice_h", $dataInsert);
		return $this->db->affected_rows();
	}

	function editInvoiceHeader($id, $dataUpdate)
	{
		$this->db->where('invh_id', $id);
		$this->db->update("t_invoice_h", $dataUpdate);
		return $this->db->affected_rows();
	}

	function updateOverdueStatus()
	{
		$data['invh_status'] = 'Overdue';
		$this->db->group_start();
		$this->db->where("invh_status = 'Sent' OR invh_status = 'Partial'");
		$this->db->group_end();
		$this->db->where("invh_date_due < CURDATE()");
		$this->db->update("t_invoice_h", $data);
		return $this->db->affected_rows();
	}

	function deleteInvoiceHeaderById($id, $dataUpdate)
	{
		$this->db->where('invh_id', $id);
		$this->db->update("t_invoice_h", $dataUpdate);
		return $this->db->affected_rows();
	}

	function addInvoiceDetail($dataInsert)
	{
		$this->db->insert("t_invoice_d", $dataInsert);
		return $this->db->affected_rows();
	}

	function editInvoiceDetailByIDs($invoiceID, $psID, $dataUpdate)
	{
		$this->db->where('invh_id', $invoiceID);
		$this->db->where('ps_id', $psID);
		$this->db->update("t_invoice_d", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteInvoiceDetailById($id)
	{
		$this->db->where('invh_id', $id);
		$this->db->delete("t_invoice_d");
		return $this->db->affected_rows();
	}

	function addAccountPayable($dataInsert)
	{
		$this->db->insert("t_ap", $dataInsert);
		return $this->db->affected_rows();
	}

	function deleteAccountPayableByInvoiceID($id, $dataUpdate)
	{
		$this->db->where('ap_bill_id', $id);
		$this->db->where('ap_bill_type', 'Invoice');
		$this->db->update("t_ap", $dataUpdate);
		return $this->db->affected_rows();
	}

	function addAccountReceivable($dataInsert)
	{
		$this->db->insert("t_ar", $dataInsert);
		return $this->db->affected_rows();
	}

	// function editAccountReceivable($id, $dataUpdate)
	// {
	// 	$this->db->where('invh_id', $id);
	// 	$this->db->update("t_ar", $dataUpdate);
	// 	return $this->db->affected_rows();
	// }

	function deleteAccountReceivableByInvoiceID($id, $dataUpdate)
	{
		$this->db->where('ar_income_id', $id);
		$this->db->where('ar_income_type', 'Invoice');
		$this->db->update("t_ar", $dataUpdate);
		return $this->db->affected_rows();
	}
}
