<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mTransactions extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function selectAllTransactions()
	{
		$select = $this->db
					->select('*')
					->from("v_transactions")
					->where("tran_status", 1)
					->order_by("tran_date", "desc")
					->get();
		return $select->result();
	}
	
	// function selectMaxId()
	// {
	// 	$select = $this->db
	// 				->select('count(*) as jumlah')
	// 				->from("t_users")
	// 				->get();
	// 	return $select->result();
	// }
	
	// function selectByIdUsers($id)
	// {
	// 	$select = $this->db
	// 				->select('*')
	// 				->from("t_users")
	// 				->where("user_id", $id)
	// 				->get();
	// 	return $select->result();
	// }

	function selectTransactionByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_cost_journals")
					->where("cj_id", $id)
					->get();
		return $select->row();
	}

	function selectAccountPayableByCJID($cjid) {
		$select = $this->db
					->select("ap_payment_type, ap_notes")
					->from("t_ap")
					->where("ap_bill_id", $cjid)
					->where("ap_bill_type", "Biaya")
					->get();
		return $select->row();
	}

	function selectPaymentByInvoiceID($invh_id){
		$select = $this->db
					->select('*')
					->from("t_ar")
					->where("ar_status", 1)
					->where("ar_income_id", $invh_id)
					->where("ar_income_type", "Invoice")
					->order_by("ar_date", "asc")
					->get();
		return $select->result();
	}

	function selectAccountReceivableByID($id)
	{
		$select = $this->db
					->select('*')
					->from("t_ar")
					->where("ar_id", $id)
					->get();
		return $select->row();
	}

	function selectAccountReceivableByOIID($id)
	{
		$select = $this->db
					->select('*')
					->from("t_ar")
					->where("ar_income_id", $id)
					->where("ar_income_type", "Other Income")
					->get();
		return $select->row();
	}

	function selectTransactionNameByID($id)
	{
		$select = $this->db
					->select("cj_name")
					->from("t_cost_journals")
					->where("cj_id", $id)
					->get();
		return $select->row();
	} 
	
	function addAccountPayable($dataInsert)
	{
		$this->db->insert("t_ap", $dataInsert);
		return $this->db->affected_rows();
	}

	function editAccountPayableByCJID($id, $dataUpdate)
	{
		$this->db->where('ap_bill_id', $id);
		$this->db->where('ap_bill_type', 'Biaya');
		$this->db->update("t_ap", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteAccountPayableByCJID($id, $dataUpdate)
	{
		$this->db->where('ap_bill_id', $id);
		$this->db->where('ap_bill_type', 'Biaya');
		$this->db->update("t_ap", $dataUpdate);
		return $this->db->affected_rows();
	}

	function addAccountReceivable($dataInsert)
	{
		$this->db->insert("t_ar", $dataInsert);
		return $this->db->affected_rows();
	}
	
	function editAccountReceivable($id, $dataUpdate)
	{
		$this->db->where('ar_id', $id);
		$this->db->update("t_ar", $dataUpdate);
		return $this->db->affected_rows();
	}

	function editAccountReceivableByOIID($id, $dataUpdate)
	{
		$this->db->where('ar_income_id', $id);
		$this->db->where('ar_income_type', 'Other Income');
		$this->db->update("t_ar", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteAccountReceivable($id, $dataUpdate)
	{
		$this->db->where('ar_id', $id);
		$this->db->update("t_ar", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteAccountReceivableByOIID($id, $dataUpdate)
	{
		$this->db->where('ar_income_id', $id);
		$this->db->where('ar_income_type', 'Other Income');
		$this->db->update("t_ar", $dataUpdate);
		return $this->db->affected_rows();
	}
}