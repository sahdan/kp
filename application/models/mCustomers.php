<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mCustomers extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function selectAllCustomers()
	{
		$select = $this->db
					->select('*')
					->from("t_customers")
					->where("cust_status", 1)
					->order_by("cust_id", "asc")
					->get();
		return $select->result();
	}
	
	// function selectMaxId()
	// {
	// 	$select = $this->db
	// 				->select('count(*) as jumlah')
	// 				->from("t_users")
	// 				->get();
	// 	return $select->result();
	// }
	
	// function selectByIdUsers($id)
	// {
	// 	$select = $this->db
	// 				->select('*')
	// 				->from("t_users")
	// 				->where("user_id", $id)
	// 				->get();
	// 	return $select->result();
	// }

	function selectAllCustomersName()
	{
		$select = $this->db
					->select('cust_id, cust_name')
					->from("t_customers")
					->where("cust_status", 1)
					->order_by("cust_name", "asc")
					->get();
		return $select->result();
	}

	function selectCustomersByName($str)
	{
		$select = $this->db
					->select('cust_id, cust_name')
					->from("t_customers")
					->like('cust_name', $str)
					->where("cust_status", 1)
					->limit(5)
					->order_by("cust_name", "asc")
					->get();
		return $select->result();
	}

	function selectCustomerByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_customers")
					->where("cust_id", $id)
					->get();
		return $select->row();
	}

	function selectCustomerNameByID($id)
	{
		$select = $this->db
					->select("cust_name")
					->from("t_customers")
					->where("cust_id", $id)
					->get();
		return $select->row();
	} 

	function countNewCustomersThisMonth()
	{
		$select = $this->db
					->select("count(*) as total")
					->from("v_new_cust_this_month")
					->get();
		return $select->row();
	}
	
	function addCustomer($dataInsert)
	{
		$this->db->insert("t_customers", $dataInsert);
		return $this->db->affected_rows();
	}
	
	function editCustomer($id, $dataUpdate)
	{
		$this->db->where('cust_id', $id);
		$this->db->update("t_customers", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteCustomer($id, $dataUpdate)
	{
		$this->db->where('cust_id', $id);
		$this->db->update("t_customers", $dataUpdate);
		return $this->db->affected_rows();
	}
}