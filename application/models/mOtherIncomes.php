<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mOtherIncomes extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function selectAllOtherIncomes()
	{
		$select = $this->db
					->select('*')
					->from("t_other_incomes")
					->where("oi_status", 1)
					->order_by("oi_id", "asc")
					->get();
		return $select->result();
	}
	
	function selectMaxOtherIncomeId()
	{
		$select = $this->db
					->select('max(oi_id) as maxId')
					->from("t_other_incomes")
					->get();
		return $select->result();
	}

	function selectOtherIncomesNonBankByMonthYear($month, $year)
	{
		$select = $this->db
					->select("*")
					->from("t_other_incomes")
					->where("oi_category", "Pendapatan Usaha Lainnya")
					->where("MONTH(oi_date)", $month)
					->where("YEAR(oi_date)", $year)
					->where("oi_status", 1)
					->get();
		return $select->result();
	}

	function selectOtherIncomesBankTotalByMonthYear($month, $year)
	{
		$select = $this->db
					->select("SUM(oi_amount) as total")
					->from("t_other_incomes")
					->where("oi_category", "Pendapatan Lain-Lain (Jasa Giro,Bunga,dll)")
					->where("MONTH(oi_date)", $month)
					->where("YEAR(oi_date)", $year)
					->where("oi_status", 1)
					->get();
		return $select->row();
	}

	function selectOtherIncomeNameByID($id)
	{
		$select = $this->db
					->select("oi_name")
					->from("t_other_incomes")
					->where("oi_id", $id)
					->where("oi_status", 1)
					->get();
		return $select->row();
	} 
	
	function addOtherIncome($dataInsert)
	{
		$this->db->insert("t_other_incomes", $dataInsert);
		return $this->db->affected_rows();
	}
	
	function editOtherIncome($id, $dataUpdate)
	{
		$this->db->where('oi_id', $id);
		$this->db->update("t_other_incomes", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteOtherIncome($id, $dataUpdate)
	{
		$this->db->where('oi_id', $id);
		$this->db->update("t_other_incomes", $dataUpdate);
		return $this->db->affected_rows();
	}
}