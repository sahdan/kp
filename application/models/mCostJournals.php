<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mCostJournals extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function selectAllCostJournals()
	{
		$select = $this->db
					->select('*')
					->from("t_cost_journals")
					->where("cj_status", 1)
					->order_by("cj_id", "asc")
					->get();
		return $select->result();
	}

	function selectCostJournalsByMonthYearCategory($month, $year, $category)
	{
		$select = $this->db
					->select('*')
					->from("t_cost_journals")
					->where("cj_status", 1)
					->where("cj_category", $category)
					->where("MONTH(cj_date)", $month)
					->where("YEAR(cj_date)", $year)
					->order_by("cj_id", "asc")
					->get();
		return $select->result();
	}

	function selectCostJournalsTotalByMonthYearCategory($month, $year, $category)
	{
		$select = $this->db
					->select('SUM(cj_amount) as total')
					->from("t_cost_journals")
					->where("cj_status", 1)
					->where("cj_category", $category)
					->where("MONTH(cj_date)", $month)
					->where("YEAR(cj_date)", $year)
					->get();
		return $select->row();
	}


	
	function selectMaxCostJournalId()
	{
		$select = $this->db
					->select('max(cj_id) as maxId')
					->from("t_cost_journals")
					->get();
		return $select->result();
	}

	function selectCostJournalByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_cost_journals")
					->where("cj_id", $id)
					->get();
		return $select->row();
	}

	function selectCostJournalNameByID($id)
	{
		$select = $this->db
					->select("cj_name")
					->from("t_cost_journals")
					->where("cj_id", $id)
					->get();
		return $select->row();
	} 
	
	function addCostJournal($dataInsert)
	{
		$this->db->insert("t_cost_journals", $dataInsert);
		return $this->db->affected_rows();
	}
	
	function editCostJournal($id, $dataUpdate)
	{
		$this->db->where('cj_id', $id);
		$this->db->update("t_cost_journals", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteCostJournal($id, $dataUpdate)
	{
		$this->db->where('cj_id', $id);
		$this->db->update("t_cost_journals", $dataUpdate);
		return $this->db->affected_rows();
	}
}