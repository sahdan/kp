<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mReminders extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function selectAllReminders()
	{
		$select = $this->db
					->select('*')
					->from("t_reminders")
					->get();
		return $select->result();
	}

	function selectReminderByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_reminders")
					->where("rem_id", $id)
					->get();
		return $select->row();
	}

	function selectRemindersByInvoiceID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_reminders")
					->where("invh_id", $id)
					->get();
		return $select->result();
	}

	function selectRemindersForToday()
	{
		$select = $this->db
					->select("*")
					->from("t_reminders")
					->where("rem_date_remind = date(now())")
					->get();
		return $select->result();
	}

	function addReminder($dataInsert)
	{
		$this->db->insert("t_reminders", $dataInsert);
		return $this->db->affected_rows();
	}

	function editReminder($id, $dataUpdate)
	{
		$this->db->where('rem_id', $id);
		$this->db->update("t_reminders", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteReminder($id)
	{
		$this->db->where('rem_id', $id);
		return $this->db->delete("t_reminders");
	}
}
