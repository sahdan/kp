<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mVendors extends CI_Model {
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function selectAllVendors()
	{
		$select = $this->db
					->select('*')
					->from("t_vendors")
					->where("vend_status", 1)
					->order_by("vend_id", "asc")
					->get();
		return $select->result();
	}

	function selectAllVendorsName()
	{
		$select = $this->db
					->select('vend_id, vend_name')
					->from("t_vendors")
					->where("vend_status", 1)
					->order_by("vend_id", "asc")
					->get();
		return $select->result();
	}
	
	// function selectMaxId()
	// {
	// 	$select = $this->db
	// 				->select('count(*) as jumlah')
	// 				->from("t_users")
	// 				->get();
	// 	return $select->result();
	// }
	
	// function selectByIdUsers($id)
	// {
	// 	$select = $this->db
	// 				->select('*')
	// 				->from("t_users")
	// 				->where("user_id", $id)
	// 				->get();
	// 	return $select->result();
	// }

	function selectVendorByID($id)
	{
		$select = $this->db
					->select("*")
					->from("t_vendors")
					->where("vend_id", $id)
					->get();
		return $select->row();
	}

	function selectVendorNameByID($id)
	{
		$select = $this->db
					->select("vend_name")
					->from("t_vendors")
					->where("vend_id", $id)
					->get();
		return $select->row();
	}
	
	function addVendor($dataInsert)
	{
		$this->db->insert("t_vendors", $dataInsert);
		return $this->db->affected_rows();
	}
	
	function editVendor($id, $dataUpdate)
	{
		$this->db->where('vend_id', $id);
		$this->db->update("t_vendors", $dataUpdate);
		return $this->db->affected_rows();
	}

	function deleteVendor($id, $dataUpdate)
	{
		$this->db->where('vend_id', $id);
		$this->db->update("t_vendors", $dataUpdate);
		return $this->db->affected_rows();
	}
}